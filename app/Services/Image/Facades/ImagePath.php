<?php

namespace App\Services\Image\Facades;

use Illuminate\Support\Facades\Facade;
use App\Services\Image\ImagePath as Image;

class ImagePath extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Image::class;
    }
}