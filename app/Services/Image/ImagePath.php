<?php

namespace App\Services\Image;

use App\Services\Image\ImageService;

class ImagePath extends ImageService
{
    private $imagecache = false;
    private $filter = 'square';


    public function cache()
    {
        $this->imagecache = true;
        return $this;
    }

    /**
     * Изображение по умолчанию
     * @return string
     */
    public function defaultValue($size, $key = null)
    {
        if (!is_null($key)) {
            return asset('img/default/'.$key.'/'.$size.'.png');
        } else {
            return asset('img/default/'.$size.'.png');
        }

        /*
        return $this->imagecache ?
            asset('imagecache/original/default/user.png') :
            asset('img/default/user.png');
        */
    }

    public function image($key, $size, $model, $filter = null)
    {
        $filename = $model->filename;
        if (is_null($filename) || empty($filename)) {
            return $this->defaultValue($size, $key);
        }
        if ($this->imagecache) {
            $outFilter = is_null($filter) ? $this->filter : $filter;
            return asset('imagecache/'.$outFilter.'/'.$key.'/'.$model->imageable_id.'/'.$size.'/'.$filename);
        } else {
            return asset($this->publicImagesPath.'/'.$key.'/'.$model->imageable_id.'/'.$size.'/'.$filename);
        }
    }

    public function main($key, $size, $model, $filter = null)
    {
        $images = $model->images;
        if (is_null($images) || empty($images) || empty($images[0])) {
            return $this->defaultValue($size, $key);
        }
        $oMainImage = $images->where('is_main', 1)->first();
        if (is_null($oMainImage)) {
            $oMainImage = $images->first();
            $oMainImage->is_main = 1;
            $oMainImage->save();
        }
        $filename = $oMainImage->filename;
        if ($this->imagecache) {
            $outFilter = is_null($filter) ? $this->filter : $filter;
            return asset('imagecache/'.$outFilter.'/'.$key.'/'.$model->id.'/'.$size.'/'.$filename);
        } else {
            return asset($this->publicImagesPath.'/'.$key.'/'.$model->id.'/'.$size.'/'.$filename);
        }
    }

    public function publicPath($key, $size, $model, $filter = null)
    {
        $filename = $model->filename;
        if (is_null($filename) || empty($filename)) {
            return $this->defaultValue($size, $key);
        }
        return public_path($this->publicImagesPath.'/'.$key.'/'.$model->imageable_id.'/'.$size.'/'.$filename);
    }
}