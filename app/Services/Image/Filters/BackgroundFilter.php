<?php

namespace App\Services\Image\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\ImageManagerStatic as ImageStatic;
use Illuminate\Support\Facades\File;

class BackgroundFilter implements FilterInterface
{
    private $image = null;
    private $options = [];

    public function __construct($options)
    {
        $this->options = $options;
    }

    public function applyFilter(Image $image)
    {
        $image = $image->fit(774, 205);
        if (isset($this->options['blur'])) {
            $image = $image->blur($this->options['blur']);
        }
        return $image;
    }

    /**
     * Обрезка по размерам
     * @param $sFileName
     * @param $originalPath
     * @param $path
     */
    public function resize($sFileName, $originalPath, $path, $key)
    {
        $this->image = ImageStatic::make($originalPath.$sFileName);
        $this->image = $this->applyFilter($this->image);
        $sPath = $path.'/'.$key.'/';
        $this->checkDirectory($sPath);
        $this->image->save($sPath.$sFileName);
    }

    /**
     * Проверить на существование директории
     * @param $path
     */
    public function checkDirectory($path)
    {
        if(!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }
    }
}