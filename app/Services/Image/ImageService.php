<?php

namespace App\Services\Image;

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class ImageService
{
    /**
     * Директория с оригиналами
     * @var string
     */
    protected $originalPath = 'original';
    protected $publicImagesPath = 'images';
    protected $path;

    public $sFileName = '';
    protected $image = null;

    /**
     * Загрузить и обрезать
     * @param $file
     * @param $sType
     * @param $nId
     * @return string
     */
    public function upload($file, $sType, $nId, $filters = null)
    {
        $this->path = $this->publicImagesPath.'/'.$sType.'/'.$nId;
        $this->originalPath = public_path($this->path.'/'.$this->originalPath.'/');

        $this->sFileName = $this->uploadOriginalFile($file, $this->originalPath);

        if (!is_null($filters)) {
            foreach($filters as $key => $filter) {
                $oObject = new $filter['filter']($filter['options']);
                $oObject->resize($this->sFileName, $this->originalPath, $this->path, $key);
            }
        }


        return $this->sFileName;
    }

    /**
     * Загрузить оригинальное изображение
     * @param $file
     * @param $path
     * @return string
     */
    public function uploadOriginalFile($file, $path)
    {
        $sFileName = $file->getClientOriginalName();
        $sFileName = str_random(12).''.substr(strrchr($sFileName, '.'), 0);
        $file->move($path,$sFileName);


        return $sFileName;
    }

    /**
     * Удалить изображение со всех папок
     * @param $sFileName
     * @param $sType
     * @param $nId
     */
    public function deleteImages($sFileName, $sType, $nId)
    {
        $aSizes[] = [
            'path' => 'original'
        ];
        if (isset($sType['key'])) {
            $path = $this->publicImagesPath.'/'.$sType['key'].'/'.$nId;
            $aSizes = [];
            $aSizes[] = 'original';
            foreach($sType['filters'] as $key => $filter) {
                $aSizes[] = $key;
            }
        } else {
            $path = $this->publicImagesPath.'/'.$sType.'/'.$nId;
        }
        foreach($aSizes as $size) {
            if (isset($size['path'])) {
                $file = public_path($path.'/'.$size['path'].'/'.$sFileName);
            } else {
                $file = public_path($path.'/'.$size.'/'.$sFileName);
            }
            if (file_exists($file)) {
                File::Delete($file);
            }
        }
        //File::deleteDirectory(public_path($path));
    }

    public function copy($file, $sType, $nId, $filters = null)
    {
        $this->path = $this->publicImagesPath.'/'.$sType.'/'.$nId;
        $this->originalPath = 'original';
        $this->originalPath = public_path($this->path.'/'.$this->originalPath.'/');

        $file = Image::make($file);
        $this->checkDir($this->originalPath);
        $file->save($this->originalPath.$file->basename);

        if (!is_null($filters)) {
            foreach($filters as $key => $filter) {
                $oObject = new $filter['filter']($filter['options']);
                $oObject->resize($file->basename, $this->originalPath, $this->path, $key);
            }
        }
    }


    public function checkDir($path)
    {
        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }
    }



}