<?php

namespace App\Services;


use App\Mail\SendOrderToClient;
use App\Mail\SendOrderToOwner;
use App\Models\Payment;
use App\Models\Place;
use App\Models\Reservation;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class ReservationService
{
    private $reservationKey = 'reservation_id';

    private $oReservation = null;

    private $cacheReservationMinutes = 20;

    /**
     * Сохранение кода в сессию
     */
    public function setUniqueSessionId($oEvent)
    {
        $this->reservationKey = $this->getReservationKeyByEventId($oEvent->id);

        if (Session::exists($this->reservationKey)) {
            Session::put($this->reservationKey, $this->getUniqueId());
        } else {
            Session::put($this->reservationKey, $this->getUniqueId());
        }
    }

    /**
     * Ключ повторяется в views\site\components\contact\block.blade.php
     *
     * @param $id
     * @return string
     */
    private function getReservationKeyByEventId($id)
    {
        return $this->reservationKey.'-event_'.$id;
    }

    /**
     * Сгенерировать уникальный код
     *
     * @return string
     */
    public function getUniqueId()
    {
        return str_random(10);
    }


    public function checkReservation($oEvent, $oPlace)
    {
        $key = 'event_'.$oEvent->id.'_reservations';
        if (Cache::get($key)) {
            $aCacheReservation = Cache::get($key);
            $this->updateCacheReservation($aCacheReservation, $key, $oPlace);
        } else {
            $this->updateCacheReservation([], $key, $oPlace);
        }
    }

    public function getCacheReservation($oEvent)
    {
        $key = 'event_'.$oEvent->id.'_reservations';
        $aCacheReservation = Cache::get($key);
        //Log::info(json_encode($aCacheReservation));
        return $aCacheReservation;
    }
    public function getCacheReservationClient($oEvent)
    {
        $this->reservationKey = $this->getReservationKeyByEventId($oEvent->id);

        $key = 'event_'.$oEvent->id.'_reservations';
        $keyReserv = 'event_'.$oEvent->id.'_reservations_id';
        $aCacheReservation = Cache::get($keyReserv);
        $keySession = Session::get($this->reservationKey);
        if (isset($aCacheReservation[$keySession])) {
            //Log::info(json_encode($aCacheReservation[$keySession]));
            return $aCacheReservation[$keySession];
        } else {
            return [];
        }
    }

    public function updateCacheReservation($aCacheReservation, $key, $oPlace)
    {
        $sCode = $oPlace->section->name.'-'.$oPlace->row.'-'.$oPlace->num;
        $aCacheReservation = array_merge($aCacheReservation, [
            $sCode => [
                'status' => 0,
                $this->reservationKey => Session::get($this->reservationKey)
            ]
        ]);
        Cache::forget($key);
        Cache::put($key, $aCacheReservation, $this->cacheReservationMinutes);
        return $aCacheReservation;
    }

    public function setSimpleCacheReservation($aData)
    {
        $keyS = 'event_'.$aData[0]['event_id'].'_reservations';
        $keyReserv = 'event_'.$aData[0]['event_id'].'_reservations_id';

        $this->reservationKey = $this->getReservationKeyByEventId($aData[0]['event_id']);

        //Cache::forget($key);
        $aCacheStore = [];
        $aCacheReservation = Cache::get($keyS);
        //Log::info(json_encode($aCacheReservation));
        foreach($aData as $array) {
            $aCacheStore[$array['place_id']] = [
                'status' => 0
            ];
        }
        if (is_null($aCacheReservation)) {
            $aCacheReservation = [];
        }
        $aCacheStore = $aCacheReservation + $aCacheStore;
        Cache::put($keyS, $aCacheStore, $this->cacheReservationMinutes);
        $keySession = Session::get($this->reservationKey);
        $aCacheStore = [];
        foreach($aData as $array) {
            $aCacheStore[$keySession][] = $array['place_id'];
        }
        $aCacheReservation = Cache::get($keyReserv);
        if (is_null($aCacheReservation)) {
            $aCacheReservation = [];
        }
        $aCacheStore = $this->combinePlaces($aCacheReservation, $aCacheStore, $keySession);
        Cache::put($keyReserv, $aCacheStore['add'], $this->cacheReservationMinutes);
        if (!empty($aCacheStore['remove'])) {
            $s =  Cache::get($keyS);
            foreach($s as $key => $value) {
                if (in_array($key, $aCacheStore['remove'][$keySession])) {
                    unset($s[$key]);
                }
            }
            Cache::put($keyS, $s, $this->cacheReservationMinutes);
        }
        return $aCacheStore['add'][$keySession];
    }

    public function combinePlaces($aCache, $aNew, $keySession)
    {
        $add[$keySession] = [];
        $remove[$keySession] = [];
        if (isset($aCache[$keySession])) {
            foreach($aCache[$keySession] as $key => $value) {
                if (!array_search($value, $add[$keySession])) {
                    $remove[$keySession][] = $value;
                }
            }
        }

        if (isset($aNew[$keySession])) {
            foreach($aNew[$keySession] as $key => $value) {
                if (!array_search($value, $add[$keySession])) {
                    $add[$keySession][] = $value;
                }
            }
        }

        if (isset($aCache[$keySession])) {
            foreach($remove[$keySession] as $key => $value) {
                if (in_array($value, $add[$keySession])) {
                    unset($remove[$keySession][$key]);
                }
            }
        }
        return [
            'add' => $add,
            'remove' => $remove
        ];
    }

    /**
     * Создаем и возвращаем платежку
     *
     * @param $oClient
     * @param $oEvent
     * @param null $reservationKey
     * @return null
     */
    public function setCachePlacesBySession($oClient, $oEvent, $reservationKey = null)
    {
        $this->reservationKey = $this->getReservationKeyByEventId($oEvent->id);

        if (Session::exists($this->reservationKey)) {
            $keySession = Session::get($this->reservationKey);
        } else {
            $keySession = $reservationKey;
        }
        if (!is_null($reservationKey)) {
            if ($keySession !== $reservationKey) {
                Log::info('Client id: '.$oClient->id.' has wrong session key.');
                Log::info('Session key: '.$keySession.'.');
                Log::info('Form    key: '.$reservationKey.'.');
                $keySession = $reservationKey;
            }
        }
        $keyReserv = 'event_'.$oEvent->id.'_reservations_id';
        $aCacheReservation = Cache::get($keyReserv);
        if (isset($aCacheReservation[$keySession])) {
            $aPlacesId = $aCacheReservation[$keySession];
            $oPlaces = Place::whereIn('id', $aPlacesId)->get();
            if (!empty($oPlaces) && !empty($oPlaces[0])) {
                $oPayment = $this->setReservationByPlaces($oEvent, $oPlaces, $oClient);
                return $oPayment;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Зарезервировать места и вернуть платежку
     *
     * @param $oEvent
     * @param $oPlaces
     * @param $oClient
     * @return null
     */
    public function setReservationByPlaces($oEvent, $oPlaces, $oClient)
    {
        $aReservationsId = [];
        $sPaymentClientPurpose = 'Покупатель: '.$oClient->name.'.';
        $sPaymentPlacesPurpose = 'Места:';
        foreach($oPlaces as $oPlace) {
            $oReservation = Reservation::create([
                'event_id' => $oEvent->id,
                'client_id' => $oClient->id,
                'place_id' => $oPlace->id,
                'code' => $oPlace->section->name.'-'.$oPlace->row.'-'.$oPlace->num,
                'status' => 0
            ]);
            $aReservationsId[] = $oReservation->id;
            $sPaymentPlacesPurpose .= ' р'.$oPlace->row.'м'.$oPlace->num.'('.$oPlace->section->title.'),';
        }
        $sPaymentPlacesPurpose = substr($sPaymentPlacesPurpose, 0, -1);
        if (!empty($aReservationsId)) {
            $fPrice = count($aReservationsId) * $oEvent->price;
            $oPayment = Payment::create([
                'type_id' => 1,
                'price' => $fPrice,
                'purpose' => '',
                'status' => 0,
            ]);
            $sPaymentPurpose = 'Оплата бронирования №'.$oPayment->id.' на '.$oEvent->beginning_at->format('d.m.Y H:i').'.';
            $sPaymentPurpose = $sPaymentPurpose.' '.$sPaymentClientPurpose.' '.$sPaymentPlacesPurpose;
            $oPayment->update([
                'purpose' => $sPaymentPurpose,
            ]);
            $oReservations = Reservation::whereIn('id', $aReservationsId)->get();
            foreach($oReservations as $oReservation) {
                $oReservation->update([
                    'payment_id' => $oPayment->id
                ]);
            }
            Session::forget($this->reservationKey);
            return $oPayment;
        } else {
            return null;
        }
    }


    /**
     * Проверить на наличие бронирований в мероприятии и месте
     *
     * @param $aData
     * @return bool
     */
    public function checkSimpleCacheReservation($aData)
    {
        foreach($aData as $array) {
            $oReservation = Reservation::where('event_id', $aData[0]['event_id'])
                ->where('place_id', $array['place_id'])
                ->where(function($q) {
                    $q->where('status', 0)
                        ->orWhere('status', 1);
                })
                ->first();

            if (!is_null($oReservation)) {
                return false;
            }
        }
        return true;
    }


    /**
     * Получить все бронирования по мероприятию
     *
     * @param $oEvent
     * @return array
     */
    public function getReservations($oEvent)
    {
        $oReservations = Reservation::where('event_id', $oEvent->id)
            ->where(function($q) {
                $q->where('status', 0)
                    ->orWhere('status', 1);
            })->get();
        if (!empty($oReservations) && !empty($oReservations[0])) {
            return $oReservations->pluck('place_id')->toArray();
        } else {
            return [];
        }
    }




    public function setReservation($oEvent, $oPlace, $oClient)
    {
        $this->oReservation = Reservation::create([
            'event_id' => $oEvent->id,
            'place_id' => $oPlace->id,
            'client_id' => $oClient->id,
            'code' => $oPlace->section->name.'-'.$oPlace->row.'-'.$oPlace->num,
            'status' => 0
        ]);

        return $this;
    }

    public function setStatus($nStatus)
    {
        $this->oReservation->update([
            'status' => $nStatus
        ]);

        return $this;
    }

    public function getReservation()
    {
        return $this->oReservation;
    }


    public function setPaymentStatus($nId, $sResult)
    {
        $success = true;
        $status = 1;
        if ($sResult !== 'success') {
            $status = 4;
            $success = false;
        }
        $oPayment = Payment::with('reservations')
            ->where('id', $nId)
            ->first();

        if (intval($oPayment->status) === 1) {
            return true;
        }
        if (!is_null($oPayment)) {
            $oReservations = $oPayment->reservations;
            if (!empty($oReservations) && !empty($oReservations[0])) {
                if ($success) {
                    $mark = $oPayment->id.'-'.Str::random(5);
                    foreach($oReservations as $oReservation) {
                        $oReservation->update([
                            'status' => $status,
                            'mark' => $mark
                        ]);
                    }
                } else {
                    foreach($oReservations as $oReservation) {
                        $oReservation->update([
                            'status' => $status
                        ]);
                    }
                }
            }
            $oPayment->update([
                'status' => $status
            ]);
            if ($success) {
                $this->sendEmail($oReservations);
            }
        }
        return $success ? true : false;
    }

    public function sendEmail($oReservations)
    {
        $oClient = $oReservations->first()->client;

        Mail::to($oClient->email)
            ->send(new SendOrderToClient($oReservations));

        Mail::to(Config::get('mail.from.address'))
            ->send(new SendOrderToOwner($oReservations));
    }
}