<?php

namespace App\Services\Billing;


use App\Models\Balance;
use App\Models\Company;
use App\Models\Payment;
use App\Models\User;
use Carbon\Carbon;

class BillingService
{
    private $sSenderType;
    private $nSenderId;

    private $sRecipientType;
    private $nRecipientId;

    private $nType;
    private $fPrice;


    public static function init(){
        return new self;
    }


    /**
     * Назначить отправителя
     * @param $nSenderId
     * @param $sSenderType
     * @return $this
     */
    public function from($nSenderId, $sSenderType)
    {
        $this->nSenderId = $nSenderId;
        $this->sSenderType = $sSenderType;
        return $this;
    }

    /**
     * Назначить получателя
     * @param $nRecipientId
     * @param $sRecipientType
     * @return $this
     */
    public function to($nRecipientId, $sRecipientType)
    {
        $this->nRecipientId = $nRecipientId;
        $this->sRecipientType = $sRecipientType;
        return $this;
    }


    /**
     * Назначить тип перевода
     * @param int $nType|default
     * @return $this
     */
    public function type($nType = 1)
    {
        $this->nType = $nType;
        return $this;
    }

    /**
     * Назначить тип перевода
     * @param int $fPrice
     * @return $this
     */
    public function price($fPrice)
    {
        $this->fPrice = $fPrice;
        return $this;
    }


    public function user($nBalanceableId, $sBalanceableType)
    {
        return User::find($nBalanceableId);
    }

    public function payment($nSenderId, $nRecipientId, $nType, $fPrice)
    {
        return new PaymentService($nSenderId, $nRecipientId, $nType, $fPrice);
    }


    /**
     * Начислить
     * @return PaymentService|bool
     */
    public function set()
    {
        $oBalanceSender = $this->user($this->nSenderId, $this->sSenderType);
        $oBalanceRecipient = $this->user($this->nRecipientId, $this->sRecipientType);
        return $this->payment($oBalanceSender, $oBalanceRecipient, $this->nType, $this->fPrice)->set();
    }


}