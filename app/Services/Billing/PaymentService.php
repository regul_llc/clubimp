<?php

namespace App\Services\Billing;

use App\Models\Payment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PaymentService
{
    private $status = [
        0, //создан
        1, //успешно выполнен
    ];

    private $type = [
        1, // Начисление
    ];

    private $oBalanceSender;
    private $oBalanceRecipient;

    private $nType;
    private $fPrice;

    private $oPayment;


    public function __construct(
        $oBalanceSender,
        $oBalanceRecipient,
        $nType,
        $fPrice
    ) {
        $this->oBalanceSender = $oBalanceSender;
        $this->oBalanceRecipient = $oBalanceRecipient;
        $this->nType = $nType;
        $this->fPrice = $fPrice;

    }


    /**
     *
     * @return PaymentService|bool
     */
    public function set()
    {
        $status = $this->before();
        if ($status['result']) {
            DB::beginTransaction();
            try {
                $this->oPayment = Payment::updateOrCreate([
                    'type_id' => $this->nType,
                    'sender_id' => $this->oBalanceSender->id,
                    'recipient_id' => $this->oBalanceRecipient->id,
                    'price' => $this->fPrice,
                    'status' => 0,
                ]);
                $status = $this->after();
                if ($status['result']) {
                    DB::commit();
                    return [
                        'result' => true,
                        'message' => 'Успешно'
                    ];
                } else {
                    return $status;
                }
            } catch (\Exception $e) {
                DB::rollback();
                return [
                    'result' => false,
                    'message' => 'Ошибка транзакции'
                ];
            }
        } else {
            return [
                'result' => false,
                'message' => $status['message']
            ];
        }
    }

    private function before()
    {
        return [
            'result' => true,
            'message' => 'Успешно'
        ];
    }

    private function after()
    {
        return [
            'result' => true,
            'message' => 'Успешно'
        ];
    }



    /**
     * Добавить дополнительные параметры после совершения транзакции
     * @param array $aData
     * @return $this
     */
    public function setAfter(array $aData = [])
    {
        if (isset($aData['purpose'])) {
            $this->oPayment->purpose = $aData['purpose'];
            $this->oPayment->save();
        }
        if (isset($aData['status'])) {
            $this->oPayment->status = $aData['status'];
            $this->oPayment->save();
        }
        return $this;
    }


    /**
     * Получить модель Payment этого платежа
     * @return mixed
     */
    public function getPayment()
    {
        return $this->oPayment;
    }
}