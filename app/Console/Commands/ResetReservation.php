<?php

namespace App\Console\Commands;

use App\Models\Reservation;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class ResetReservation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reset:reservation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset reservation by status';

    private $lostMinutes = 75;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Log::info('-------------------------------------------');
        Log::info('-------------ResetReservation--------------');
        //Log::info('-------------------------------------------');
        $this->info('Start reset reservation');

        $oReservations = Reservation::with('payment')
            ->where('status', 0)
            ->where('created_at', '<', Carbon::now()->subMinutes($this->lostMinutes))
            ->get();

        $this->info('Find: '.count($oReservations).' reservations.');
        Log::info('Find: '.count($oReservations).' reservations.');

        if (!empty($oReservations) && !empty($oReservations[0])) {
            foreach($oReservations as $oReservation) {
                $oPayment = $oReservation->payment;
                if ($oPayment->status === 0) {
                    $oPayment->update([
                        'status' => 4 // провален
                    ]);
                }
                $oReservation->update([
                    'status' => 4 // провален
                ]);
            }
        }
        $this->checkLogFile();
        Log::info('-------------------------------------------');
        //Log::info('-------------ResetReservation--------------');
        //Log::info('-------------------------------------------');
    }

    public function checkLogFile()
    {
        $file = storage_path('logs/laravel-'.Carbon::now()->format('Y-m-d').'.log');
        if (File::exists($file)) {
            File::chmod($file, 0777);
        }
    }
}
