<?php

namespace App\Cmf\Project\Site;

use App\Cmf\Core\Defaults\Facades\Page as DefaultPage;
use App\Models\Page;
use App\Models\Post;
use Illuminate\Http\Request;

class Posts extends BaseSite
{
    public function getRecords()
    {
        $this->viewShares([
            'oPosts' => Post::with('type', 'pages')->get()
        ]);
    }

    public function custom(Request $request, $sAction)
    {
        $oPost = DefaultPage::setModel(Post::class)->with(['type', 'pages'])->getById($request, $sAction);

        $this->viewShares([
            'oPost' => $oPost
        ]);
        return $oPost;
    }

    public function hi(array $aData)
    {
        dd('hi');
    }
}