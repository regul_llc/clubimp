<?php

namespace App\Cmf\Project\Site;

use App\Cmf\Core\Defaults\Facades\Page as DefaultPage;
use App\Models\Page;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Helpers\Model;

class Pages extends BaseSite
{
    public function getRecords()
    {
        //dd(Model::init('page')->getStatuses());

        $this->viewShares([
            'oPages' => Page::where('status', 1)->with('posts', 'posts.type')->get(),
            'oPosts' => Post::with('type')->get(),
        ]);
    }

    public function custom(Request $request, $sAction)
    {
        $oPage = DefaultPage::getByUrl($request, $sAction);

        $this->viewShares([
            'oPage' => $oPage
        ]);

        return $oPage;
    }

    public function createPage(array $aData)
    {
        dd($aData);
        $aData = $aData['page'];
        $aCreate = [
            'title' => $aData['title'],
            'url' => str_slug($aData['title']),
            'status' => $aData['status'],
            'seo' => $aData['seo'],
        ];
        $oPage = DefaultPage::create($aCreate);
        if (isset($aData['posts'])) {
            foreach($aData['posts'] as $key => $post) {
                if (isset($post['check'])) {
                    $oPage->posts()->attach($key, [
                        'priority' => $post['priority'],
                        'status' => $post['status'],
                    ]);
                }
            }
        }
        return $this->ajaxSuccess([
            'redirect' => url('/admin/pages/edit/'.$oPage->id)
        ]);
        //return redirect('/admin/pages/edit/'.$oPage->id);
    }

}