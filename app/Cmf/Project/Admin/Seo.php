<?php

namespace App\Cmf\Project\Admin;


use App\Models\Setting;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Validator;

class Seo extends BaseAdmin
{
    public function getRecords()
    {

    }

    public function editSeo(array $aData)
    {
        $validation = Validator::make($aData, [
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'keywords' => 'required|max:255'
        ], [], [
            'title' => 'с Заголовком сайта',
            'description' => 'с Описанием сайта',
            'keywords' => 'с Ключевыми словами'
        ]);
        if($validation->fails()) {
            return response()->json($validation->getMessageBag(),422);
        } else {
            foreach($aData as $key => $value) {
                $oSetting = Setting::where('key', 'site.'.$key)->first();
                if (!is_null($oSetting)) {
                    if ($key === 'favicon') {
                        $value = asset('img/favicon.png');
                    }
                    $oSetting->update([
                        'value' => $value
                    ]);
                }
            }
            Artisan::call('cache:clear');
            return $this->ajaxSuccess([
                'toastr' => ['title' => 'Успех',  'text' => 'Данные успешно изменены', 'type' => 'info']
            ]);
        }
    }
}