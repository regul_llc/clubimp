<?php

namespace App\Cmf\Project\Admin;

use App\Cmf\Core\Defaults\Facades\Page as DefaultPage;
use App\Models\Page;
use App\Models\Post;

class Dashboard extends BaseAdmin
{
    public function getRecords()
    {
        $this->viewShares([
            'oPages' => Page::with('posts', 'posts.type')->paginate(5),
            'oPosts' => Post::with('type')->get(),
        ]);
    }
}