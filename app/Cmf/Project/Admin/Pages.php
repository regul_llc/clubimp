<?php

namespace App\Cmf\Project\Admin;

use App\Cmf\Core\Defaults\Facades\Page as DefaultPage;
use App\Models\Page;
use App\Models\PagePosts;
use App\Models\Post;
use Illuminate\Http\Request;

class Pages extends BaseAdmin
{


    public function getRecords()
    {
        $this->viewShares([
            'oPages' => Page::with('posts', 'posts.type')->paginate(5),
            'oPosts' => Post::with('type')->get(),
        ]);
    }



    public function getEdit($nId)
    {
        $oPage = Page::find($nId);
        $oPosts = Post::all();

        $this->viewShares([
            'oPage' => $oPage,
            'oPosts' => $oPosts
        ]);
    }

    public function editPage(array $aData)
    {
        $aUpdate = $aData['page'];
        $oModel = Page::find($aUpdate['id']);
        if (isset($aUpdate['status']) && intval($aUpdate['status']) === 2) {
            $oPages = Page::where('status', 2)->get();
            foreach($oPages as $oPage) {
                $oPage->update([
                    'status' => 1
                ]);
            }
        }
        $oModel->update([
            'title' => isset($aUpdate['title']) ? $aUpdate['title'] : $oModel->title,
            'status' => isset($aUpdate['status']) ? $aUpdate['status'] : $oModel->status,
            'url' => isset($aUpdate['url']) ? $aUpdate['url'] : str_slug($aUpdate['title']),
            'seo' => isset($aUpdate['seo']) ? $aUpdate['seo'] : $oModel->seo,
        ]);

        return $this->ajaxSuccess([
            'toastr' => ['title' => 'Успех',  'text' => 'Данные успешно изменены', 'type' => 'info']
        ]);
    }

    public function editPagePost(array $aData)
    {
        $aPostUpdate = $aData['post'];
        $oPost = PagePosts::find($aPostUpdate['pivot_id']);
        $oPost->update([
            'status' => isset($aPostUpdate['status']) ? $aPostUpdate['status'] : $oPost->status,
            'priority' => isset($aPostUpdate['priority']) ? $aPostUpdate['priority'] : $oPost->priority,
        ]);

        return $this->ajaxSuccess([
            'toastr' => ['title' => 'Успех',  'text' => 'Данные успешно изменены', 'type' => 'info']
        ]);
    }

    public function appendPagePost(array $aData)
    {
        $aPostUpdate = $aData['post'];
        $oPage = Page::find($aPostUpdate['page_id']);
        $oPage->posts()->attach($aPostUpdate['id'], [
            'priority' => $aPostUpdate['priority'],
            'status' => $aPostUpdate['status'],
        ]);

        return $this->ajaxSuccess([
            'toastr' => ['title' => 'Успех',  'text' => 'Данные успешно изменены', 'type' => 'info']
        ]);
    }

    public function getPagePosts(array $aData)
    {
        $oPage = Page::find($aData['id']);
        return $this->ajaxSuccess([
            'view' => view('admin.pages.edit.posts', [
                'oPage' => $oPage
            ])->render()
        ]);
    }

    public function getPageTable(array $aData)
    {
        $oPage = Page::find($aData['id']);
        return $this->ajaxSuccess([
            'view' => view('admin.pages.edit.components.table', [
                'oPage' => $oPage
            ])->render()
        ]);
    }


    public function createPage(array $aData)
    {
        $aPageUpdate = $aData['page'];
        $oPage = Page::create([
            'title' => $aPageUpdate['title'],
            'status' => $aPageUpdate['status'],
            'url' => str_slug($aPageUpdate['title']),
            'seo' => $aPageUpdate['seo'],
        ]);
        return $this->ajaxSuccess([
            'toastr' => ['title' => 'Успех',  'text' => 'Страница успешно создана', 'type' => 'info'],
            'redirect' => url('/admin/pages/edit/'.$oPage->id)
        ]);
        //return redirect('/admin/pages/edit/'.$oPage->id);
    }



    public function create()
    {
        $this->viewShares([
            'oPages' => Page::with('posts', 'posts.type')->paginate(5),
            'oPosts' => Post::with('type')->get(),
        ]);
        return view('locator',[
            'page' => 'pages.create',
            'action' => 'index'
        ]);
    }

    public function deletePage(array $aData)
    {
        $oPage = Page::find($aData['id']);
        $oPage->delete();

        return redirect('/admin/pages');
    }

    public function deletePagePost(array $aData)
    {
        $oPage = PagePosts::find($aData['id']);
        $oPage->delete();

        return $this->ajaxSuccess([
            'toastr' => ['title' => 'Успех',  'text' => 'Данные успешно удалены', 'type' => 'info']
        ]);
    }




}