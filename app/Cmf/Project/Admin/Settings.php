<?php

namespace App\Cmf\Project\Admin;

use App\Models\Setting;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Validator;

class Settings extends BaseAdmin
{
    public function getRecords()
    {

    }

    public function editSettings(array $aData)
    {
        $validation = Validator::make($aData, [
            'copyright' => 'required|max:255',
            'link' => 'url'
        ], [], [
            'copyright' => 'с Правами на сайт',
            'link' => 'ссылка'
        ]);
        if($validation->fails()) {
            return response()->json($validation->getMessageBag(),422);
        } else {
            foreach($aData as $key => $value) {
                $oSetting = Setting::where('key', 'site.'.$key)->first();
                if (!is_null($oSetting)) {
                    $oSetting->update([
                        'value' => $value
                    ]);
                } else {
                    Setting::create([
                        'key' => 'site.'.$key,
                        'value' => $value
                    ]);
                }
            }
            Artisan::call('cache:clear');
            return $this->ajaxSuccess([
                'toastr' => ['title' => 'Успех',  'text' => 'Данные успешно изменены', 'type' => 'info']
            ]);
        }
    }
}