<?php

namespace App\Cmf\Project\Admin;


use App\Models\User;

class Users extends BaseAdmin
{
    public function getRecords()
    {
        $this->viewShares([
            'oUsers' => User::all()
        ]);
    }

    public function getCustom()
    {
        $this->viewShares([
            'page' => 'admin'
        ]);


        return view('locator');
    }
}