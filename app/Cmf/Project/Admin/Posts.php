<?php

namespace App\Cmf\Project\Admin;

use App\Cmf\Core\Defaults\Facades\Page as DefaultPage;
use App\Models\Page;
use App\Models\PagePosts;
use App\Models\Post;
use App\Models\PostType;
use Illuminate\Http\Request;

class Posts extends BaseAdmin
{
    public function getRecords()
    {
        $this->viewShares([
            'oPages' => Page::with('posts', 'posts.type')->paginate(5),
            'oPosts' => Post::with('type')->get(),
        ]);
    }

    public function types()
    {
        $oPostTypes = PostType::all();
        $this->viewShares([
            'oPostTypes' => $oPostTypes,
        ]);
    }

    public function getSubEdit($nId)
    {
        $oPostType = PostType::find($nId);
        $this->viewShares([
            'oPostType' => $oPostType,
        ]);
    }

    public function getEdit($nId)
    {
        $oPages = Page::all();
        $oPost = Post::find($nId);

        $this->viewShares([
            'oPages' => $oPages,
            'oPost' => $oPost
        ]);
    }

    public function createPost(array $aData)
    {
        $aUpdate = $aData['post'];
        $oPage = Post::create([
            'title' => $aUpdate['title'],
            'post_type_id' => $aUpdate['post_type_id'],
            'status' => $aUpdate['status'],
        ]);
        return redirect('/admin/posts/edit/'.$oPage->id);
    }


    public function editPost(array $aData)
    {
        $aUpdate = $aData['post'];
        $oModel = Post::find($aUpdate['id']);
        if ($oModel->post_type_id !== $aUpdate['post_type_id']) {
            //$aPostUpdate['template'] = PostType::find($aUpdate['post_type_id'])->slug.'.v1';
            $aUpdate['template'] = null;
            $aUpdate['options'] = null;
        }
        $oModel->update([
            'title' => isset($aUpdate['title']) ? $aUpdate['title'] : $oModel->title,
            'post_type_id' => isset($aUpdate['post_type_id']) ? $aUpdate['post_type_id'] : $oModel->post_type_id,
            'status' => isset($aUpdate['status']) ? $aUpdate['status'] : $oModel->status,
            'options' => (isset($aUpdate['options']) || is_null($aUpdate['options'])) ? $aUpdate['options'] : $oModel->options,
            'template' => (isset($aUpdate['template']) || is_null($aUpdate['template'])) ? $aUpdate['template'] : $oModel->template,
        ]);

        return $this->ajaxSuccess([
            'toastr' => ['title' => 'Успех',  'text' => 'Данные успешно изменены', 'type' => 'info']
        ]);
    }

    public function editPostType(array $aData)
    {
        $aUpdate = $aData['post'];
        $oModel = PostType::find($aUpdate['id']);
        $oModel->update([
            'title' => isset($aUpdate['title']) ? $aUpdate['title'] : $oModel->title,
            'slug' => isset($aUpdate['slug']) ? $aUpdate['slug'] : $oModel->slug,
        ]);
        return back();
    }

    public function editPostTemplate(array $aData)
    {
        $aUpdate = $aData['post'];
        $oModel = Post::find($aUpdate['id']);
        $oModel->update([
            'template' => isset($aUpdate['template']) ? $aUpdate['template'] : $oModel->template,
        ]);
        return $this->ajaxSuccess([
            'toastr' => ['title' => 'Успех',  'text' => 'Данные успешно изменены', 'type' => 'info']
        ]);
    }

    public function editPostOptions(array $aData)
    {
        $aUpdate = $aData['post'];
        $oModel = Post::find($aUpdate['id']);
        $oModel->update([
            'options' => isset($aUpdate['options']) ? $aUpdate['options'] : $oModel->options
        ]);

        return $this->ajaxSuccess([
            'toastr' => ['title' => 'Успех',  'text' => 'Данные успешно изменены', 'type' => 'info']
        ]);
    }

    public function editPostOptionsBlock(array $aData)
    {
        $aUpdate = $aData['post'];
        $aOptions = [];
        $properties = [];
        foreach($aUpdate['options']['one'] as $key => $item) {
            foreach($aUpdate['options']['one'][$key] as $subKey => $value) {
                $aOptions[$subKey][$key] = $value;
                if (!in_array($key, $properties)) {
                    $properties[] = $key;
                }
            }
        }
        $deleteKey = 0;
        foreach($aOptions as $key => $aOption) {
            foreach($properties as $property) {
                if (empty($aOption[$property]) || is_null($aOption[$property])) {
                    $deleteKey++;
                }
            }
            if (count($aOption) === $deleteKey) {
                unset($aOptions[$key]);
                $deleteKey = 0;
            }
        }
        $oModel = Post::find($aUpdate['id']);
        $aOptions = array_merge([
            'title' => $oModel->options['title'],
            'menu' => $oModel->options['menu']
        ], [
            'one' => $aOptions
        ]);
        $oModel->update([
            'options' => $aOptions
        ]);

        return $this->ajaxSuccess([
            'toastr' => ['title' => 'Успех',  'text' => 'Данные успешно изменены', 'type' => 'info']
        ]);
    }

    public function deletePost(array $aData)
    {
        $oPost = Post::find($aData['id']);
        $oPost->delete();
        return redirect('/admin/posts');
    }

    public function getPostTable(array $aData)
    {
        $oPost = Post::find($aData['id']);
        return $this->ajaxSuccess([
            'view' => view('admin.posts.edit.components.table', [
                'oPost' => $oPost
            ])->render()
        ]);
    }

    public function getPostRules(array $aData)
    {
        $oPost = Post::find($aData['id']);
        return $this->ajaxSuccess([
            'view' => view('admin.posts.edit.rules', [
                'oPost' => $oPost
            ])->render()
        ]);
    }

    public function getPostTemplate(array $aData)
    {
        $oPost = Post::find($aData['id']);
        return $this->ajaxSuccess([
            'view' => view('admin.posts.edit.template', [
                'oPost' => $oPost
            ])->render()
        ]);
    }
}