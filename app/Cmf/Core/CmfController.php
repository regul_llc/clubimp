<?php

namespace App\Cmf\Core;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Barryvdh\DomPDF\Facade as PDF;
use KekoApp\LaravelMetaTags\Facades\MetaTag;

abstract class CmfController extends Controller
{
    protected $sModelPath = '';
    protected $sView = '';
    protected $locator = '';

    protected $defaultTemplate = 'index';



    protected function getModel($sModel)
    {
        $model = Str::camel($sModel);
        $sModelName = $this->sModelPath.ucfirst($model);
        if(!class_exists($sModelName)){
            return null;
        }
        return new $sModelName();
    }

    public function getDisplay($model)
    {
        $oModel = $this->getModel($model);
        if ($oModel) {
            $aFields = $oModel->aFields;
            $aRows = $oModel->getRecords();
            $sTemplateName = ($oModel->getTemplate()) ? $oModel->getTemplate() : $this->defaultTemplate;
            return view($this->locator,[
                'page' => $model,
                'action' => $sTemplateName
            ])->with('aFields', $aFields)->with('aRows', $aRows);
        } else {
            return view($this->locator,[
                'page' => 'error',
                'status' => '404'
            ]);
        }
    }

    public function getRecords(Request $request, $model)
    {
        $oModel = $this->getModel($model);
        if ($oModel){
            $aFields = $oModel->aFields;
            $aOrder = ($request->get('order',[]));
            $sSearch = ($request->get('search',[])) ? $request->get('search')['value'] : '';
            $aRows = $oModel->getRecords($request->get('start', 0), $request->get('length', 10), $aOrder, $sSearch);
            $aRows['draw'] = $request->get('draw',1);
        } else {
            $aFields = [];
            $aRows = [];
        }
        return $aRows;
    }

    public function getCustomPage(Request $request, $model, $action)
    {
        $oModel = $this->getModel($model);
        $aData = $oModel->prepareData($request->input());
        $methodAction = Str::camel($action);
        if(method_exists($oModel, $methodAction)) {
            $oModel->$methodAction($request);
            return view($this->locator,[
                'page' => $model.'.'.$action,
                'action' => $this->defaultTemplate
            ]);

        } elseif(method_exists($oModel, 'custom')) {
            $oPage = $oModel->custom($request, $action);
            if (!is_null($oPage)) {
                $aFields = $oModel->aFields;
                $aRows = $oModel->getRecords();
                MetaTag::set('title', $oPage->title);
                if (isset($oPage->seo['description'])) {
                    MetaTag::set('description', $oPage->seo['description']);
                }
                if (isset($oPage->seo['keywords'])) {
                    MetaTag::set('keywords', $oPage->seo['keywords']);
                }


                return view($this->locator,[
                    'page' => $model,
                    'action' => $this->defaultTemplate
                ])->with('aFields', $aFields)->with('aRows', $aRows);
            } else {
                return view($this->locator,[
                    'page' => 'error',
                    'status' => '404'
                ]);
            }
        } else {
            if (View::exists('admin.'.$model.'.'.$action.'.index')) {
                return view($this->locator,[
                    'page' => $model.'.'.$action,
                    'action' => 'index'
                ]);
            } else {
                return view($this->locator,[
                    'page' => $model,
                    'action' => $methodAction
                ]);
            }

        }
    }


















    public function getEdit($model, $nId, $action = null)
    {
        $oModel = $this->getModel($model);
        if ($oModel){
            $model = $model.'.edit';
            $aData = $oModel->getEdit($nId);
            $sTemplateName = ($oModel->getTemplate()) ? $oModel->getTemplate() : $this->defaultTemplate;
            if (!is_null($action) && View::exists('admin.'.$model.'.'.$action.'.index')) {
                return view($this->locator,[
                    'page' => $model.'.'.$action,
                    'action' => $sTemplateName
                ])->with('aFields', $oModel->aFields)->with('aData', $aData );
            } else {
                return view($this->locator,[
                    'page' => $model,
                    'action' => $sTemplateName
                ])->with('aFields', $oModel->aFields)->with('aData', $aData );
            }
        } else {
            return view('errors.404');
        }
    }

    public function getSubEdit($model, $subModel, $nId, $action = null)
    {
        $oModel = $this->getModel($model);
        if ($oModel){
            $model = $model.'.'.$subModel.'.edit';
            $aData = $oModel->getSubEdit($nId);
            $sTemplateName = ($oModel->getTemplate()) ? $oModel->getTemplate() : $this->defaultTemplate;
            if (!is_null($action) && View::exists('admin.'.$model.'.'.$action.'.index')) {
                return view($this->locator,[
                    'page' => $model.'.'.$action,
                    'action' => $sTemplateName
                ])->with('aFields', $oModel->aFields)->with('aData', $aData );
            } else {
                return view($this->locator,[
                    'page' => $model,
                    'action' => $sTemplateName
                ])->with('aFields', $oModel->aFields)->with('aData', $aData );
            }
        } else {
            return view('errors.404');
        }
    }



    public function postCustomAction(Request $request, $model, $action)
    {
        $oModel = $this->getModel($model);
        $aData = $oModel->prepareData($request->input());
        $action = Str::camel($action);
        $mAnswer = $oModel->$action($aData);
        return $mAnswer;
    }

    public function postPhotos(Request $request, $model, $nId)
    {
        $oModel = $this->getModel($model);
        $aData = $oModel->prepareData($request->input());
        $aResult = $oModel->setPhotos($request->file('photos'), $nId);
        return $aResult;
    }

    public function getPdf(Request $request, $model, $id, $action)
    {
        $oModel = $this->getModel($model);
        $pdf = $oModel->getPdf($id);
        if ($oModel && $action == 'stream') {
            $sFileName = $oModel->getPdf($id, 'name');
            return $pdf->stream($sFileName);
        } elseif($oModel && $action == 'download') {
            $sFileName = $oModel->getPdf($id, 'name');
            return $pdf->download($sFileName);
        } else {
            return view('errors.404');
        }
    }
}
