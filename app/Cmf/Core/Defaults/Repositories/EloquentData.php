<?php

namespace App\Cmf\Core\Defaults\Repositories;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class EloquentData extends AbstractData implements DataInterface
{
    private $model = null;


    public function __construct($class)
    {
        $this->model = $class;
    }


    public function get(Request $request, $type = null, $value = null)
    {
        $model = $this->model;
        $oModel = $model::where($type, $value);
        return $oModel;
    }

    public function set()
    {

    }

    public function create(array $aCreateData, array $aUpdateData = null)
    {
        $model = $this->model;
        if (!is_null($aUpdateData)) {
            $oModel = $model::updateOrCreate($aCreateData, $aUpdateData);
        } else {
            $oModel = $model::create($aCreateData);
        }
        return $oModel;
    }

    public function update($oModel, array $aUpdateData)
    {
        $oModel->update($aUpdateData);
        return $oModel;
    }

    public function delete($oModel)
    {
        $oModel->delete($oModel);
        return $this;
    }


}