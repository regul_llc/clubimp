<?php

namespace App\Cmf\Core\Defaults\Repositories;

use Illuminate\Http\Request;


interface DataInterface
{
    public function __construct($class);

    public function get(Request $request, $type = null, $value = null);

    public function set();

    public function delete($oModel);

    public function create(array $aCreateData, array $aUpdateData = null);

    public function update($oModel, array $aUpdateData);
}