<?php

namespace App\Cmf\Core\Defaults;

use App\Cmf\Core\Defaults\Repositories\CacheData;
use App\Cmf\Core\Defaults\Repositories\EloquentData;
use App\Models\Page as PageModel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class Page
{
    private $model = null;
    private $sModelClass = '';

    private $with = [
        'posts',
        'posts.type',
    ];

    /**
     * type => eloquent|cache
     * @var array
     */
    private $settings = [];




    public function __construct()
    {
        $this->settings = config('project.output');
        $this->sModelClass = PageModel::class;
        $this->setModel();
    }

    /**
     * Подключение драйвера для работы с моделью.
     *
     * @param null $class
     * @return $this
     */
    public function setModel($class = null)
    {
        if (is_null($class)) {
            switch($this->settings['type']) {
                case 'eloquent':
                    $this->model = new EloquentData($this->sModelClass);
                    break;
                case 'cache':
                    $this->model = new CacheData($this->sModelClass);
                    break;
                default:
                    $this->model = new EloquentData($this->sModelClass);
                    break;
            }
        } else {
            $this->model = new EloquentData($class);
        }
        return $this;
    }

    public function with(array $with)
    {
        $this->with = $with;
        return $this;
    }

    public function getByUrl(Request $request, $url)
    {
        $oModel = $this->model->get($request, 'url', $url);
        return $this->getQuery($oModel);
    }

    public function getById(Request $request, $id)
    {
        $oModel = $this->model->get($request, 'id', $id);
        return $this->getQuery($oModel);
    }

    public function getQuery(Builder $oModel)
    {
        $oModel = $oModel->where('status', 1);
        return $oModel->with($this->with)->first();
    }

    public function create(array $aCreateData, array $aUpdateData = null)
    {
        $oModel = $this->model->create($aCreateData, $aUpdateData);
        return $oModel;
    }
    public function update($oModel, array $aUpdateData)
    {
        $oModel = $this->model->update($oModel, $aUpdateData);
        return $oModel;
    }
    public function delete($oModel)
    {
        $oModel = $this->model->delete($oModel);
        return $oModel;
    }




}