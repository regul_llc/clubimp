<?php

namespace App\Cmf\Core\Defaults\Facades;

use Illuminate\Support\Facades\Facade;

use App\Cmf\Core\Defaults\Page as PageDefaults;

class Page extends Facade
{
    protected static function getFacadeAccessor()
    {
        return PageDefaults::class;
    }
}