<?php

namespace App\Cmf\Core;

use Illuminate\Support\Facades\View;
use App\Registries\MemberRegistry;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

abstract class CmfWeb
{
    public $modelName = '';

    public $modelNameSpace = 'App\Models\\';

    public $aFields = [];

    public $dbModel = null;

    public $bIsEditable = true;

    public $bIsDeletable = true;

    public $bIsAddable = false;

    public $aColumns = [];

    public $aEditFields = [];

    public $sDisplayType = 'table';

    protected $sCurrentTemplate = null;

    protected $oUser;

    protected $aBodyCls = ['is-default-class'];

    protected $member = null;

    public function __construct()
    {
        $this->oUser = Sentinel::getUser();
        $this->member = MemberRegistry::getInstance();

        if($this->modelName) {
            //$sModelName = $this->modelNameSpace . $this->modelName;
            //$this->dbModel = new $sModelName();
            //$this->aColumns = Schema::getColumnListing($this->dbModel->getTable());
        }
    }

    protected function viewShares(array $aShares)
    {
        foreach($aShares as $key => $value) {
            View::share($key, $value);
        }
    }

    protected function addBodyCls(array $aClassNames)
    {
        foreach($aClassNames as $value) {
            $this->aBodyCls[] = $value;
        }
    }

    public function __destruct()
    {
        $sBodyCls = implode(' ', $this->aBodyCls);
        View::share('sBodyCls', $sBodyCls);
    }

    public function prepareData(array $aData)
    {
        unset($aData['_token']);
        return $aData;
    }

    public function getTemplate()
    {
        return $this->sCurrentTemplate;
    }

    public function setTemplate($sTemplateName)
    {
        $this->sCurrentTemplate = $sTemplateName;
    }


    public function ajaxSuccess(array $aData = [])
    {
        return array_merge([
            'success' => true
        ], $aData);
    }

    public function ajaxError(array $aData = [])
    {
        return array_merge([
            'success' => false
        ], $aData);
    }

    public function jsonError($text, $key = 'error')
    {
        return response()->json([
            $key => [$text]
        ], 422);
    }


}
