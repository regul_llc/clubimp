<?php

namespace App\Cmf\Core;

use Route;

abstract class CmfRoute
{
    public static function resource($name, $controller, array $options = [])
    {
        Route::get('/', $controller.'@index');

        Route::get('{model}', [
            'as'   => 'model',
            'uses' => $controller.'@getDisplay'
        ]);
        Route::post('{model}/action/{sAction}', [
            'as'   => 'model.action.post',
            'uses' => $controller.'@postCustomAction',
        ]);
        Route::get('{model}/page/{sAction}', [
            'as'   => 'model.page',
            'uses' => $controller.'@getCustomPage',
        ]);
        Route::get('{model}/{sAction}', [
            'as'   => 'model.page',
            'uses' => $controller.'@getCustomPage',
        ]);
        Route::get('{model}/edit/{nId}', [
            'as'   => $name.'.model.edit',
            'uses' => $controller.'@getEdit',
        ]);
        Route::get('{model}/{subModel}/edit/{nId}', [
            'as'   => $name.'.model.edit',
            'uses' => $controller.'@getSubEdit',
        ]);
        Route::get('{model}/edit/{nId}/{sAction}', [
            'as'   => $name.'.model.edit',
            'uses' => $controller.'@getEdit',
        ]);










        Route::get('getrecords/{model}', [
            'as'   => $name.'.model',
            'uses' => $controller.'@getRecords'
        ]);

        Route::get('get-records-ajax/{model}', [
            'as'   => $name.'.model',
            'uses' => $controller.'@getRecordsAjax'
        ]);

        Route::get('custom/{model}/{action}', [
            'as'   => $name.'.model',
            'uses' => $controller.'@getCustomData'
        ]);

        Route::post('custom/{model}/{action}', [
            'as'   => $name.'.model',
            'uses' => $controller.'@postCustomData'
        ]);





        Route::post('setoption', [
            'as'   => $name.'.model',
            'uses' => $controller.'@setUserOption'
        ]);





        Route::post('{model}/edit/{nId}', [
            'as'   => $name.'.model.save',
            'uses' => $controller.'@postSave',
        ]);

        Route::get('{model}/create', [
            'as'   => $name.'.model.create',
            'uses' => $controller.'@getCreate',
        ]);

        Route::post('{model}/create', [
            'as'   => $name.'.model.create',
            'uses' => $controller.'@postCreate',
        ]);

        Route::post('{model}/{modelId}', [
            'as'   => $name.'.model.update',
            'uses' => $controller.'@postUpdate',
        ]);

        Route::delete('{model}/{modelId}', [
            'as'   => $name.'.model.destroy',
            'uses' => $controller.'@delete',
        ]);

        Route::post('{model}/photos/{modelId}', [
            'as'   => $name.'.model.uploadPhotos',
            'uses' => $controller.'@postPhotos',
        ]);

        Route::get('{model}/photos/{modelId}', [
            'as'   => $name.'.model.getPhotos',
            'uses' => $controller.'@getPhotos',
        ]);

        Route::get('{model}/pdf/invoice/{id}/{action}', [
            'as'   => $name.'.model.getPdf',
            'uses' => $controller.'@getPdf',
        ]);
    }
}