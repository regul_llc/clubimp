<?php

namespace App\Helpers;

class Model
{
    /**
     * Create new class object for get attributes in views
     *
     * @param $class
     * @return Model object|null
     */
    public static function init($class)
    {
        if (class_exists($class)) {
            return new $class();
        } else {
            $class = '\App\Models\\'.studly_case($class);
            if (class_exists($class)) {
                return new $class();
            } else {
                return null;
            }
        }
    }
}