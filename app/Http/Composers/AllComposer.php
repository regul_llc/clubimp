<?php

namespace App\Http\Composers;

use App\Models\PostType;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Route;
use App\Registries\MemberRegistry;
use Illuminate\Support\Facades\Cache;
use App\Models\Page;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use App\Models\Setting;

class AllComposer
{
    private $member;
    private $locator;
    private $settings;
    private $aPostTypes;
    private $templates;

    public function __construct(Route $route)
    {
        $this->member = MemberRegistry::getInstance();

        $this->locator = stristr(Route::current()->getName(),'.', true);

        $this->settings = Cache::remember('settings', 60, function() {
            $array = [];
            $oSettings = Setting::where('status', 1)->whereNotNull('value')->get();
            foreach($oSettings as $oSetting) {
                $key = stristr($oSetting->key,'.', true);
                if ($key) {
                    $sub_key = substr(stristr($oSetting->key,'.'), 1);
                    $array[$key][$sub_key] = $oSetting->value;
                } else {
                    $array[$oSetting->key] = $oSetting->value;
                }
            }
            return $array;
        });

        $this->aPostTypes = Cache::remember('post_types', 60, function() {
            return PostType::all()->toArray();
        });

        $this->templates = $this->getFiles(storage_path('../resources/views/layouts/templates'), 'templates');
        //dd($this->templates);

        /*
        $url = explode('/', stristr(URL::current(), $this->locator));

        dd($url);

        if (in_array($this->locator, $url)) {
            unset($url[array_search($this->locator, $url)]);
            sort($url);
        }
        dd($url);
        */
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('aComposerMember', $this->member);
        $view->with('sComposerLocator', $this->locator);
        $view->with('aComposerSettings', $this->settings);
        $view->with('aComposerPostTypes', $this->aPostTypes);
        $view->with('aComposerTemplates', $this->templates);
    }


    public function getFiles($dir, $slug)
    {
        $files = File::allFiles($dir);
        $paths = [];
        $envServer = env('APP_SERVER', false);
        $slash = $envServer ? '/' : '\\';
        foreach ($files as $file) {
            $item = stristr(stristr(File::dirname((string)$file), $slug), $slash).$slash.File::basename((string)$file);
            $item = substr($item, 1);
            $paths[] = $item;
        }
        $paths = array_unique($paths);
        $pages = [];

        foreach($paths as $path) {
            $array = explode($slash, $path);
            if (isset($array[2])) {
                $pages[$array[0].'.'.$array[1]][] = substr(str_replace('blade.php', '', $array[2]), 0, -1);
            } else {
                $pages[$array[0]][] = substr(str_replace('blade.php', '', $array[1]), 0, -1);
            }

        }
        return $pages;
    }


}