<?php

namespace App\Http\Middleware;

use Closure;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Checkpoints\ActivationCheckpoint;
use Illuminate\Support\Facades\Route;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;

class AuthenticateSentinel
{
    /**
     * The guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * AuthenticateSentinel constructor.
     * @param Sentinel $auth
     */
    public function __construct(Sentinel $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $sentinel = $this->auth;
        try {
            return $sentinel::guest() ? redirect()->route('auth.login') : $next($request);
        } catch (NotActivatedException $e) {
            return $next($request);
        }

    }
}