<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Illuminate\Support\Facades\Route;

class RedirectIfAuthenticated
{

    protected $auth;

    /**
     * RedirectIfAuthenticated constructor.
     * @param Sentinel $auth
     */
    public function __construct(Sentinel $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $sentinel = $this->auth;
        try {
            return $sentinel::guest() ? $next($request) : redirect()->guest('admin');
        } catch (NotActivatedException $e) {
            return $next($request);
        }
    }
}
