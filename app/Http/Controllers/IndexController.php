<?php

namespace App\Http\Controllers;

use App\Mail\SendOrderToClient;
use App\Models\Client;
use App\Models\Event;
use App\Models\Payment;
use App\Models\Place;
use App\Models\Reservation;
use App\Models\Section;
use App\Services\Cashier;
use Illuminate\Http\Request;
use App\Services\ReservationService;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class IndexController extends Controller
{
    /**
     * Экземпляр @see \App\Services\ReservationService
     *
     * @var ReservationService|null
     */
    private $oReservationService = null;


    /**
     * IndexController constructor.
     */
    public function __construct()
    {
        $this->oReservationService = new ReservationService();
    }


    /**
     * Основной отбор ближайщих мероприятий
     *
     * Главная
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $oEvent = Event::next()->first();
        if (is_null($oEvent)) {
            return view('site.index');
        }
        $this->oReservationService->setUniqueSessionId($oEvent);
        $oPlan = $oEvent->plan;
        $oPlaces = $this->getPlacesByPlan($oPlan);

        $aCachePlaces = $this->oReservationService->getCacheReservation($oEvent);
        $aPlacesId = [];
        if (!is_null($aCachePlaces)) {
            $aPlacesId = array_keys($aCachePlaces);
        }
        $aPlacesId = array_merge($aPlacesId, $this->oReservationService->getReservations($oEvent));


        $aClientCachePlaces = $this->oReservationService->getCacheReservationClient($oEvent);

        foreach($this->setVersionByPlan($oPlan) as $key => $share) {
            View::share($key, $share);
        }

        return view('site.index', [
            'oEvent' => $oEvent,
            'oPlan' => $oPlan,
            'oPlaces' => $oPlaces,
            'aPlacesId' => $aPlacesId,
            'aClientCachePlaces' => $aClientCachePlaces,
            'oEvents' => Event::with('plan')->next()->take(4)->orderBy('beginning_at', 'asc')->get()->reject(function($oItem) use ($oEvent) {
                 return $oItem->id === $oEvent->id;
            })
        ]);
    }

    /**
     * Конкретное мероптияние на основе id и даты
     *
     * По url Route::get('/event/{id}/{date}'
     * data = $oNextEvent->beginning_at->format('d-m-Y')
     *
     * Должно быть тоже самое что и @see \App\Http\Controllers\IndexController::index()
     *
     * @param $id
     * @param $date
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function event($id, $date)
    {
        $oEvent = Event::find($id);
        if (is_null($oEvent)) {
            return view('site.index');
        }
        $this->oReservationService->setUniqueSessionId($oEvent);
        $oPlan = $oEvent->plan;
        $oPlaces = $this->getPlacesByPlan($oPlan);

        $aCachePlaces = $this->oReservationService->getCacheReservation($oEvent);
        $aPlacesId = [];
        if (!is_null($aCachePlaces)) {
            $aPlacesId = array_keys($aCachePlaces);
        }
        $aPlacesId = array_merge($aPlacesId, $this->oReservationService->getReservations($oEvent));


        $aClientCachePlaces = $this->oReservationService->getCacheReservationClient($oEvent);

        foreach($this->setVersionByPlan($oPlan) as $key => $share) {
            View::share($key, $share);
        }

        return view('site.index', [
            'oEvent' => $oEvent,
            'oPlan' => $oPlan,
            'oPlaces' => $oPlaces,
            'aPlacesId' => $aPlacesId,
            'aClientCachePlaces' => $aClientCachePlaces,
            'oEvents' => Event::with('plan')->next()->take(4)->orderBy('beginning_at', 'asc')->get()->reject(function($oItem) use ($oEvent) {
                return $oItem->id === $oEvent->id;
            })
        ]);
    }

    /**
     * Отдебажить распарсенные места
     *
     * @param $type
     */
    public function debug($type)
    {
        $oEvent = Event::next()->first();
        if (is_null($oEvent)) {
            dd('Следующее мероприятие не найдено');
        }
        $this->oReservationService->setUniqueSessionId($oEvent);
        $oPlan = $oEvent->plan;
        $oPlaces = $this->getPlacesByPlan($oPlan);

        $aCachePlaces = $this->oReservationService->getCacheReservation($oEvent);
        $aPlacesId = [];
        if (!is_null($aCachePlaces)) {
            $aPlacesId = array_keys($aCachePlaces);
        }
        $aPlacesId = array_merge($aPlacesId, $this->oReservationService->getReservations($oEvent));

        dd($oPlaces);
    }

    /**
     * Выбрать событие, ближайшее или по request
     *
     * @param Request $request
     * @return mixed
     */
    private function getEvent(Request $request)
    {
        if ($request->exists('event_id')) {
            $oEvent = Event::find($request->get('event_id'));
        } else {
            $oEvent = Event::next()->first();
        }
        return $oEvent;
    }

    /**
     * Отрендерить план, для динамики
     *
     *
     * @param Request $request
     * @return array
     * @throws \Throwable
     */
    public function plan(Request $request)
    {
        $oEvent = $this->getEvent($request);
        if (is_null($oEvent)) {
            dd('Следующее мероприятие не найдено');
        }
        $oPlan = $oEvent->plan;

        $oPlaces = $this->getPlacesByPlan($oPlan);

        $aCachePlaces = $this->oReservationService->getCacheReservation($oEvent);
        $aPlacesId = [];
        if (!is_null($aCachePlaces)) {
            $aPlacesId = array_keys($aCachePlaces);
        }
        $aPlacesId = array_merge($aPlacesId, $this->oReservationService->getReservations($oEvent));
        $aClientCachePlaces = $this->oReservationService->getCacheReservationClient($oEvent);
        //dd($oPlaces->where('section.name', 't1')->max('num'));
        if (!empty($aClientCachePlaces)) {
            $aHiddenPlaces = Place::with('section')->whereIn('id', $aClientCachePlaces)->get();
            $sHiddenPlaces = '';
            foreach($aHiddenPlaces as $aHiddenPlace) {
                $sHiddenPlaces .= $aHiddenPlace->section->name.'-'.$aHiddenPlace->row.'-'.$aHiddenPlace->num.';';
            }
        }


        return [
            'success' => true,
            'view' => view('site.'.$oPlan->tpl, [
                'oEvent' => $oEvent,
                'oPlan' => $oPlan,
                'oPlaces' => $oPlaces,
                'aPlacesId' => $aPlacesId,
                'aClientCachePlaces' => $aClientCachePlaces,
                'sHiddenPlaces' => isset($sHiddenPlaces) ? $sHiddenPlaces : null
            ])->render()
        ];
    }


    /**
     * Отправить в шаблон стили по плану
     * plan_3:
     * - __1 класс для секции
     * - __wide класс для шикорого формата
     *
     * @param $oPlan
     * @return array
     */
    private function setVersionByPlan($oPlan)
    {
        $aStyleShares = [];
        switch($oPlan->name) {
            case 'plan_1':
                $aStyleShares['shareClassWide'] = '__wide';
                break;
            case 'plan_3':
                $aStyleShares['shareClassPlan'] = '__1';
                $aStyleShares['shareClassWide'] = '__wide';
                break;
            default:
                break;
        }
        return $aStyleShares;
    }

    /**
     * Форма 1, выбор, проверка мест
     *
     * @param Request $request
     * @return array
     * @throws \Throwable
     */
    public function check(Request $request)
    {
        $oEvent = $this->getEvent($request);
        if (is_null($oEvent)) {
            dd('Следующее мероприятие не найдено');
        }
        $oPlan = $oEvent->plan;

        $aCorrectPlaces = $this->parsePlaces($request->get('places'));
        $aCachePlaces = $this->getDataForCachePlaces($oEvent, $aCorrectPlaces);
        $check = true;
        $aPlacesId = [];
        if (!empty($aCachePlaces)) {
            $check = $this->oReservationService->checkSimpleCacheReservation($aCachePlaces);
            if ($check) {
                $aPlacesId = $this->oReservationService->setSimpleCacheReservation($aCachePlaces);
            }
        }
        if (!empty($aPlacesId)) {
            $oPlaces = Place::with('section')->whereIn('id', $aPlacesId)->get();
            return [
                'success' => true,
                'check' => $check,
                'view' => view('site.components.contact.places', [
                    'oPlaces' => $oPlaces
                ])->render()
            ];
        } else {
            return [
                'success' => true,
                'check' => $check,
            ];
        }
    }

    /**
     * Отправить форму с контактами и начать оплачивать
     *
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function contact(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'uniqid' => 'required',
            'name' => 'required',
            'phone' => 'required|phone',
            'email' => 'required|email',
            'checkbox' => 'required',
            'paymentType' => 'required',
        ], [
            'uniqid.required' => 'Ошибка сессии, попробуйте перезагрузить страницу.',
            'phone.phone' => 'Некорректный номер телефона.',
            'paymentType.required' => 'Необходимо выбрать способ оплаты.',
            'checkbox.required' => 'Необходимо дать согласие на обработку Ваших персональных данных.',
        ], [
            'name' => 'ФИО',
            'phone' => 'Телефон',
            'email' => 'E-mail',
            'checkbox' => 'Согласие'
        ]);
        if($validation->fails()) {
            $aMessages = $validation->getMessageBag()->toArray();
            if (isset($aMessages['uniqid'])) {
                return response()->json([
                    'error' => $aMessages['uniqid']
                ],422);
            }
            return response()->json($validation->getMessageBag(),422);
        } else {
            $oEvent = $this->getEvent($request);
            if (is_null($oEvent)) {
                dd('Следующее мероприятие не найдено');
            }
            $oClient = Client::create([
                'name' => $request->get('name'),
                'phone' => preg_replace("/[^0-9]/",'',$request->get('phone')),
                'email' => $request->get('email'),
            ]);
            $oPayment = $this->oReservationService->setCachePlacesBySession($oClient, $oEvent, $request->get('uniqid'));
            if (!is_null($oPayment)) {
                $aReturn = $this->paymentForm($oPayment, $oEvent, $request->get('paymentType'));
                if (isset($aReturn['cps'])) {
                    $oPayment->update([
                        'cps' => $aReturn['cps']
                    ]);
                    unset($aReturn['cps']);
                }
                if (isset($aReturn['type']) && $aReturn['type'] === 'yandex') {
                    $oPayment->update([
                        'api_id' => 1
                    ]);
                }
                return array_merge([
                    'success' => true,
                    'check' => true
                ], $aReturn);
            } else {
                return response()->json([
                    'error' => 'Ошибка создания платежа, попробуйте перезагрузить страницу.'
                ],422);
            }
        }
    }


    /**
     * Кэшированные места на основе брониваний
     *
     * @param $oEvent
     * @param $aCorrectPlaces
     * @return array
     */
    public function getDataForCachePlaces($oEvent, $aCorrectPlaces)
    {
        $aCachePlaces = [];
        foreach($aCorrectPlaces as $aCorrectPlace) {
            $oSection = Section::where('name', $aCorrectPlace['section'])->where('plan_id', $oEvent->plan_id)->first();
            if (is_null($oSection)) {
                dd('Section not found');
            }
            $oPlace = Place::where('section_id', $oSection->id)
                ->where('plan_id', $oEvent->plan_id)
                ->where('row', $aCorrectPlace['row'])
                ->where('num', $aCorrectPlace['num'])
                ->where('status', 1)
                ->first();

            if (is_null($oPlace)) {
                dd('Place not found');
            }
            $oReservation = Reservation::where('place_id', $oPlace->id)
                ->where(function($q) {
                    $q->where('status', 0)
                        ->orWhere('status', 1);
                })->where('event_id', $oEvent->id)->first();
            if (is_null($oReservation)) {
                $aCachePlaces[] = [
                    'event_id' => $oEvent->id,
                    'place_id' => $oPlace->id,
                ];
            }
        }
        return $aCachePlaces;
    }

    /**
     * Спарсить места из request для удобного вида
     *
     * @param $sPlaces
     * @return array
     */
    public function parsePlaces($sPlaces)
    {
        $aPlaces = explode(';', $sPlaces);
        $aCorrectPlaces = [];
        foreach($aPlaces as $key => $aPlace) {
            if (empty($aPlace)) {
                unset($aPlaces[$key]);
                continue;
            }
            $aTempPlace = explode('-', $aPlace);
            if (isset($aTempPlace[0])) {
                $aCorrectPlaces[$key]['section'] = $aTempPlace[0];
            }
            if (isset($aTempPlace[1])) {
                $aCorrectPlaces[$key]['row'] = $aTempPlace[1];
            }
            if (isset($aTempPlace[2])) {
                $aCorrectPlaces[$key]['num'] = $aTempPlace[2];
            }
        }
        return $aCorrectPlaces;
    }

    /**
     * Получить сортированный массив мест по названию плана
     *
     * @param $oPlan
     * @return array
     */
    public function getPlacesByPlan($oPlan)
    {
        switch($oPlan->name) {
            case 'plan_1':
                return $this->getPlacesByPlan1($oPlan);
            case 'plan_2':
                return $this->getPlacesByPlan2($oPlan);
            case 'plan_3':
                return $this->getPlacesByPlan3($oPlan);
            default:
                return [];
        }
    }

    /**
     * Сортировка для плана просп. Михаила Нагибина, 3Г
     *
     * @param $oPlan
     * @return array
     */
    public function getPlacesByPlan1($oPlan)
    {
        $oPlaces = Place::with('section', 'plan')
            ->where('plan_id', $oPlan->id)
            ->get()
            ->groupBy('section.name');

        $aPlaces = [];
        foreach($oPlaces as $key => $oPlace) {
            if ($key === 'p') {
                $aPlaces[$key] = $oPlace->sortByDesc('row')->groupBy('row');
                $aTmpPlaces = [];
                $oOrderPlaces = $aPlaces[$key]->sortBy('row');
                foreach($oOrderPlaces as $subKey => $oRow) {
                    $oOrderRow = $oRow->sortBy('num');
                    foreach($oOrderRow as $oSubPlace) {
                        if ($oSubPlace->num < 11) {
                            $aTmpPlaces[0][$subKey][] = $oSubPlace;
                        } else {
                            $aTmpPlaces[1][$subKey][] = $oSubPlace;
                        }
                    }
                }
                krsort($aTmpPlaces[0]);
                krsort($aTmpPlaces[1]);
                $aPlaces[$key] = $aTmpPlaces;
            } else {
                $aTempPlaces = $oPlace->sortByDesc('row')->groupBy('row');
                $aTmpPlaces = [];
                $oOrderPlaces = $aTempPlaces->sortBy('row');
                foreach($oOrderPlaces as $subKey => $oRow) {
                    $oOrderRow = $oRow->sortBy('num');
                    foreach($oOrderRow as $oSubPlace) {
                        $aTmpPlaces[$subKey][] = $oSubPlace;
                    }
                }
                krsort($aTmpPlaces);
                $aPlaces[$key] = $aTmpPlaces;
            }
        }
        return $aPlaces;
    }

    /**
     * Сортировка для плана Коммунаров 52
     *
     * @param $oPlan
     * @return array
     */
    public function getPlacesByPlan2($oPlan)
    {
        $oPlaces = Place::with('section', 'plan')
            ->where('plan_id', $oPlan->id)
            ->get()
            ->groupBy('section.name');


        $aPlaces = [];
        $maxCount = 0;
        foreach($oPlaces as $key => $oPlace) {
            $aPlaces[$key] = $oPlace->sortByDesc('row')->groupBy('row');
            foreach($aPlaces[$key] as $item) {
                if (count($item) > $maxCount) {
                    $maxCount = count($item);
                }
            }
            $aPlaces[$key.'-count'] = $maxCount;
        }

        $aTmpPlaces = [];
        foreach($aPlaces['p'] as $key => $oRow) {
            for($i = 0; $i < $aPlaces['p-count']; $i++) {
                $issetPlace = $oRow->where('num', ($i + 1))->first();
                if (is_null($issetPlace)) {
                    $aTmpPlaces[$key][$i + 1] = null;
                } else {
                    $aTmpPlaces[$key][$i + 1] = $issetPlace;
                }
            }

        }
        $aPlaces['p'] = $aTmpPlaces;
        return $aPlaces;
    }

    /**
     * Сортировка для плана Большая Садовая 51, бывший к/т Победа
     *
     * @param $oPlan
     * @return array
     */
    public function getPlacesByPlan3($oPlan)
    {
        $oPlaces = Place::with('section', 'plan')
            ->where('plan_id', $oPlan->id)
            ->get()
            ->groupBy('section.name');


        $aPlaces = [];

        foreach($oPlaces as $key => $oPlace) {
            $maxCount = 0;
            $aPlaces[$key] = $oPlace->sortByDesc('row')->groupBy('row');
            foreach($aPlaces[$key] as $item) {
                if (count($item) > $maxCount) {
                    $maxCount = count($item);
                }
            }
            $aPlaces[$key.'-count'] = $maxCount;
        }
        $aTmpPlaces = [];
        foreach($aPlaces['p'] as $key => $oRow) {
            for ($i = 0; $i < $aPlaces['p-count']; $i++) {
                $issetPlace = $oRow->where('num', ($i + 1))->first();
                if (is_null($issetPlace)) {
                    $aTmpPlaces[$key][$i + 1] = null;
                } else {
                    $aTmpPlaces[$key][$i + 1] = $issetPlace;
                }
            }
        }
        $aPlaces['p'] = $aTmpPlaces;

        /*
         * Костыль для первого ряда.
         *
         * Места, по базе 1 место и т.д.
         * 1 2 3 4 5 6 ... 19 null null
         * должны быть
         * null 1 2 3 4 5 ... 20 null
         * т.е. 1 место - 1 место, но находится во второй колонке
         */
        $aFirstRowPlaces = [];
        array_unshift($aPlaces['p']['1'], null);
        array_pop($aPlaces['p']['1']);
        for($i = 0; $i < $aPlaces['p-count']; $i++) {
            $aFirstRowPlaces[$i + 1] = isset($aPlaces['p']['1'][$i]) ? $aPlaces['p']['1'][$i] : null;
        }

        $aTmpPlaces = [];
        foreach($aPlaces['b'] as $key => $oRow) {
            for($i = 0; $i < $aPlaces['b-count']; $i++) {
                $issetPlace = $oRow->where('num', ($i + 1))->first();
                if (is_null($issetPlace)) {
                    $aTmpPlaces[$key][$i + 1] = null;
                } else {
                    $aTmpPlaces[$key][$i + 1] = $issetPlace;
                }
            }
        }
        $aPlaces['b'] = $aTmpPlaces;
        return $aPlaces;
    }


    /**
     * Рендер формы на основе типа оплаты/метода оплаты(api|быстрая оплата) и платежки
     *
     * @param $oPayment
     * @param $oEvent
     * @param $sPaymentType
     * @return array
     * @throws \Throwable
     */
    public function paymentForm($oPayment, $oEvent, $sPaymentType)
    {
        $cashier = new Cashier();
        if (Config::get('yandex.form') === 'api') {
            $form = $cashier->setPayment($oPayment->price, $oPayment->purpose);
            if (isset($form['cps'])) {
                $cps = $form['cps'];
                unset($form['cps']);
            }
        } else {
            $form = [
                'form' => view('defaults.app.yandex.quickpay', [
                    'oEvent' => $oEvent,
                    'oPayment' => $oPayment,
                    'sPaymentType' => $sPaymentType
                ])->render()
            ];
        }
        return [
            'post' => $form,
            'cps' => isset($cps) ? $cps : null
        ];
    }

    /**
     * Редирект с сессией об успешной оплате
     *
     * Возможно не используется!
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function payment(Request $request)
    {
        if ($request->exists('success') && $request->exists('cps_context_id')) {
            //$cashier = new Cashier();
            //$fSum = $cashier->updateYandexPayment($request->get('cps_context_id'), $request->all());
            //$this->oReservationService->successPaymentReservation($request->get('cps_context_id'));
            Session::flash('success_payment', 'true');
            return redirect()->route('index.index');
        } else {
            return redirect()->route('index.index');
        }
    }

    /**
     * Прием яндекс запроса уведомления о получении средств
     *
     * @param Request $request
     * @return string|void
     */
    public function notification(Request $request)
    {
        Log::info('--------notification----------');
        Log::info(json_encode($request->all()));
        Log::info('--------notification----------');
        $aData = $request->all();
        $cashier = new Cashier();
        $result = $cashier->getNotification()->check($aData);
        if ($result['success']) {
            $success = $this->oReservationService->setPaymentStatus($result['payId'], 'success');
            return 'ok';
        } else {
            $success = $this->oReservationService->setPaymentStatus($result['payId'], 'error');
            if ($success) {
                return 'ok';
            }
            return abort('500', 'Error pay');
        }
    }

    /**
     * Тестовая отправка email по url
     */
    public function email()
    {
        $oPayment = Payment::first();
        $oReservations = $oPayment->reservations;
        /*
        if (!empty($oReservations) && !empty($oReservations[0])) {
            $oEvent = Event::find($oReservations->first()->id);
            $aPlacesId = [];
            foreach($oReservations as $oReservation) {
                $aPlacesId[] = $oReservation->place_id;
            }
            $oPlaces = Place::with('section')->whereIn('id', $aPlacesId)->get();

            return view('emails.client', [
                'oPlaces' => $oPlaces,
                'oEvent' => $oEvent,
            ]);
        }
        */



        $this->oReservationService->sendEmail($oReservations);
        dd('Send successful');
    }
}
