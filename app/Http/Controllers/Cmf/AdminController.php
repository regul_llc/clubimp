<?php

namespace App\Http\Controllers\Cmf;

use Illuminate\Http\Request;
use App\Cmf\Core\CmfController;

class AdminController extends CmfController
{
    protected $sModelPath = 'App\Cmf\Project\Admin\\';
    protected $sView = 'admin';
    protected $locator = 'locator';

    public function index()
    {
        return view($this->locator, [
            'page' => 'home',
            'action' => 'index'
        ]);
    }
}
