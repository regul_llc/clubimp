<?php

namespace App\Http\Controllers\Cmf;

use Illuminate\Http\Request;
use App\Cmf\Core\CmfController;
use App\Models\Page;

class SiteController extends CmfController
{
    protected $sModelPath = 'App\Cmf\Project\Site\\';
    protected $sView = 'site';
    protected $locator = 'locator';

    public function index()
    {
        $oPage = Page::where('status', 2)->first();
        if (!is_null($oPage)) {
            return view('locator', [
                'page' => 'pages',
                'action' => 'index',
                'oPage' => $oPage
            ]);
        } else {
            return view('welcome');
        }

    }
}
