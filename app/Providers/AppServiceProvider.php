<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * -------------------------------------
     * Schema::defaultStringLength(191);
     * https://laravel-news.com/laravel-5-4-key-too-long-error
     * -------------------------------------
     * Error migrations for laravel 5.4 for long unique key.
     * Error: "Syntax error or access violation: 1071 Specified key was too long; max key length is 767 bytes".
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Validator::extend('phone', function ($attribute, $value, $parameters, $validator) {
            return intval($value) < 99999999999 && intval($value) > 1000000000;
        });

        Blade::directive('prod', function ($beta) {
            return "<?php if (app()->environment('production')): ?>";
        });
        Blade::directive('endprod', function ($beta) {
            return "<?php endif; ?>";
        });
        Blade::directive('ifDebug', function ($beta) {
            return "<?php if (config('app.debug')): ?>";
        });
        Blade::directive('endifDebug', function ($beta) {
            return "<?php endif; ?>";
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
