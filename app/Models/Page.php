<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'url', 'status', 'seo'
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Страница не опубликована',
        1 => 'Страница опубликована',
        2 => 'Страница главная',
    ];

    protected $statusIcons = [
        0 => [
            'class' => 'badge badge-default',
            'title' => 'Не активно'
        ],
        1 => [
            'class' => 'badge badge-success',
            'title' => 'Активно'
        ],
        2 => [
            'class' => 'badge badge-primary',
            'title' => 'Главная'
        ]
    ];

    public function getStatuses()
    {
        return $this->statuses;
    }

    public function posts()
    {
        return $this->BelongsToMany(Post::class, 'page_posts')
            ->withPivot('page_id', 'priority', 'status', 'id')
            ->orderBy('priority', 'desc')
            ->withTimestamps();
    }

    public function activePosts()
    {
        return $this->BelongsToMany(Post::class, 'page_posts')
            ->withPivot('page_id', 'priority', 'status')
            ->where('page_posts.status', 1)
            ->where('posts.status', 1)
            ->orderBy('priority', 'desc')
            ->withTimestamps();
    }

    public function inactivePosts()
    {
        return $this->BelongsToMany(Post::class, 'page_posts')
            ->withPivot('page_id', 'priority', 'status')
            ->where('page_posts.status', 0)
            ->orderBy('priority', 'desc')
            ->withTimestamps();
    }

    public function newPosts()
    {
        return $this->BelongsToMany(Post::class, 'page_posts')->withPivot('page_id')->orderBy('created_at', 'desc');
    }


    /**
     * Get priority by this pivot value
     *
     * @return mixed
     */
    public function getPivotStatusAttribute()
    {
        return $this->pivot->status;
    }

    /**
     * Accessor for get text pivot status
     *
     * @return string
     */
    public function getPivotStatusTextAttribute()
    {
        switch($this->pivot->status) {
            case 0:
                return 'Пост не опубликован на этой странице';
                break;
            case 1:
                return 'Пост опубликован на этой странице';
                break;
            default:
                return 'Статус неизвестен';
                break;
        }
    }

    /**
     * Accessor for get text status
     *
     * @return string
     */
    public function getStatusTextAttribute()
    {
        return $this->statuses[$this->status];
    }

    /**
     * Accessor for get text status
     *
     * @return string
     */
    public function getStatusIconAttribute()
    {
        return $this->statusIcons[$this->status];
    }

    public function getSeoAttribute($value) {
        $result = json_decode($value, true);
        return ($result && is_array($result)) ? collect($result) : collect();
    }

    public function setSeoAttribute($value) {
        if(is_array($value)) {
            $this->attributes['seo'] = json_encode($value, JSON_NUMERIC_CHECK | JSON_UNESCAPED_UNICODE);
        } elseif (is_string($value) && json_decode($value)) {
            $this->attributes['seo'] = $value;
        }
    }
}
