<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Services\Modelable\Statusable;
use App\Services\Modelable\DateFormatable;

class Event extends Model
{
    use Statusable;
    use DateFormatable;

    protected $table = 'events';

    protected $dates = [
        'beginning_at',
        'ticket_sale_to_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'plan_id', 'beginning_at', 'ticket_sale_to_at', 'price', 'status',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Не активное',
        1 => 'Активное',
    ];

    public function plan()
    {
        return $this->hasOne(Plan::class, 'id', 'plan_id');
    }

    public function scopeNext($query)
    {
        $query->where('status', 1)->where('beginning_at', '>', Carbon::now());
    }

    public function getBeginningAtEventAttribute()
    {
        return $this->dmYHi($this->beginning_at);
    }

}
