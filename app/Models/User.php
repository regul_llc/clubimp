<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cartalyst\Sentinel\Activations\EloquentActivation as SentinelEloquentActivation;
use Cartalyst\Sentinel\Reminders\EloquentReminder as SentinelEloquentReminder;
use Cartalyst\Sentinel\Users\EloquentUser;

class User extends EloquentUser
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'permissions', 'first_name', 'last_name', 'status',
    ];

    protected $dates = [
        'last_login',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Пользователь заблокирован',
        1 => 'Пользователь активирован',
    ];

    protected $statusIcons = [
        0 => [
            'class' => 'badge badge-default',
            'title' => 'Inactive'
        ],
        1 => [
            'class' => 'badge badge-success',
            'title' => 'Active'
        ]
    ];

    public function getStatuses()
    {
        return $this->statuses;
    }

    /**
     * Login by phone
     * @var array
     */
    protected $loginNames = ['email'];

    public function getRoleAttribute()
    {
        return $this->roles()->first();
    }

    /**
     * User activation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function activation()
    {
        return $this->hasOne(SentinelEloquentActivation::class, 'user_id', 'id');
    }

    /**
     * User reminder
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function reminder()
    {
        return $this->hasOne(SentinelEloquentReminder::class, 'user_id', 'id');
    }

    /**
     * Accessor for get text status
     *
     * @return string
     */
    public function getStatusTextAttribute()
    {
        return $this->statuses[$this->status];
    }

    /**
     * Accessor for get text status
     *
     * @return string
     */
    public function getStatusIconAttribute()
    {
        return $this->statusIcons[$this->status];
    }


}
