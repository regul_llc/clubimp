<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostType extends Model
{
    protected $table = 'post_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug', 'title', 'rules'
    ];

    /**
     * Get all pages when use post
     *
     * @return $this
     */
    public function posts()
    {
        return $this->hasMany(Post::class, 'post_type_id', 'id');
    }

    public function setRulesAttribute($value)
    {
        $this->attributes['rules'] = json_encode($value);
    }

    public function getRulesAttribute($value)
    {
        return json_decode($value ,true);
    }

}
