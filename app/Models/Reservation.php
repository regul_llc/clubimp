<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Modelable\Statusable;

class Reservation extends Model
{
    use Statusable;

    protected $table = 'reservations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_id', 'client_id', 'place_id', 'code', 'mark', 'payment_id', 'status',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'В процессе',
        1 => 'Забронировано',
        2 => 'Анулировано',
        3 => 'Анулировано', // tmp
        4 => 'Провален',
        5 => 'Тест'
    ];

    public function event()
    {
        return $this->hasOne(Event::class, 'id', 'event_id');
    }

    public function client()
    {
        return $this->hasOne(Client::class, 'id', 'client_id');
    }

    public function place()
    {
        return $this->hasOne(Place::class, 'id', 'place_id');
    }

    public function payment()
    {
        return $this->hasOne(Payment::class, 'id', 'payment_id');
    }

    public function setCode()
    {
        $oPlace = $this->place;
        $oSection = $oPlace->section;
        $this->code = $oSection->name.'-'.$oPlace->row.'-'.$oPlace->num;
        $this->save();
    }
}
