<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Modelable\Statusable;

class Payment extends Model
{
    use Statusable;

    protected $table = 'payments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type_id', 'price', 'purpose', 'status',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Не оплачен',
        1 => 'Оплачен',
        2 => 'Аннулирован',
        3 => 'Аннулирован', // tmp
        4 => 'Провален',
        5 => 'Тест'
    ];

    protected $types = [
        1 => 'Оплата'
    ];
    

    public function reservations()
    {
        return $this->hasMany(Reservation::class, 'payment_id', 'id');
    }
}
