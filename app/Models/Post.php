<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_type_id', 'title', 'status', 'template', 'options'
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Пост не опубликован',
        1 => 'Пост опубликован',
    ];

    protected $statusIcons = [
        0 => [
            'class' => 'badge badge-default',
            'title' => 'Не активно'
        ],
        1 => [
            'class' => 'badge badge-success',
            'title' => 'Активно'
        ]
    ];

    public function getStatuses()
    {
        return $this->statuses;
    }

    /**
     * Type attribute
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function type()
    {
        return $this->hasOne(PostType::class, 'id', 'post_type_id');
    }

    /**
     * Select first page when use post
     *
     * @return mixed
     */
    public function page()
    {
        $pagePost = $this->hasOne(PagePosts::class, 'post_id', 'id');
        return $pagePost->getResults()->hasOne(Page::class, 'id', 'page_id');
    }

    /**
     * Get all pages when use post
     *
     * @return $this
     */
    public function pages()
    {
        return $this->BelongsToMany(Page::class, 'page_posts')
            ->withPivot('post_id', 'priority', 'status')
            ->orderBy('priority', 'desc')
            ->withTimestamps();
    }


    /**
     * Get priority value this post in current page.
     * use: $oPost->priority($oPage)
     *
     * @param $page
     * @return int
     */
    public function priority($page)
    {
        $priority = $this->pagePosts()->where('page_id', $page->id)->first();
        return intval($priority->priority);
    }

    /**
     * Get pivot table, use only for $this->priority($page)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pagePosts()
    {
        return $this->hasMany(PagePosts::class, 'post_id', 'id');
    }


    /**
     * Get priority by this pivot value
     *
     * @return mixed
     */
    public function getPriorityAttribute()
    {
        return $this->pivot->priority;
    }

    /**
     * Get priority by this pivot value
     *
     * @return mixed
     */
    public function getPivotStatusAttribute()
    {
        return $this->pivot->status;
    }

    /**
     * Accessor for get text status
     *
     * @return string
     */
    public function getStatusTextAttribute()
    {
        return $this->statuses[$this->status];
    }

    /**
     * Accessor for get text pivot status
     *
     * @return string
     */
    public function getPivotStatusTextAttribute()
    {
        switch($this->pivot->status) {
            case 0:
                return 'Пост не опубликован на этой странице';
                break;
            case 1:
                return 'Пост опубликован на этой странице';
                break;
            default:
                return 'Статус неизвестен';
                break;
        }
    }

    /**
     * Accessor for get text status
     *
     * @return string
     */
    public function getStatusIconAttribute()
    {
        return $this->statusIcons[$this->status];
    }

    /**
     * Accessor for get text status
     *
     * @return string
     */
    public function getPivotStatusIconAttribute()
    {
        return $this->statusIcons[$this->pivot->status];
    }


    public function setOptionsAttribute($value)
    {
        $this->attributes['options'] = json_encode($value);
    }

    public function getOptionsAttribute($value)
    {
        return json_decode($value ,true);
    }

}
