<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Modelable\Statusable;

class Plan extends Model
{
    use Statusable;

    protected $table = 'plans';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'title', 'tpl', 'address', 'address_title', 'coordinates', 'status',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Не активный',
        1 => 'Активный',
    ];

    public function event()
    {
        return $this->belongsTo(Event::class, 'plain_id', 'id');
    }

    public function setCoordinatesAttribute($value)
    {

        if (is_array($value) && count($value) === 2) {
            $coordinates['lon'] = isset($value['lon']) ? $value['lon'] : $value[0];
            $coordinates['lat'] = isset($value['lat']) ? $value['lat'] : $value[1];
            $value = $coordinates;
        } else {
            $value = [
                'lon' => 0,
                'lat' => 0,
            ];
        }
        $this->attributes['coordinates'] = json_encode($value);
    }

    public function getCoordinatesAttribute($value)
    {
        return json_decode($value ,true);
    }

}
