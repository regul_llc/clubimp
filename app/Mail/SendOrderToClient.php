<?php

namespace App\Mail;

use App\Models\Event;
use App\Models\Payment;
use App\Models\Place;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Config;

class SendOrderToClient extends Mailable
{
    use Queueable, SerializesModels;

    private $fromAddress = null;
    private $fromName = null;

    private $oEvent = null;
    private $oPlaces = null;
    private $mark = null;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($oReservations)
    {
        $this->fromAddress = Config::get('mail.from.address');
        $this->fromName = Config::get('mail.from.name');

        if (!empty($oReservations) && !empty($oReservations[0])) {
            $oFirstReservation = $oReservations->first();

            $this->oEvent = Event::with('plan')->where('id', $oFirstReservation->event_id)->first();
            $aPlacesId = [];
            foreach($oReservations as $oReservation) {
                $aPlacesId[] = $oReservation->place_id;
            }
            $this->oPlaces = Place::with('section')->whereIn('id', $aPlacesId)->get();
            $this->mark = $oFirstReservation->mark;
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('emails.client')->with([
            'oEvent' => $this->oEvent,
            'oPlaces' => $this->oPlaces,
            'mark' => $this->mark
        ])->subject('Бронирование билетов на концерт Клуб импровизации Для своих.');
    }
}
