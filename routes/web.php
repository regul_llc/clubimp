<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'domain' => '',
    'as' => 'auth.',
    'prefix' => 'auth',
    'middleware' => ['guest'],
    'namespace' => 'Auth'
], function () {
    Route::get('/login',                    ['uses' => 'LoginController@showLoginForm',                'as' => 'login']);
    Route::post('/login',                   ['uses' => 'LoginController@login',                        'as' => 'login.post']);

    Route::get('/register',                 ['uses' => 'RegisterController@showRegistrationForm',      'as' => 'register']);
    Route::post('/register',                ['uses' => 'RegisterController@register',                  'as' => 'register.post']);

    Route::get('/password/reset',           ['uses' => 'ForgotPasswordController@showLinkRequestForm', 'as' => 'password.request']);
    Route::post('/password/email',          ['uses' => 'ForgotPasswordController@sendResetLinkEmail',  'as' => 'password.email.post']);
    Route::get('/password/reset/{token}',   ['uses' => 'ResetPasswordController@showResetForm',        'as' => 'password.reset']);
    Route::post('/password/reset',          ['uses' => 'ResetPasswordController@reset',                'as' => 'password.reset.post']);



});

//Route::get('/',         ['uses' => 'IndexController@index',                'as' => 'site.index']);
Route::get('/home',     ['uses' => 'HomeController@index',                 'as' => 'index.home']);


Route::group([
    'domain' => '',
    'as' => 'admin.',
    'prefix' => 'admin',
    'middleware' => ['admin'],
    'namespace' => 'Auth'
], function () {
    Route::post('/logout',                    ['uses' => 'LoginController@logout',                'as' => 'logout']);
});

Route::group([
    'domain' => '',
    'as' => 'admin.',
    'prefix' => 'admin',
    'middleware' => ['admin'],
    'namespace' => 'Cmf'
], function () {
    CmfRoute::resource('admin', 'AdminController');
});

Route::group([
    'domain' => '',
    'as' => 'site.',
    'prefix' => '',
    'middleware' => [],
    'namespace' => 'Cmf'
], function () {

    //CmfRoute::resource('site', 'SiteController');

    Route::get('/',         ['uses' => 'IndexController@index',                 'as' => 'index.index']);
});

Route::group([
    'domain' => '',
    'as' => 'index.',
    'prefix' => '',
    'middleware' => [],
], function () {

    Route::get('/',                 ['uses' => 'IndexController@index',                 'as' => 'index']);
    Route::get('/event/{id}/{date}',      ['uses' => 'IndexController@event',                 'as' => 'event']);
    Route::get('/debug/{type}',     ['uses' => 'IndexController@debug',                 'as' => 'debug']);
    Route::get('/payment',          ['uses' => 'IndexController@payment',                 'as' => 'payment']);
    Route::get('/payment/notification',          ['uses' => 'IndexController@notification',  'as' => 'payment.notification']);
    Route::post('/payment/notification',          ['uses' => 'IndexController@notification',  'as' => 'payment.notification.post']);
    Route::post('/payment',          ['uses' => 'IndexController@payment',                 'as' => 'payment.post']);
    Route::post('/plan',            ['uses' => 'IndexController@plan',                  'as' => 'plan.post']);
    Route::post('/check',           ['uses' => 'IndexController@check',                 'as' => 'check.post']);
    Route::post('/contact',         ['uses' => 'IndexController@contact',               'as' => 'contact.post']);

    Route::get('/email',         ['uses' => 'IndexController@email',               'as' => 'contact.email']);
});




/*
|--------------------------------------------------------------------------
| Development debugbar assets error
|--------------------------------------------------------------------------
|
|
*/

Route::get('/_debugbar/assets/stylesheets', [
    'as' => 'debugbar-css',
    'uses' => '\Barryvdh\Debugbar\Controllers\AssetController@css'
]);

Route::get('/_debugbar/assets/javascript', [
    'as' => 'debugbar-js',
    'uses' => '\Barryvdh\Debugbar\Controllers\AssetController@js'
]);