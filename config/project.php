<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Output
    |--------------------------------------------------------------------------
    |
    | type => eloquent|cache
    |
    */
    'output' => [
        'type' => 'eloquent'
    ],


    /*
    |--------------------------------------------------------------------------
    | Roles for project
    |--------------------------------------------------------------------------
    |
    | Usage for seeding column roles in /database/seeds/...
    | After change: php artisan db:seed
    |
    */
    'roles' => [
        [
            'slug' => 'admin',
            'name' => 'Администратор'
        ],
        [
            'slug' => 'user',
            'name' => 'Пользователь'
        ]
    ],


    /*
    |--------------------------------------------------------------------------
    | Post Types for project
    |--------------------------------------------------------------------------
    |
    | Usage for seeding column roles in /database/seeds/...
    | After change: php artisan db:seed
    |
    */
    'post_types' => [
        [
            'slug' => 'header',
            'title' => 'Хэдер',
            'rules' => [
                'menu' => 'string',
                'title' => 'string',
                'description' => 'text'
            ]
        ],
        [
            'slug' => 'contacts',
            'title' => 'Контакты',
            'rules' => [
                'menu' => 'string',
                'title' => 'string',
                'description' => 'text',
                'address' => 'string',
                'phone' => 'integer'
            ]
        ],
        [
            'slug' => 'slider.simple',
            'title' => 'Простой слайдер',
            'rules' => [
                'menu' => 'string',
                'title' => 'string'
            ]
        ],
        [
            'slug' => 'static',
            'title' => 'Статичный блок',
            'rules' => [
                'menu' => 'string',
                'title' => 'string'
            ]
        ],
        [
            'slug' => 'blocks',
            'title' => 'Блоки',
            'rules' => [
                'menu' => 'string',
                'title' => 'string',
                'one' => [
                    'title' => 'string',
                    'description' => 'text',
                    'icon' => 'string'
                ]
            ]
        ]
    ],

    'menu' => [
        'admin' => [
            '/'         => ['title' => 'Домой',  'icon' => 'icon-home', 'badge' => 'info|new'],
            'dashboard' => ['title' => 'Панель управления',  'icon' => 'icon-speedometer'],
            'users'     => ['title' => 'Пользователи',  'icon' => 'icon-people'],
            'seo'       => ['title' => 'Сео',  'icon' => 'icon-globe'],
            'pages'   => [
                'title' => 'Страницы',  'icon' => 'icon-folder',
                'sub' => [
                    '/' => ['title' => 'Все',  'icon' => 'icon-speedometer'],
                    'create' => ['title' => 'Создать',  'icon' => 'icon-speedometer'],
                    'edit' => ['title' => 'Изменить',  'icon' => 'icon-speedometer', 'hidden' => '1'],
                ]
            ],
            'posts'   => [
                'title' => 'Посты',  'icon' => 'icon-doc',
                'sub' => [
                    '/' => ['title' => 'Все',  'icon' => 'icon-speedometer'],
                    'create' => ['title' => 'Создать',  'icon' => 'icon-speedometer'],
                    'edit' => ['title' => 'Изменить',  'icon' => 'icon-speedometer', 'hidden' => '1'],
                    'types' => ['title' => 'Типы',  'icon' => 'icon-speedometer'],
                ]
            ],
            'settings' => [
                'title' => 'Настройки',
                'icon' => 'icon-settings',
                'sub' => [
                    'site' => ['title' => 'Сайт',  'icon' => 'icon-people'],
                    'users' => ['title' => 'Пользователи',  'icon' => 'icon-people'],
                ]
            ],
        ],
        'site' => [
            '/'     => ['title' => 'Site',   'icon' => ''],
            'pages' => ['title' => 'Pages',  'icon' => ''],
            'posts' => ['title' => 'Posts',  'icon' => ''],
        ]
    ],

    'layout' => [
        'admin'     => 'admin.app',
        'site'      => 'app'
    ]




];