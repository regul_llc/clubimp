<?php

return [
    'client_id'             => env('YANDEX_CLIENT_ID'),
    'client_secret'         => env('YANDEX_CLIENT_SECRET'),
    'access_token'          => env('YANDEX_ACCESS_TOKEN'),
    'money_wallet'          => env('YANDEX_MONEY_WALLET'),
    'redirect_uri'          => env('YANDEX_REDIRECT_URI'),
    'notification_secret'   => env('YANDEX_NOTIFICATION_SECRET'),


    /*
     |--------------------------------------------------------------------------
     | Form type for pay
     |--------------------------------------------------------------------------
     |
     | Supported: "quickpay", "api"
     |
     */

    'form' => env('YANDEX_FORM_TYPE', 'quickpay'),
];