<table class="table table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>Title</th>
        <th>Status</th>
        <th>Post title</th>
        <th>Post type</th>
        <th>Post status</th>
        <th>Post priority in this page</th>
        <th>Post status in this page</th>
    </tr>
    </thead>
    <tbody>
    @foreach($oPages as $oPage)
        @foreach($oPage->posts as $key => $oPost)
            <tr>
                @if($key === 0)
                    <td rowspan="{{ count($oPage->posts) }}">{{ $oPage->id }}</td>
                    <td rowspan="{{ count($oPage->posts) }}">
                        <a href="/site/pages/{{ $oPage->url }}">
                            {{ $oPage->title }}
                        </a>
                    </td>
                    <td rowspan="{{ count($oPage->posts) }}">
                        {{ $oPage->status }}
                        <span title="{{ $oPage->status_text }}" style="cursor: pointer;">[?]</span>
                    </td>
                @endif
                <td>{{ $oPost->title }}</td>
                <td>{{ $oPost->type->title }}</td>
                <td>
                    {{ $oPost->status }}
                    <span title="{{ $oPost->status_text }}" style="cursor: pointer;">[?]</span>
                </td>
                <td>{{ $oPost->priority }}</td>
                <td>
                    {{ $oPost->pivot_status }}
                    <span title="{{ $oPost->pivot_status_text }}" style="cursor: pointer;">[?]</span>
                </td>
            </tr>
        @endforeach
    @endforeach
    </tbody>
</table>