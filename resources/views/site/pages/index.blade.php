@if(isset($oPage))
    @foreach($oPage->activePosts as $key => $oPost)
        @if(View::exists('layouts.templates.'.$oPost->template))
            @include('layouts.templates.'.$oPost->template, [
                'aOptions' => $oPost->options
            ])
        @endif
    @endforeach
@endif