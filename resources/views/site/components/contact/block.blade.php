<!-- CONTACT -->
<section id="contact" class="module-sm contacs hidden">

    <!-- MODULE DIVIDER -->
    <a class="striped-icon divider inner-scroll" href="#contact">
        <i class="fa fa-envelope-o"></i>
    </a>
    <!-- /MODULE DIVIDER -->

    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <h3 class="module-subtitle">Забронируй / Купи / Радуйся</h3>
                {{--<h4 class="">Следующее мероприятие 25 июля 2017г. в 20:00</h4>--}}
                <h4 class="">Билеты на концерт - {{ $oPlan->address_title }} {{ $oEvent->beginning_at_event }}</h4>
            </div>

            <div class="col-sm-8 col-sm-offset-2">
                <!-- CONTACT FORM -->
                <form class="ajax-form"
                      action="{{ route('index.contact.post', ['event_id' => $oEvent->id]) }}"
                      {{--action="{{ route('index.payment.post') }}"--}}
                      {{--action="https://m.money.yandex.ru/internal/public-api/to-payment-type"--}}
                      method="post"
                      data-show=".message-cont.payment-process"
                      data-hide=".send-cont"
                      data-callback="showHideAfterSuccess"
                >
                    @if(Session::exists('reservation_id-event_'.$oEvent->id))
                        <input type="hidden" name="uniqid" value="{{ Session::get('reservation_id-event_'.$oEvent->id) }}">
                    @endif
                    <div class="row">
                        <div class="send-cont">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="sr-only" for="subject">ФИО</label>
                                    <input type="text" id="subject" class="form-control" name="name" placeholder="ФИО" required>
                                    {{--<p class="help-block text-danger"></p>--}}
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="sr-only" for="phone">Телефон</label>
                                    <input class="form-control" data-role="js-mask-phone-int" type="text" id="phone" name="phone" placeholder="Телефон" required>
                                    {{--<p class="help-block text-danger"></p>--}}
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="sr-only" for="email">E-mail</label>
                                    <input type="email" id="email" name="email" class="form-control" placeholder="E-mail" required>
                                    {{--<p class="help-block text-danger"></p>--}}
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group __check">
                                    <label>
                                        <input type="checkbox" name="checkbox" id="check">
                                        <div class="custom-checkbox"><i class="fa fa-check" aria-hidden="true"></i></div>
                                        <label class="internal" for="check" style="text-align: left;padding-left: 10px;">Даю согласие на обработку моих персональных данных. {{-- в соответствии с условиями <a href="#">публичной оферты</a> --}}</label>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-8 col-sm-offset-2">
                                <div class="form-group" data-radio-danger-name="paymentType" style="margin-bottom: 10px;text-align: center;">
                                    <label for="" style="font-size: 16px;width: 100%;">Выберите удобный для Вас способ оплаты</label>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group __check">
                                            <label>
                                                <input class="hidden-radio" type="radio" name="paymentType" id="paymentType1" value="PC">
                                                <div class="custom-checkbox"><i class="fa fa-check" aria-hidden="true"></i></div>
                                                <label class="internal" for="paymentType1" style="text-align: left;padding-left: 10px;width: inherit;">Яндекс.Деньгами</label>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group __check">
                                            <label>
                                                <input class="hidden-radio" type="radio" name="paymentType" id="paymentType2" value="AC" checked>
                                                <div class="custom-checkbox"><i class="fa fa-check" aria-hidden="true"></i></div>
                                                <label class="internal" for="paymentType2" style="text-align: left;padding-left: 10px;width: inherit;">Банковской картой</label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="text-center">
                                    <p>На указанный Вами <b>email</b> придет письмо с подтверждением бронирования и вашими местами.</p>
                                </div>
                                <div class="text-center order-places-description">

                                </div>
                            </div>
                            <div class="col-sm-12" style="position: relative">
                                <button type="button" class="btn btn-base ajax-link" action="{{ route('index.plan.post', ['event_id' => $oEvent->id]) }}"
                                        data-pagination="1"
                                        data-pagination-container=".plan-container"
                                        data-show=".module.booking"
                                        data-hide="#contact"
                                        data-without-check="1"
                                        data-callback="showHideAfterSuccess"
                                        style="position: absolute;left: 0;height: 44px;bottom: 0;"
                                        title="Назад"
                                >
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 477.175 477.175" style="enable-background:new 0 0 477.175 477.175;" xml:space="preserve" width="18px" height="18px">
                                            <g>
                                                <path d="M145.188,238.575l215.5-215.5c5.3-5.3,5.3-13.8,0-19.1s-13.8-5.3-19.1,0l-225.1,225.1c-5.3,5.3-5.3,13.8,0,19.1l225.1,225   c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4c5.3-5.3,5.3-13.8,0-19.1L145.188,238.575z" fill="#FFFFFF"/>
                                            </g>
                                            </svg>
                                        </span>
                                </button>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-base inner-form-submit">
                                        <span>Отправить</span>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="message-cont payment-process hidden">
                            <div class="col-sm-12">
                                <div class="form-group __check ">
                                    <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                    <p>Вы временно забронировали места.</p>
                                </div>
                            </div>
                        </div>
                        <div class="message-cont hidden">
                            <div class="col-sm-12">
                                <div class="form-group __check ">
                                    <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                    <p>Вы успешно забронировали места. Ждем вас на концерте.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
                <!-- /CONTACT FORM -->

                <!-- Ajax response -->
                <div id="contact-response" class="ajax-response"></div>

            </div>

        </div><!-- .row -->

    </div>
    @ifDebug
        <div class="text-center" style="color: #ff1e6a; padding: 50px; font-size: 20px;">
            {{ trans('site.debug.sale') }}
        </div>
    @endifDebug
</section>
<!-- /CONTACT -->