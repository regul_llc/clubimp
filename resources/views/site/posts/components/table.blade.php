<table class="table table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>Title</th>
        <th>Type</th>
        <th>Status</th>
        <th>Pages</th>
        <th>Page status</th>
        <th>Post priority in Page</th>
        <th>Post status in Page</th>
    </tr>
    </thead>
    <tbody>
    @foreach($oPosts as $oPost)
        @foreach($oPost->pages as $key => $oPage)
            <tr>
                @if($key === 0)
                    <td rowspan="{{ count($oPost->pages) }}">{{ $oPost->id }}</td>
                    <td rowspan="{{ count($oPost->pages) }}">
                        <a href="/site/posts/{{ $oPost->id }}">
                            {{ $oPost->title }}
                        </a>
                    </td>
                    <td rowspan="{{ count($oPost->pages) }}">
                        {{ $oPost->type->title }}
                    </td>
                    <td rowspan="{{ count($oPost->pages) }}">
                        {{ $oPost->status }}
                        <span title="{{ $oPost->status_text }}" style="cursor: pointer;">[?]</span>
                    </td>
                @endif
                <td>{{ $oPage->title }}</td>
                <td>
                    {{ $oPage->status }}
                    <span title="{{ $oPage->status_text }}" style="cursor: pointer;">[?]</span>
                </td>
                <td>{{ $oPage->pivot->priority }}</td>
                <td>
                    {{ $oPage->pivot_status }}
                    <span title="{{ $oPage->pivot_status_text }}" style="cursor: pointer;">[?]</span>
                </td>
            </tr>
        @endforeach
    @endforeach
    </tbody>
</table>