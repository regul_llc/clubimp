<div class="row">
    <div class="col-xs-12">
        @if(isset($oPosts))
            @include('site.posts.components.table', [
                'oPosts' => $oPosts
            ])
        @endif
    </div>
    <div class="col-xs-12">
        @if(isset($oPost))
            @include('site.posts.one', [
                'oPost' => $oPost
            ])
        @endif
    </div>
</div>



