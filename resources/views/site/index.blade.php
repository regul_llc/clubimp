<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{ config('app.name') }}</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicons -->
    <link rel="shortcut icon" href="/web/assets/images/favicon.png">
    <link rel="apple-touch-icon" href="/web/assets/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/web/assets/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/web/assets/images/apple-touch-icon-114x114.png">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>

    <!-- Bootstrap core CSS -->
    <link href="{{ mix('css/app/app.css') }}" rel="stylesheet">

    <!-- Color CSS -->
    <!--<link href="assets/css/colors/blue.css" rel="stylesheet" type="text/css">-->

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>

<!-- PRELOADER -->
<div class="page-loader" >
    <div class="loader">Loading...</div>
</div>
<!-- /PRELOADER -->

<div class="wrapper">


    <!--<section id="fss" class="module-hero overlay-dark">

        <div class="hero-caption">
            <div class="hero-text">


                <h1 class="m-b-40">Клуб импровизации "Для своих"</h1>
                <p class="lead m-b-60">До начала следующего концерта осталось:</p>

                <div id="countdown" class="m-b-100" data-countdown="2017/01/01"></div>


            </div>
        </div>


    </section>-->

    <!-- HERO -->
    <section id="hero" class="module-hero module-video overlay-dark" >

        <!-- HERO TEXT -->
        <div class="hero-caption">
            <div class="hero-text">



            </div>
        </div>
        <!-- /HERO TEXT -->

        <div class="video-player" data-property="{ videoURL: 'https://youtu.be/Q5OpJGrdDI8', containment: '#hero', quality: 'large', startAt: 0, autoPlay: true,loop: true, opacity: 1, showControls: false, showYTLogo: false, vol: 0, mute: true }"></div>


    </section>
    <!-- /HERO -->

    <!-- SERVICES -->
    <section id="services" class="module booking parter-p {{ $shareClassPlan or '' }}"
        @if(isset($oEvent) && !is_null($oEvent))
            @if(isset($oEvents))
                style="height: inherit;padding-bottom: 0;"
            @else
                style=""
            @endif
        @else
            style="height: inherit;"
        @endif
    >

        <!-- MODULE DIVIDER -->
        <a id="hero-divider" class="striped-icon divider inner-scroll" href="#services">
            <i class="fa fa-angle-down"></i>
        </a>
        <!-- /MODULE DIVIDER -->

        <div class="container __book {{ $shareClassWide or '' }}">


            <div class="row">
                <div class="col-sm-12">
                    <h3 class="module-subtitle">Забронируй / Купи / Радуйся</h3>
                    @if(isset($oEvent) && !is_null($oEvent))
                        <h4 class="">Билеты на концерт - {{ $oPlan->address_title }} {{ $oEvent->beginning_at_event }}</h4>
                    @else
                        <h4 class="">На текущий момент продажа билетов не производится. Следите за следующими мероприятиями в наших группах в социальных сетях</h4>
                    @endif
                </div>
            </div>

            @if(isset($oEvent) && !is_null($oEvent))
                <div class="row frame">
                    <div class="legend">
                        <div class="top-places">
                            <div class="places-wrp">
                                <div class="places">
                                    <div class="row-item">
                                        <div class="item-wrp">
                                            <div class="item disabled __m">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-item">
                                        <div class="item-wrp">
                                            <div class="item-row number">
                                                <span> - свободно</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="places">
                                    <div class="row-item">
                                        <div class="item-wrp">
                                            <div class="item disabled chosen ">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-item">
                                        <div class="item-wrp">
                                            <div class="item-row ">
                                                <span> - выбрано</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="places">
                                    <div class="row-item">
                                        <div class="item-wrp">
                                            <div class="item disabled ">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-item">
                                        <div class="item-wrp">
                                            <div class="item-row number">
                                                <span> - занято</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="preloader ">
                        <div></div>
                    </div>

                    <div class="plan-container">
                        @include('site.'.$oPlan->tpl, [
                            'oEvent' => $oEvent,
                            'oPlan' => $oPlan,
                            'oPlaces' => $oPlaces,
                        ])
                    </div>
                    <div class="text-center hidden">
                        <button type="button" class="ajax-link" action="{{ route('index.plan.post', ['event_id' => $oEvent->id]) }}"
                                data-pagination="1"
                                data-pagination-container=".plan-container"
                        >
                            Загрузить
                        </button>
                    </div>
                </div>
            @endif
            @if(isset($oEvents) && count($oEvents) !== 0)
                <div class="row frame" style="border: none;height: inherit;margin-bottom: 50px;">
                    <div class="text-center" style="padding: 50px; font-size: 20px;">
                        Другие мероприятия
                    </div>
                    @foreach($oEvents as $oNextEvent)
                        <div class="text-center col-sm-{{ 12 / count($oEvents) }}">
                            <div style="margin-bottom: 10px;min-height: 69px;">
                                <b>{{ $oNextEvent->beginning_at_event }}</b><br>
                                {{ $oNextEvent->plan->address_title }}
                            </div>
                            <a href="{{ route('index.event', ['id' => $oNextEvent->id, 'date' => $oNextEvent->beginning_at->format('d-m-Y')]) }}"
                               class="btn btn-sm btn-base"
                               target="_blank"
                            >
                                <span>Купить билеты</span>
                            </a>
                        </div>
                    @endforeach
                </div>
            @else
                <div style="height: 30px;"></div>
            @endif
        </div>

        @ifDebug
            <div class="text-center" style="color: #ff1e6a; padding: 50px; font-size: 20px;">
                {{ trans('site.debug.sale') }}
            </div>
        @endifDebug

    </section>
    <!-- /SERVICES -->

    @if(isset($oEvent) && !is_null($oEvent))
        @include('site.components.contact.block', [
            'oEvent' => $oEvent,
            'oPlan' => $oPlan,
            'oPlaces' => $oPlaces,
            'aPlacesId' => $aPlacesId,
            'aClientCachePlaces' => $aClientCachePlaces
        ])
    @endif



    @if(isset($oEvent) && !is_null($oEvent))
    <!-- GOOGLE MAP -->
    <section class="module-map m-b-40"
             id="event-map"
             data-event-title="{{ config('app.name') }}"
             data-plan-address="{{ $oPlan->address or '' }}"
             data-hint-content="{{ $oPlan->address_title or '' }}"
             data-balloon-content="{{ $oPlan->address_title or '' }}"
             data-coordinates-lon="{{ $oPlan->coordinates['lon'] or 0 }}"
             data-coordinates-lat="{{ $oPlan->coordinates['lat'] or 0 }}"
    >
        <!-- ADD YOUR ADDRESS HERE -->
        <div id="map-yandex"></div>
    </section>
    <!-- /GOOGLE MAP -->
    @endif



    <!-- FOOTER -->

    <footer class="footer p-t-0">
        <div class="container">

            <div class="row">

                <!--<div class="col-sm-2 col-sm-offset-5">
                    <div class="text-center m-b-40">
                        <img src="assets/images/logo.png" alt="">
                    </div>
                </div>-->

            </div>

            <div class="row">

                <div class="col-sm-12">

                    <div class="social-icons social-icons-animated m-b-40">
                        <a href="https://www.instagram.com/clubimprov/" target="_blank" class="fa fa-instagram instagram wow fadeInUp"></a>
                        <a href="https://vk.com/clubimprov" target="_blank" class="fa fa-vk behance wow fadeInUp"></a>
                    </div>

                </div>

            </div>

            <hr class="divider">

            <div class="row">

                <div class="col-sm-12">

                    <div class="copyright text-center m-t-40">
                        &copy 2017 {{ config('app.name') }}
                    </div>
                    <div class=" logo-bottom">
                        <p>Разработано:</p>
                        <a href="http://webregul.ru/" target="_blank"><img src="images/regul-logo.png" alt="" width="101" height="29"></a>
                    </div>

                </div>

            </div>

        </div>
    </footer>

    <!-- /FOOTER -->

</div>
<!-- /WRAPPER -->

<div class="modal fade" id="check-reservation-error">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Места заняты</h4>
            </div>
            <div class="modal-body text-center" style="font-size: 14px;">
                К сожалению кто-то забронировал эти места раньше вас. Обновите страницу и попробуйте снова
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-base" data-dismiss="modal" style="padding-bottom: 5px;padding-top: 7px;">Закрыть</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="check-reservation-success">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Успех</h4>
            </div>
            <div class="modal-body text-center" style="font-size: 14px;height: 75px;">
                <div class="message-cont">
                    <div class="col-sm-12">
                        <div class="form-group __check " style="margin-bottom: 0;">
                            <i class="fa fa-check-square-o" aria-hidden="true"></i>
                            <p>Ваша заявка принята, идет проверка платежа.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-base" data-dismiss="modal" style="padding-bottom: 5px;padding-top: 7px;">Закрыть</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- JAVASCRIPT FILES -->
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

<script src="/web/assets/js/jquery-2.1.4.min.js"></script>
<script src="/js/app/map.js"></script>

<script src="/web/assets/bootstrap/js/bootstrap.min.js"></script>

<script src="/web/assets/js/jquery.mb.YTPlayer.min.js"></script>
<script src="/web/assets/js/jquery.backstretch.min.js"></script>

<script src="/web/assets/js/owl.carousel.min.js"></script>
<script src="/web/assets/js/jquery.fitvids.js"></script>

<script src="/web/assets/js/wow.min.js"></script>

<!--<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="assets/js/gmap3.min.js"></script>-->

<script src="/web/assets/js/typed.min.js"></script>
<script src="/web/assets/js/jquery.countdown.min.js"></script>

<script src="/web/assets/js/jqBootstrapValidation.js"></script>
<script src="/web/assets/js/smoothscroll.js"></script>
<script src="/web/assets/js/contact.js"></script>
<script src="/web/assets/js/custom.js"></script>
<script src="/web/js/plugins/jquery.mask.min.js"></script>
<script src="/web/js/project/mask.js"></script>

<script src="/web/assets/js/fss.js"></script>
<script src="/web/assets/js/fss-settings.js"></script>
{{--<script src="/web/js/main.js"></script>--}}

<script src="{{ mix('js/app/app.js') }}?v=1342"></script>
@if(Session::exists('success_payment'))
    <script>
        $('#check-reservation-success').modal('show');
    </script>
@endif

</body>
</html>