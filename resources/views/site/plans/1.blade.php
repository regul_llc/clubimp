<div class="top-places clearfix">
    <div class="wrapper-1-scrolled">
        <div class="wrapper-2-scrolled clearfix">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <div class="places-wrp">
                    <div class="places">
                        <div class="row-item">
                            <div class="item-wrp">
                                <div class="item-row number">
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        @for($i=0;$i<8;$i++)
                            <div class="row-item">
                                <div class="item-wrp">
                                    <div class="item-row number">
                                        <span>{{ $i + 1}}</span>
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>
                    @foreach($oPlaces['t1'] as $key => $oRow)
                        <div class="places">
                            <div class="row-item">
                                <div class="item-wrp">
                                    <div class="item-row number">
                                        <span>{{ $key }}</span>
                                    </div>
                                </div>
                            </div>
                            @foreach($oRow as $oPlace)
                                <div class="row-item">
                                    <div class="item-wrp">
                                        <div @if(in_array($oPlace->id, $aClientCachePlaces))
                                             class="item active"
                                             @elseif(in_array($oPlace->id, $aPlacesId))
                                             class="item disabled"
                                             @else
                                             class="item "
                                             @endif
                                             data-section="{{ $oPlace->section->name }}"
                                             data-row='{{ $oPlace->row }}'
                                             data-place='{{ $oPlace->num }}'
                                        >

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <div class="places-wrp">
                    <div class="places">
                        <div class="row-item">
                            <div class="item-wrp">
                                <div class="item-row number">
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        @for($i=0;$i<8;$i++)
                            <div class="row-item">
                                <div class="item-wrp">
                                    <div class="item-row number">
                                        <span>{{ $i + 1}}</span>
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>
                    @foreach($oPlaces['t2'] as $key => $oRow)
                        <div class="places">
                            <div class="row-item">
                                <div class="item-wrp">
                                    <div class="item-row number">
                                        <span>{{ $key }}</span>
                                    </div>
                                </div>
                            </div>
                            @foreach($oRow as $oPlace)
                                <div class="row-item">
                                    <div class="item-wrp">
                                        <div @if(in_array($oPlace->id, $aClientCachePlaces))
                                             class="item active"
                                             @elseif(in_array($oPlace->id, $aPlacesId))
                                             class="item disabled"
                                             @else
                                             class="item "
                                             @endif
                                             data-section="{{ $oPlace->section->name }}"
                                             data-row='{{ $oPlace->row }}'
                                             data-place='{{ $oPlace->num }}'
                                        >

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>


            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <div class="places-wrp">
                    <div class="places">
                        <div class="row-item">
                            <div class="item-wrp">
                                <div class="item-row number">
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        @for($i=0;$i<8;$i++)
                            <div class="row-item">
                                <div class="item-wrp">
                                    <div class="item-row number">
                                        <span>{{ $i + 1}}</span>
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>
                    @foreach($oPlaces['t3'] as $key => $oRow)
                        <div class="places">
                            <div class="row-item">
                                <div class="item-wrp">
                                    <div class="item-row number">
                                        <span>{{ $key }}</span>
                                    </div>
                                </div>
                            </div>
                            @foreach($oRow as $oPlace)
                                <div class="row-item">
                                    <div class="item-wrp">
                                        <div @if(in_array($oPlace->id, $aClientCachePlaces))
                                             class="item active"
                                             @elseif(in_array($oPlace->id, $aPlacesId))
                                             class="item disabled"
                                             @else
                                             class="item "
                                             @endif
                                             data-section="{{ $oPlace->section->name }}"
                                             data-row='{{ $oPlace->row }}'
                                             data-place='{{ $oPlace->num }}'
                                        >

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>



            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="places-wrp __big">
                    @foreach($oPlaces['p'][0] as $key => $oRow)
                        <div class="places">
                            @foreach($oRow as $oPlace)
                                <div class="row-item">
                                    <div class="item-wrp">
                                        <div @if(in_array($oPlace->id, $aClientCachePlaces))
                                             class="item active"
                                             @elseif(in_array($oPlace->id, $aPlacesId))
                                             class="item disabled"
                                             @else
                                             class="item "
                                             @endif
                                             data-section="{{ $oPlace->section->name }}"
                                             data-row='{{ $oPlace->row }}'
                                             data-place='{{ $oPlace->num }}'
                                        >

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="row-item">
                                <div class="item-wrp">
                                    <div class="item-row number">
                                        <span>{{ $key }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <div class="places">
                        @for($j=0;$j<10;$j++)
                            <div class="row-item">
                                <div class="item-wrp">
                                    <div class="item-row number">
                                        <span>{{$j + 1}}</span>
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>

                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="places-wrp __big">
                    @foreach($oPlaces['p'][1] as $key => $oRow)
                        <div class="places">
                            @foreach($oRow as $oPlace)
                                <div class="row-item">
                                    <div class="item-wrp">
                                        <div @if(in_array($oPlace->id, $aClientCachePlaces))
                                             class="item active"
                                             @elseif(in_array($oPlace->id, $aPlacesId))
                                             class="item disabled"
                                             @else
                                             class="item "
                                             @endif
                                             data-section="{{ $oPlace->section->name }}"
                                             data-row='{{ $oPlace->row }}'
                                             data-place='{{ $oPlace->num }}'
                                        >

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="row-item">
                                <div class="item-wrp">
                                    <div class="item-row number">
                                        <span>{{ $key }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="places">
                        @for($j=0;$j<10;$j++)
                            <div class="row-item">
                                <div class="item-wrp">
                                    <div class="item-row number">
                                        <span>{{$j + 11}}</span>
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>


            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="btn btn-base-2">Сцена</div>
            </div>


        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="counter-wrp">
            <div class="num-places">
                <span>Мест: </span>
                <span class="__count" data-count="{{ count($aClientCachePlaces) }}">{{ count($aClientCachePlaces) }}</span>
            </div>
            <div class="sum" data-price="{{ $oEvent->price }}">
                <span>Сумма:</span>
                <span class="__count">{{ count($aClientCachePlaces) * $oEvent->price }}</span>
            </div>
        </div>
        <input type="hidden" id="places-data" name="places" value="{{ $sHiddenPlaces or '' }}">
        <div class="book-bt">
            <a href="#"
               @if(!empty($aClientCachePlaces))
               class="btn btn-base ajax-link"
               @else
               class="btn btn-base ajax-link disabled"
               @endif

               action="{{ route('index.check.post', ['event_id' => $oEvent->id]) }}"

               data-merge-data="#places-data"
               data-merge-data-name="places"
               data-show="#contact"
               data-hide=".module.booking"
               data-support-view=".order-places-description"
               data-preloader=".preloader"
               data-list=".plan-container"
               data-list-preloader=".preloader"
               data-list-action="{{ route('index.plan.post', ['event_id' => $oEvent->id]) }}"
               data-callback="showHideAfterSuccess, refreshAfterSubmit"
            >
                <span>Забронировать</span>
            </a>
        </div>
    </div>
</div>