<div class="top-places clearfix">
    <div class="wrapper-1-scrolled">
        <div class="wrapper-2-scrolled clearfix">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="places-wrp">
                    <div class="places">
                        <div class="row-item">
                            <div class="item-wrp">
                                <div class="item-row number">
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="row-item">
                            <div class="item-wrp">
                                <div class="item-row number">
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="row-item">
                            <div class="item-wrp">
                                <div class="item-row number">
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="row-item __offset">
                            <div class="item-wrp">
                                <div class="item-row number">
                                    <span>диваны</span>
                                </div>
                            </div>
                        </div>
                        <div class="row-item">
                            <div class="item-wrp">
                                <div class="item-row number">
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="row-item __offset">
                            <div class="item-wrp">
                                <div class="item-row number">
                                    <span>диваны</span>
                                </div>
                            </div>
                        </div>
                        <div class="row-item">
                            <div class="item-wrp">
                                <div class="item-row number">
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="row-item __offset">
                            <div class="item-wrp">
                                <div class="item-row number">
                                    <span>диваны</span>
                                </div>
                            </div>
                        </div>
                        <div class="row-item">
                            <div class="item-wrp">
                                <div class="item-row number">
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="row-item">
                            <div class="item-wrp">
                                <div class="item-row number">
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="row-item">
                            <div class="item-wrp">
                                <div class="item-row number">
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="row-item">
                            <div class="item-wrp">
                                <div class="item-row number">
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="row-item">
                            <div class="item-wrp">
                                <div class="item-row number">
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="row-item">
                            <div class="item-wrp">
                                <div class="item-row number">
                                    <span></span>
                                </div>
                            </div>
                        </div>

                    </div>

                    @foreach($oPlaces['p'] as $key => $oRow)
                        <div class="places">
                            <div class="row-item">
                                <div class="item-wrp">
                                    <div class="item-row number">
                                        <span>{{ $key }}</span>
                                    </div>
                                </div>
                            </div>
                            @foreach($oRow as $subKey => $oPlace)
                                @if(is_null($oPlace))
                                    @if($key === 5 && $subKey === 12)
                                        <div class="row-item __offset">
                                            <div class="item-wrp">
                                                <div class="item-row number">
                                                <span>диваны</span>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="row-item">
                                            <div class="item-wrp">
                                                <div class="item-row number">
                                                <span></span>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @else
                                    <div class="row-item">
                                        <div class="item-wrp">
                                            <div @if(in_array($oPlace->id, $aClientCachePlaces))
                                                 class="item active"
                                                 @elseif(in_array($oPlace->id, $aPlacesId))
                                                 class="item disabled"
                                                 @else
                                                 class="item "
                                                 @endif
                                                 data-section="{{ $oPlace->section->name }}"
                                                 data-row='{{ $oPlace->row }}'
                                                 data-place='{{ $oPlace->num }}'
                                            >

                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    @endforeach
                    <div class="places">
                        <div class="row-item">
                            <div class="item-wrp">
                                <div class="item-row number">
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        @for($i=0;$i<13;$i++)
                            <div class="row-item">
                                <div class="item-wrp">
                                    <div class="item-row number">
                                        <span>{{ $i + 1}}</span>
                                    </div>
                                </div>
                            </div>
                        @endfor

                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="btn btn-base-2">Сцена</div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="text-bottom">
                    <span>
                        <b>*</b> при бронировании дивана желательно выкупать сразу 2 места
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="counter-wrp">
            <div class="num-places">
                <span>Мест: </span>
                <span class="__count" data-count="{{ count($aClientCachePlaces) }}">{{ count($aClientCachePlaces) }}</span>
            </div>
            <div class="sum" data-price="{{ $oEvent->price }}">
                <span>Сумма:</span>
                <span class="__count">{{ count($aClientCachePlaces) * $oEvent->price }}</span>
            </div>
        </div>
        <input type="hidden" id="places-data" name="places" value="{{ $sHiddenPlaces or '' }}">
        <div class="book-bt">
            <a href="#"
               @if(!empty($aClientCachePlaces))
               class="btn btn-base ajax-link"
               @else
               class="btn btn-base ajax-link disabled"
               @endif

               action="{{ route('index.check.post', ['event_id' => $oEvent->id]) }}"

               data-merge-data="#places-data"
               data-merge-data-name="places"
               data-show="#contact"
               data-hide=".module.booking"
               data-support-view=".order-places-description"
               data-preloader=".preloader"
               data-list=".plan-container"
               data-list-preloader=".preloader"
               data-list-action="{{ route('index.plan.post', ['event_id' => $oEvent->id]) }}"
               data-callback="showHideAfterSuccess, refreshAfterSubmit"
            >
                <span>Забронировать</span>
            </a>
        </div>
    </div>
</div>