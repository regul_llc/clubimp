@extends('emails.layout')

@section('content')
    <p>Было совершено бронирование билетов на мероприятие {{ $oEvent->beginning_at->format('d.m.Y H:i') }} </p>
    <p>Заказчик: {{ $oClient->name }}, {{ $oClient->phone }}, {{ $oClient->email }} </p>
    <p>Места: <br>
    @foreach($oPlaces as $oPlace)
        {{ $oPlace->section->title }}, ряд {{ $oPlace->row }}, место {{ $oPlace->num }}; <br>
    @endforeach
    </p>
    <p>Код брони: {{ $mark }}</p>
@endsection