@extends('emails.layout')

@section('content')
    <p>Вы успешно забронировали билеты на концерт. </p>
    <p>Место проведения: {{ $oEvent->plan->address_title }}, {{ $oEvent->plan->address }} </p>
    <p>Дата проведения: {{ $oEvent->beginning_at->format('d.m.Y H:i') }} </p>
    <p>Ваши места: <br>
    @foreach($oPlaces as $oPlace)
        {{ $oPlace->section->title }}, ряд {{ $oPlace->row }}, место {{ $oPlace->num }}; <br>
    @endforeach
    </p>
    <p>Ваш код брони: {{ $mark }}</p>
@endsection
