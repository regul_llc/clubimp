<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('defaults.title')

    <!-- Premium Icons -->
    <link href="{{ asset('css/admin/glyphicons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin/glyphicons-filetypes.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin/glyphicons-social.css') }}" rel="stylesheet">

    <!-- include summernote css -->
    {{--<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">--}}

    <!-- Main styles for this application -->
    <link href="{{ asset('css/admin/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};
    </script>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">

@include('layouts.admin.components.header')

<div class="app-body">

    @include('layouts.admin.components.sidebar')

    <!-- Main content -->
    <main class="main">

        <!-- Page Header -->
        <div class="page-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-7">
                        @yield('head')
                    </div>
                    {{--
                    <div class="col-md-5 charts">
                        <div class="row">
                            <div class="col-12 col-sm-4">
                                <div class="chart-wrapper" style="height:33px">
                                    <canvas id="header-chart-1" class="chart" height="33"></canvas>
                                </div>
                                <div class="text-center title">
                                    <span class="text-muted">Income:</span>
                                    <strong>$189.128</strong>
                                </div>
                            </div>
                            <div class="col-6 col-sm-4">
                                <div class="chart-wrapper" style="height:33px">
                                    <canvas id="header-chart-2" class="chart" height="33"></canvas>
                                </div>
                                <div class="text-center title">
                                    <span class="text-muted">New clients:</span>
                                    <strong class="text-danger">1.128</strong>
                                </div>
                            </div>
                            <div class="col-6 col-sm-4">
                                <div class="chart-wrapper" style="height:33px">
                                    <canvas id="header-chart-3" class="chart" height="33"></canvas>
                                </div>
                                <div class="text-center title">
                                    <span class="text-muted">Orders:</span>
                                    <strong class="text-success">2.345</strong>
                                </div>
                            </div>
                        </div>
                    </div>
                    --}}
                </div>
            </div>
        </div>
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            {{--<li class="breadcrumb-item" data-rel="tooltip" title="Home">Home</li>--}}

            @include('defaults.admin.breadcrumb')

        </ol>

        <div class="container-fluid">

            {{--
            <div id="current-title" hidden="">Dashboard</div>
            <div id="current-desc" hidden="">Welcome to Root Bootstrap 4 Admin Template</div>
            --}}

            <div class="animated fadeIn">

                @yield('content')

            </div>

            @yield('modals')

        </div>
        <!-- /.conainer-fluid -->
    </main>

    @include('layouts.admin.components.aside')

</div>

@include('layouts.admin.components.footer')


<script src="{{ asset('js/admin/manifest.js') }}"></script>
<script src="{{ asset('js/admin/vendor.js') }}"></script>
<script src="{{ asset('js/admin/app.js') }}"></script>



</body>

</html>