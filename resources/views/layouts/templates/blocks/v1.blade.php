<center>
    {{ $aOptions['title'] }}
</center>
<div class="row">
    @foreach($aOptions['one'] as $block)
        <div class="col-lg-4" style="background-color: #ccc;">
            {{ $block['title'] }}<br>
            {{ $block['description'] }}<br>
            {{ $block['icon'] }}
        </div>
    @endforeach
</div>