<center>
    {{ $aOptions['title'] or '' }}
</center>
<div>
    <b>{{ $aOptions['description'] or '' }}</b>
</div>
<div>
    <b>{{ $aOptions['address'] or '' }}</b>
</div>
<div>
    @if(isset($aOptions['phone']))
        @php
            $phones = explode('-', $aOptions['phone']);
        @endphp
        @if(count($phones) > 1)
            @foreach($phones as $phone)
                {{ $phone }}<br>
            @endforeach
        @else
            {{ $aOptions['phone'] }}
        @endif
    @endif
</div>