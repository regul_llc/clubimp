@extends('layouts.'.config('project.layout.'.$sComposerLocator))

@section('menu')
    @if(View::exists('defaults.'.$sComposerLocator.'.menu'))
        @include('defaults.'.$sComposerLocator.'.menu')
    @endif
@endsection

@section('head')
    @if(View::exists($sComposerLocator.'.'.$page.'.components.head'))
        @include($sComposerLocator.'.'.$page.'.components.head')
    @elseif(View::exists($sComposerLocator.'.components.head'))
        @include($sComposerLocator.'.components.head')
    @elseif(View::exists('defaults.'.$sComposerLocator.'.head'))
        @include('defaults.'.$sComposerLocator.'.head')
    @else
        @include('defaults.head')
    @endif
@endsection

@section('content')



    @if(isset($action) && !empty($action))
        @if($action == 'edit')
            @if(View::exists($sComposerLocator.'.'.$page.'.edit'))
                @include($sComposerLocator.'.'.$page.'.edit')
            @else
                @include ('standard.edit')
            @endif
        @elseif($action == 'list')

            @if(View::exists($sComposerLocator.'.content.'.$page.'.list'))
                @include($sComposerLocator.'.content.'.$page.'.list')
            @else
                @include ('standard.list')
            @endif
        @elseif($action == 'index')
            @if(View::exists($sComposerLocator.'.'.$page.'.index'))
                @include($sComposerLocator.'.'.$page.'.index')
            @else
                @include ('defaults.'.$sComposerLocator.'.index')
            @endif
        @else
            @if(View::exists($sComposerLocator.'.'.$page.'.'.$action))
                @include($sComposerLocator.'.'.$page.'.'.$action)
            @else
                @include ('defaults.'.$sComposerLocator.'.index')
            @endif
        @endif
    @endif

    @if($page === 'error' && isset($status))
        @if(View::exists('errors.'.$status))
            @include('errors.'.$status)
        @endif
    @endif

@endsection

@section('modals')
    @if(View::exists($sComposerLocator.'.'.$page.'.components.modals.all'))
        @include($sComposerLocator.'.'.$page.'.components.modals.all')
    @endif
    @if(isset($action) && !empty($action) && View::exists($sComposerLocator.'.'.$page.'.'.$action.'.components.modals.all'))
        @include($sComposerLocator.'.'.$page.'.'.$action.'.components.modals.all')
    @endif
    @if(View::exists($sComposerLocator.'.components.modals.all'))
        @include($sComposerLocator.'.components.modals.all')
    @endif
@endsection