@foreach(config('project.menu.site') as $key => $menu)
    <li>
        <a href="{{ url('/'.$key) }}">{{ $menu['title'] }}</a>
    </li>
@endforeach