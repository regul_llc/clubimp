<form action="{{ $action }}" id="ext_auth_form" method="POST" {{-- target="_blank" --}} style="display: none;">
    @foreach($params as $key => $param)
        <input type="text" name="{{ $key }}" value="{{ $param }}">
    @endforeach
</form>