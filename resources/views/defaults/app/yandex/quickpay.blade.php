<form method="POST" id="ext_auth_form" action="https://money.yandex.ru/quickpay/confirm.xml" style="display: none;">
    <input type="hidden" name="receiver" value="{{ Config::get('yandex.money_wallet') }}">
    <input type="hidden" name="formcomment" value="{{ config('app.name') }}">
    <input type="hidden" name="short-dest" value="{{ $oPayment->purpose }}">
    <input type="hidden" name="label" value="{{ $oPayment->id }}">
    <input type="hidden" name="quickpay-form" value="donate">
    <input type="hidden" name="targets" value="Оплата бронирования №{{ $oPayment->id }}">
    {{--<input type="hidden" name="targets" value="Оплата бронирования №{{ $oPayment->id }} на {{ $oEvent->beginning_at->format('d.m.Y H:i') }}">--}}
    <input type="hidden" name="sum" value="{{ $oPayment->price }}" data-type="number">
    <input type="hidden" name="comment" value="{{ $oPayment->purpose }}">
    <input type="hidden" name="need-fio" value="false">
    <input type="hidden" name="need-email" value="false">
    <input type="hidden" name="need-phone" value="false">
    <input type="hidden" name="need-address" value="false">
    @if(isset($sPaymentType) && $sPaymentType === 'PC')
        <input type="hidden" name="paymentType" value="PC">
    @else
        <input type="hidden" name="paymentType" value="AC">
    @endif
    {{--<label><input type="radio" name="paymentType" value="PC">Яндекс.Деньгами</label>--}}
    {{--<label><input type="radio" name="paymentType" value="AC" checked>Банковской картой</label>--}}
    <input type="submit" value="Оплатить">
</form>