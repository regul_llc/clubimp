@extends('layouts.app')

@section('title')
    <title>Страница не найдена</title>
@endsection

@section('content')
    <div class="text-center">
        <h1>Страница не найдена</h1>
    </div>
@endsection
