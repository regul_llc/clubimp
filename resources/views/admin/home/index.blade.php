<div class="row">
    <div class="col-md-4">
        <div class="card card-inverse card-primary text-center">
            <div class="card-block">
                <div class="card-actions" style="position: absolute;top: 5px;right: 8px;">
                    <a href="#" class="btn-close" style="color: #fff;"><i class="icon-close"></i></a>
                </div>
                <blockquote class="card-blockquote">
                    <p>Welcome to ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                    <footer>Someone famous in
                        <cite title="Source Title">Source Title</cite>
                    </footer>
                </blockquote>
            </div>
        </div>
    </div>
</div>