<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i>
                Users
            </div>
            <div class="card-block">
                @if(isset($oUsers))
                    @include('admin.users.components.table', [
                        'oUsers' => $oUsers
                    ])
                @endif
            </div>
        </div>
    </div>
</div>
