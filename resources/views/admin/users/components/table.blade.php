<table class="table table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>{{ trans('admin.first_name') }}</th>
        <th>{{ trans('admin.last_name') }}</th>
        <th>{{ trans('admin.email') }}</th>
        <th>{{ trans('admin.role') }}</th>
        <th>{{ trans('admin.status') }}</th>
        <th>{{ trans('admin.activation') }}</th>
        <th>{{ trans('admin.last_login') }}</th>
        <th>{{ trans('admin.updated_at') }}</th>
        <th>{{ trans('admin.created_at') }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($oUsers as $oUser)
        <tr>
            <td>{{ $oUser->id }}</td>
            <td>{{ $oUser->first_name }}</td>
            <td>{{ $oUser->last_name }}</td>
            <td>{{ $oUser->email }}</td>
            <td>{{ $oUser->role->name }}</td>
            <td>
                <span class="{{ $oUser->status_icon['class'] }}" title="{{ $oUser->status_text }}">{{ $oUser->status_icon['title'] }}</span>
            </td>
            <td>
                @if(isset($oUser->activation) && !empty($oUser->activation) && $oUser->activation->completed)
                    Activate
                @else
                    No Activate
                @endif
            </td>
            <td>
                @if(!is_null($oUser->last_login))
                    {{ $oUser->last_login->format('d.m.Y H:i:s')}}
                @else
                    No login
                @endif
            </td>
            <td>
                {{ $oUser->updated_at->format('d.m.Y H:i:s') }}
            </td>
            <td>
                {{ $oUser->created_at->format('d.m.Y H:i:s') }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@if($oUsers instanceof \Illuminate\Pagination\LengthAwarePaginator)
    @if(isset($aSearch))
        {{ $oUsers->appends($aSearch)->links('defaults.admin.pagination') }}
    @else
        {{ $oUsers->links('defaults.admin.pagination') }}
    @endif
@endif