<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i>
                Типы постов
            </div>
            <div class="card-block">
                @if(isset($oPostTypes))
                    @include('admin.posts.types.components.table', [
                        'oPostTypes' => $oPostTypes
                    ])
                @endif
            </div>
        </div>
    </div>
</div>
