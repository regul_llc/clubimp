<div class="row">
    <div class="col-lg-6">
        <form class="card" action="/admin/posts/action/edit-post-type" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="post_type[id]" value="{{ $oPostType->id }}">
            <div class="card-header">
                <i class="fa fa-align-justify"></i>
                Изменить тип
            </div>
            <div class="card-block">
                <div class="form-group">
                    <label>{{ trans('admin.title') }}</label>
                    <input type="text" class="form-control" name="post_type[title]" placeholder="{{ trans('admin.title') }}" value="{{ $oPostType->title }}">
                </div>
                <div class="form-group">
                    <label>{{ trans('admin.path_template') }}</label>
                    <select class="form-control" name="post_type[slug]">
                        @if(isset($aComposerTemplates))
                            @foreach($aComposerTemplates as $pathKey => $aComposerTemplate)
                                <option value="{{ $pathKey }}" @if($pathKey === $oPostType->slug) selected @endif>{{ $pathKey }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary" data-style="slide-down">{{ trans('admin.submit.edit') }}</button>
            </div>
        </form>
    </div>
</div>