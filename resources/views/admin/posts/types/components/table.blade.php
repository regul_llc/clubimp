<table class="table table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>{{ trans('admin.title') }}</th>
        <th>Количество постов с этим типом</th>
    </tr>
    </thead>
    <tbody>
    @foreach($oPostTypes as $oPostType)
        <tr>
            <td>{{ $oPostType->id }}</td>
            <td>
                <a href="/admin/posts/types/edit/{{ $oPostType->id }}" title="Изменить" data-rel="tooltip">
                    {{ $oPostType->title }}
                </a>
            </td>
            <td>
                {{ count($oPostType->posts) }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@if($oPostTypes instanceof \Illuminate\Pagination\LengthAwarePaginator)
    @if(isset($aSearch))
        {{ $oPostTypes->appends($aSearch)->links('defaults.admin.pagination') }}
    @else
        {{ $oPostTypes->links('defaults.admin.pagination') }}
    @endif
@endif