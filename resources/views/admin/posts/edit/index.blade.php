<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i>
                {{ $oPost->title }}
                <div class="card-actions">
                    <a href="#" class="btn-close" onclick="event.preventDefault();document.getElementById('post-form').submit();"
                       data-rel="tooltip" title="Удалить"
                    >
                        <i class="icon-close"></i>
                    </a>
                </div>
                <form id="post-form" action="{{ url('/admin/posts/action/delete-post') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $oPost->id }}">
                </form>
            </div>
            <div class="card-block ajax-table-block">
                @if(isset($oPost))
                    @include('admin.posts.edit.components.table', [
                        'oPost' => $oPost
                    ])
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="row ajax-block-template">
            @include('admin.posts.edit.template', [
                'oPost' => $oPost
            ])
        </div>
    </div>
    <div class="col-lg-6 ajax-block-rules">
        @include('admin.posts.edit.rules', [
            'oPost' => $oPost
        ])
    </div>
</div>