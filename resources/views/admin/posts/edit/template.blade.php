<div class="col-lg-12">
    <form class="card ajax-form" action="{{ url('/admin/posts/action/edit-post-template') }}" method="POST"
          data-list=".ajax-block-template"
          data-list-action="{{ url('/admin/posts/action/get-post-template') }}"
          data-callback="refreshAfterSubmit"
          data-form-data="#post-form"
    >
        <input type="hidden" name="post[id]" value="{{ $oPost->id }}">
        <div class="card-header">
            <i class="fa fa-align-justify"></i>
            {{ trans('admin.template') }}
            @if(is_null($oPost->template))
                <span class="badge badge-danger float-right">Не выбрано</span>
            @endif
        </div>
        <div class="card-block">
            <div class="form-group">
                <label>{{ trans('admin.template') }}</label>
                <select class="form-control" name="post[template]" required>
                    @if(is_null($oPost->template))
                        <option value="">Выберите шаблон</option>
                    @endif
                    @if(isset($aComposerTemplates[$oPost->type->slug]))
                        @foreach($aComposerTemplates[$oPost->type->slug] as $pathKey => $aComposerTemplate)
                            <option value="{{ $oPost->type->slug }}.{{$aComposerTemplate}}" @if($oPost->type->slug.'.'.$aComposerTemplate === $oPost->template) selected @endif>{{ $oPost->type->slug }}.{{$aComposerTemplate}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary inner-form-submit" data-style="slide-down">{{ trans('admin.submit.save') }}</button>
        </div>
    </form>
</div>