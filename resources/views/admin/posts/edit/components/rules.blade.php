@if($rule === 'string')
    <div class="form-group">
        <label>{{ $type }}</label>
        @if(isset($prefix) && isset($item))
            <input type="text" class="form-control" name="post[options][{{$prefix}}][{{ $type }}][]" placeholder="{{ $type }}" @if(isset($oPostModel)) value="{{ $oPostModel->options[$prefix][$item][$type] or '' }}" @endif required>
        @elseif(isset($prefix))
            <input type="text" class="form-control" name="post[options][{{$prefix}}][{{ $type }}][]" placeholder="{{ $type }}" @if(isset($oPostModel)) value="{{ $oPostModel->options[$prefix][$type] or '' }}" @endif required>
        @else
            <input type="text" class="form-control" name="post[options][{{ $type }}]" placeholder="{{ $type }}" @if(isset($oPostModel)) value="{{ $oPostModel->options[$type] or '' }}" @endif required>
        @endif
    </div>
@endif
@if($rule === 'text')
    <div class="form-group">
        <label>{{ $type }}</label>
        @if(isset($prefix) && isset($item))
            <textarea  class="form-control" name="post[options][{{$prefix}}][{{ $type }}][]" cols="30" rows="10" placeholder="{{ $type }}">@if(isset($oPostModel)){{ $oPostModel->options[$prefix][$item][$type] or '' }}@endif</textarea>
        @elseif(isset($prefix))
            <textarea  class="form-control" name="post[options][{{$prefix}}][{{ $type }}][]" cols="30" rows="10" placeholder="{{ $type }}">@if(isset($oPostModel)){{ $oPostModel->options[$prefix][$type] or '' }}@endif</textarea>
        @else
            <textarea  class="form-control" name="post[options][{{ $type }}]" cols="30" rows="10" placeholder="{{ $type }}">@if(isset($oPostModel)){{ $oPostModel->options[$type] or '' }}@endif</textarea>
        @endif
    </div>
@endif
@if($rule === 'integer')
    <div class="form-group">
        <label>{{ $type }}</label>
        @if($type === 'phone')
            <i class="icon-info" title='Используйте разделитель "|" для разделения разных номеров.' data-rel="tooltip" style="cursor: pointer;"></i>
        @endif
        @if(isset($prefix) && isset($item))
            <input type="number" class="form-control" name="post[options][{{$prefix}}][{{ $type }}][]" placeholder="{{ $type }}" @if(isset($oPostModel)) value="{{ $oPostModel->options[$prefix][$item][$type] or '' }}" @endif required>
        @elseif(isset($prefix))
            <input type="number" class="form-control" name="post[options][{{$prefix}}][{{ $type }}][]" placeholder="{{ $type }}" @if(isset($oPostModel)) value="{{ $oPostModel->options[$prefix][$type] or '' }}" @endif required>
        @else
            <input type="number" class="form-control" name="post[options][{{ $type }}]" placeholder="{{ $type }}" @if(isset($oPostModel)) value="{{ $oPostModel->options[$type] or '' }}" @endif required>
        @endif
    </div>
@endif