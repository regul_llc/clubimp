<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form class="modal-content ajax-form" action="{{ url('/admin/posts/action/edit-post') }}" method="POST"
              data-list=".ajax-table-block, .ajax-block-rules, .ajax-block-template"
              data-list-action="{{ url('/admin/posts/action/get-post-table') }}, {{ url('/admin/posts/action/get-post-rules') }}, {{ url('/admin/posts/action/get-post-template') }}"
              data-callback="refreshAfterSubmit, closeModalAfterSubmit"
              data-form-data="#post-form"
        >
            <div class="modal-header">
                <h4 class="modal-title">Изменить пост</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="post[id]" value="{{ $oPost->id }}">
                <div class="form-group">
                    <label>{{ trans('admin.title') }}</label>
                    <input type="text" class="form-control" name="post[title]" placeholder="{{ trans('admin.title') }}" value="{{ $oPost->title }}" required>
                </div>
                <div class="form-group">
                    <label>{{ trans('admin.type') }}</label>
                    <i class="icon-info" title="При смене типа поля будут сброшены" data-rel="tooltip" style="cursor: pointer;"></i>
                    <select class="form-control" name="post[post_type_id]">
                        @foreach($aComposerPostTypes as $key => $aComposerPostType)
                            <option value="{{ $aComposerPostType['id'] }}" @if($aComposerPostType['id'] === $oPost->post_type_id) selected @endif>{{ $aComposerPostType['title'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>{{ trans('admin.status') }}</label>
                    <select class="form-control" name="post[status]">
                        @foreach(Model::init('post')->getStatuses() as $key => $status)
                            <option value="{{ $key }}" @if($key === $oPost->status) selected @endif>{{ $status }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('admin.submit.close') }}</button>
                <button type="submit" class="btn btn-primary inner-form-submit">{{ trans('admin.submit.edit') }}</button>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->