<table class="table table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>{{ trans('admin.title') }}</th>
        <th>{{ trans('admin.type') }}</th>
        <th>{{ trans('admin.status') }}</th>
        <th>{{ trans('admin.updated_at') }}</th>
        <th>{{ trans('admin.created_at') }}</th>
        <th>{{ trans('admin.pages') }}</th>
    </tr>
    </thead>
    <tbody>
    @if(count($oPost->pages))
        @foreach($oPost->pages as $key => $oPage)
            <tr>
                @if($key === 0)
                    <td rowspan="{{ count($oPost->pages) }}">{{ $oPost->id }}</td>
                    <td rowspan="{{ count($oPost->pages) }}">
                        <a href="/admin/posts/edit/{{ $oPost->id }}" title="Изменить" data-rel="tooltip">
                            {{ $oPost->title }}
                        </a>
                    </td>
                    <td rowspan="{{ count($oPost->pages) }}">{{ $oPost->type->title }}</td>
                    <td rowspan="{{ count($oPost->pages) }}">
                        <span class="{{ $oPost->status_icon['class'] }}" title="{{ $oPost->status_text }}">{{ $oPost->status_icon['title'] }}</span>
                    </td>
                    <td rowspan="{{ count($oPost->pages) }}">
                        {{ $oPost->updated_at->format('d.m.Y H:i:s') }}
                    </td>
                    <td rowspan="{{ count($oPost->pages) }}">
                        {{ $oPost->created_at->format('d.m.Y H:i:s') }}
                    </td>
                @endif
                <td>
                    <a href="/admin/pages/edit/{{ $oPage->id }}" target="_blank" title="Изменить" data-rel="tooltip">
                        {{ $oPage->title }}
                    </a>
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td>{{ $oPost->id }}</td>
            <td>
                <a href="/admin/posts/edit/{{ $oPost->id }}" title="Изменить" data-rel="tooltip">
                    {{ $oPost->title }}
                </a>
            </td>
            <td>{{ $oPost->type->title }}</td>
            <td>
                <span class="{{ $oPost->status_icon['class'] }}" title="{{ $oPost->status_text }}">{{ $oPost->status_icon['title'] }}</span>
            </td>
            <td>
                {{ $oPost->updated_at->format('d.m.Y H:i:s') }}
            </td>
            <td>
                {{ $oPost->created_at->format('d.m.Y H:i:s') }}
            </td>
            <td>
                Нет страниц
            </td>
        </tr>
    @endif

    </tbody>
</table>