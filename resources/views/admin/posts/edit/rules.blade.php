<div class="row">
    <div class="col-lg-12">
        <form class="card ajax-form" action="{{ url('/admin/posts/action/edit-post-options') }}" method="POST"
              data-list=".ajax-block-rules"
              data-list-action="{{ url('/admin/posts/action/get-post-rules') }}"
              data-callback="refreshAfterSubmit"
              data-form-data="#post-form"
        >
            <input type="hidden" name="post[id]" value="{{ $oPost->id }}">
            <div class="card-header">
                <i class="fa fa-align-justify"></i>
                Поля
                @if(is_null($oPost->options))
                    <span class="badge badge-danger float-right">Не выбрано</span>
                @endif
            </div>
            <div class="card-block">
                @foreach($oPost->type->rules as $key => $rule)
                    @if($key === 'one')

                    @else
                        @include('admin.posts.edit.components.rules', [
                            'type' => $key,
                            'rule' => $rule,
                            'oPostModel' => $oPost
                        ])
                    @endif
                @endforeach
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary inner-form-submit" data-style="slide-down">{{ trans('admin.submit.save') }}</button>
            </div>
        </form>
    </div>
</div>
@if(isset($oPost->type->rules['one']))
    @if(isset($oPost->options['one']))
        <form class="card ajax-form" action="{{ url('/admin/posts/action/edit-post-options-block') }}" method="POST"
              data-list=".ajax-table-block, .ajax-block-rules"
              data-list-action="{{ url('/admin/posts/action/get-post-table') }}, {{ url('/admin/posts/action/get-post-rules') }}"
              data-callback="refreshAfterSubmit"
              data-form-data="#post-form"
        >
            <input type="hidden" name="post[id]" value="{{ $oPost->id }}">
            <div class="card-header">
                <i class="fa fa-align-justify"></i>
                Поля
            </div>
            <div class="card-block">
                @foreach($oPost->options['one'] as $key => $block)
                    <strong>Блок {{ $loop->iteration }}</strong>
                    @foreach($block as $subKey => $value)
                        @include('admin.posts.edit.components.rules', [
                            'type' => $subKey,
                            'rule' => $oPost->type->rules['one'][$subKey],
                            'oPostModel' => $oPost,
                            'prefix' => 'one',
                            'item' => $key
                        ])
                    @endforeach
                    <hr>
                @endforeach
                <strong>Новый блок</strong>
                @foreach($oPost->type->rules['one'] as $key => $one)
                    @include('admin.posts.edit.components.rules', [
                        'type' => $key,
                        'rule' => $one,
                        'prefix' => 'one',
                        'new' => true
                    ])
                @endforeach
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary inner-form-submit" data-style="slide-down">{{ trans('admin.submit.save') }}</button>
            </div>
        </form>
    @else
        <form class="card ajax-form" action="{{ url('/admin/posts/action/edit-post-options-block') }}" method="POST"
              data-list=".ajax-table-block, .ajax-block-rules"
              data-list-action="{{ url('/admin/posts/action/get-post-table') }}, {{ url('/admin/posts/action/get-post-rules') }}"
              data-callback="refreshAfterSubmit"
              data-form-data="#post-form"
        >
            <input type="hidden" name="post[id]" value="{{ $oPost->id }}">
            <div class="card-header">
                <i class="fa fa-align-justify"></i>
                Post options
            </div>
            <div class="card-block">
                <strong>Новый блок</strong>
                @foreach($oPost->type->rules['one'] as $key => $one)
                    @include('admin.posts.edit.components.rules', [
                        'type' => $key,
                        'rule' => $one,
                        'prefix' => 'one',
                        'new' => true
                    ])
                @endforeach
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary inner-form-submit" data-style="slide-down">{{ trans('admin.submit.add') }}</button>
            </div>
        </form>
    @endif
@endif