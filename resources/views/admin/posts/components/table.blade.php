<table class="table table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>{{ trans('admin.title') }}</th>
        <th>{{ trans('admin.type') }}</th>
        <th>{{ trans('admin.status') }}</th>
        <th>{{ trans('admin.updated_at') }}</th>
        <th>{{ trans('admin.created_at') }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($oPosts as $oPost)
        <tr>
            <td>{{ $oPost->id }}</td>
            <td>
                <a href="/admin/posts/edit/{{ $oPost->id }}" title="Изменить" data-rel="tooltip">
                    {{ $oPost->title }}
                </a>
            </td>
            <td>{{ $oPost->type->title }}</td>
            <td>
                <span class="{{ $oPost->status_icon['class'] }}" title="{{ $oPost->status_text }}">{{ $oPost->status_icon['title'] }}</span>
            </td>
            <td>
                {{ $oPost->updated_at->format('d.m.Y H:i:s') }}
            </td>
            <td>
                {{ $oPost->created_at->format('d.m.Y H:i:s') }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@if($oPosts instanceof \Illuminate\Pagination\LengthAwarePaginator)
    @if(isset($aSearch))
        {{ $oPosts->appends($aSearch)->links('defaults.admin.pagination') }}
    @else
        {{ $oPosts->links('defaults.admin.pagination') }}
    @endif
@endif