<div class="row">
    <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-12">
                <form class="card" action="/admin/posts/action/create-post" method="POST">
                    {{ csrf_field() }}
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>
                        Form
                    </div>
                    <div class="card-block">
                        <div class="form-group">
                            <label>{{ trans('admin.title') }}</label>
                            <input type="text" class="form-control" name="post[title]" placeholder="{{ trans('admin.title') }}" value="">
                        </div>
                        <div class="form-group">
                            <label>{{ trans('admin.type') }}</label>
                            <i class="icon-info" title="При смене типа поля будут сброшены" data-rel="tooltip" style="cursor: pointer;"></i>
                            <select class="form-control" name="post[post_type_id]">
                                @foreach($aComposerPostTypes as $key => $aComposerPostType)
                                    <option value="{{ $aComposerPostType['id'] }}">{{ $aComposerPostType['title'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>{{ trans('admin.status') }}</label>
                            <select class="form-control" name="post[status]">
                                @foreach(Model::init('post')->getStatuses() as $key => $status)
                                    <option value="{{ $key }}">{{ $status }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" data-style="slide-down">{{ trans('admin.submit.create') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>