<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i>
                Посты
            </div>
            <div class="card-block">
                @if(isset($oPosts))
                    @include('admin.posts.components.table', [
                        'oPosts' => $oPosts
                    ])
                @endif
            </div>
        </div>
    </div>
</div>
