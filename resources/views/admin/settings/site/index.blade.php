<div class="row">
    <div class="col-sm-6">

        <form class="card ajax-form" action="{{ url('/admin/settings/action/edit-settings') }}" method="POST">
            <div class="card-header">
                <strong>{{ trans('admin.settings') }}</strong>
            </div>
            <div class="card-block">
                <div class="form-group">
                    <label>{{ trans('admin.copyright') }}</label>
                    <input type="text" class="form-control" name="copyright" placeholder="{{ trans('admin.copyright') }}" value="{{ $aComposerSettings['site']['copyright']['text'] or ''}}">
                </div>
                <div class="form-group">
                    <label>{{ trans('admin.link') }}</label>
                    <input type="text" class="form-control" name="link" placeholder="{{ trans('admin.link') }}" value="{{ $aComposerSettings['site']['copyright']['link'] or ''}}">
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary inner-form-submit">{{ trans('admin.submit.edit') }}</button>
            </div>
        </form>
    </div>
</div>
