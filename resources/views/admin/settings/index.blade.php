<div class="row">
    <div class="col-sm-6 col-md-4">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-spin fa-cog"></i><a href="/admin/settings/site">{{ trans('admin.site') }}</a>
            </div>
            <div class="card-block">
                Настройки сайта
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-spin fa-cog"></i><a href="/admin/settings/users">{{ trans('admin.users') }}</a>
            </div>
            <div class="card-block">
                Настройки пользователей
            </div>
        </div>
    </div>
</div>
