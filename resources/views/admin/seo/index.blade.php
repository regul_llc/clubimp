<div class="row">
    <div class="col-sm-6">

        <form class="card ajax-form" action="/admin/seo/action/edit-seo" method="POST">
            {{ csrf_field() }}
            <div class="card-header">
                <strong>Seo {{ trans('admin.settings') }}</strong>
            </div>
            <div class="card-block">
                <div class="form-group">
                    <label>{{ trans('admin.title') }}</label>
                    <input type="text" class="form-control" name="title" placeholder="{{ trans('admin.title') }}" value="{{ $aComposerSettings['site']['title'] or ''}}">
                </div>

                <div class="form-group">
                    <label>{{ trans('admin.description') }}</label>
                    <textarea name="description" rows="9" class="form-control" placeholder="{{ trans('admin.description') }}...">{{ $aComposerSettings['site']['description'] or ''}}</textarea>
                </div>

                <div class="form-group">
                    <label>Keywords</label>
                    <textarea name="keywords" rows="9" class="form-control" placeholder="key, word, something">{{ $aComposerSettings['site']['keywords'] or ''}}</textarea>
                </div>

                <div class="form-group">
                    <label>Favicon</label>
                    <img src="{{ $aComposerSettings['site']['favicon'] or ''}}" alt="" width="100">
                </div>
                <div class="form-group row">
                    <label class="col-md-3 form-control-label" for="file-input">File input</label>
                    <div class="col-md-9">
                        <input type="file" id="file-input" name="favicon" value="{{ $aComposerSettings['site']['favicon'] or ''}}">
                    </div>
                </div>

            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary inner-form-submit" data-style="slide-down">{{ trans('admin.submit.save') }}</button>
            </div>
        </form>
    </div>
</div>

