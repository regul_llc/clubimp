<table class="table table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>Заголовок</th>
        <th>Url</th>
        <th>Посты (всего/активных)</th>
        <th>Статус</th>
        <th>Дата изменения</th>
        <th>Дата создания</th>
    </tr>
    </thead>
    <tbody>
    @foreach($oPages as $oPage)
        <tr>
            <td>{{ $oPage->id }}</td>
            <td>
                <a href="/admin/pages/edit/{{ $oPage->id }}" title="Изменить" data-rel="tooltip">
                    {{ $oPage->title }}
                </a>
            </td>
            <td>
                @if($oPage->status === 2)
                    <a href="{{ url('/') }}" target="_blank" title="{{ url('/') }}" data-rel="tooltip">
                        {{ str_limit(url('/'), 30) }}
                    </a>
                    <br>
                @endif
                <a href="{{ url('/pages/'.$oPage->url) }}" target="_blank" title="{{ url('/pages/'.$oPage->url) }}" data-rel="tooltip">
                    {{ str_limit(url('/pages/'.$oPage->url), 30) }}
                </a>
            </td>
            <td>{{ count($oPage->posts) }}/{{ count($oPage->activePosts) }}</td>
            <td>
                <span class="{{ $oPage->status_icon['class'] }}" title="{{ $oPage->status_text }}">{{ $oPage->status_icon['title'] }}</span>
            </td>
            <td>
                {{ $oPage->updated_at->format('d.m.Y H:i:s') }}
            </td>
            <td>
                {{ $oPage->created_at->format('d.m.Y H:i:s') }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@if($oPages instanceof \Illuminate\Pagination\LengthAwarePaginator)
    @if(isset($aSearch))
        {{ $oPages->appends($aSearch)->links('defaults.admin.pagination') }}
    @else
        {{ $oPages->links('defaults.admin.pagination') }}
    @endif
@endif