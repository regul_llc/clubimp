<div class="row">
    <div class="col-lg-6">
        <form class="card ajax-form" action="/admin/pages/action/create-page" method="POST">
            <div class="card-header">
                <i class="fa fa-align-justify"></i>
                Добавить страницу
            </div>
            <div class="card-block">
                <div class="form-group">
                    <label>Заголовок</label>
                    <input type="text" class="form-control" name="page[title]" placeholder="Title" value="" required>
                </div>
                <div class="form-group">
                    <label>Статус</label>
                    <select class="form-control" name="page[status]">
                        @foreach(Model::init('page')->getStatuses() as $key => $status)
                            <option value="{{ $key }}">{{ $status }}</option>
                        @endforeach
                    </select>
                </div>
                <hr>
                <div class="form-group">
                    <label>Seo Description</label>
                    <textarea name="page[seo][description]" rows="9" class="form-control" placeholder="Description..."></textarea>
                </div>

                <div class="form-group">
                    <label>Seo Keywords</label>
                    <textarea name="page[seo][keywords]" rows="9" class="form-control" placeholder="key, word, something"></textarea>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary inner-form-submit" data-style="slide-down">{{ trans('admin.submit.create') }}</button>
            </div>
        </form>
    </div>
</div>
