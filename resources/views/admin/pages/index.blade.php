<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i>
                Страницы
            </div>
            <div class="card-block">
                @if(isset($oPages))
                    @include('admin.pages.components.table', [
                        'oPages' => $oPages
                    ])
                @endif
            </div>
        </div>
    </div>
</div>
