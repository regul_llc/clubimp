@foreach($oPage->posts as $oPost)
    <div class="row">
        <div class="col-lg-12">
            <form id="delete-page-post-form-{{$oPost->pivot->id}}" action="{{ url('/admin/pages/action/delete-page-post') }}" method="POST" >
                <input type="hidden" name="id" value="{{ $oPost->pivot->id }}">
            </form>
            <form class="ajax-form" action="{{ url('/admin/pages/action/delete-page-post') }}" method="POST"
                  data-list=".page-posts, .page-table-block,"
                  data-list-action="{{ url('/admin/pages/action/get-page-posts') }}, {{ url('/admin/pages/action/get-page-table') }}"
                  data-callback="refreshAfterSubmit"
                  data-ajax-init="tooltip"
                  data-form-data="#page-form"
                  style="position: absolute;top: 7px;right: 25px;z-index: 1;"
            >
                <input type="hidden" name="id" value="{{ $oPost->pivot->id }}">
                <button type="submit" class="btn btn-sm btn-primary inner-form-submit" data-style="slide-down">
                    <i class="fa fa-trash"></i>
                </button>
            </form>
            <form class="card ajax-form" action="{{ url('/admin/pages/action/edit-page-post') }}" method="POST"
                  data-list=".page-table-block"
                  data-list-action="{{ url('/admin/pages/action/get-page-table') }}"
                  data-callback="refreshAfterSubmit"
                  data-form-data="#page-form"
                  data-ajax-init="tooltip"
            >
                <input type="hidden" name="post[id]" value="{{ $oPost->id }}">
                <input type="hidden" name="post[page_id]" value="{{ $oPage->id }}">
                <input type="hidden" name="post[pivot_id]" value="{{ $oPost->pivot->id }}">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i>
                    #{{ $oPost->id }}: {{ $oPost->title }}

                </div>
                <div class="card-block">
                    <div class="form-group">
                        <label>Приоритет</label>
                        <input type="text" class="form-control" name="post[priority]" placeholder="Приоритет" value="{{ $oPost->priority }}" required>
                    </div>
                    <div class="form-group">
                        <label>Статус</label>
                        <select class="form-control" name="post[status]">
                            @foreach(Model::init('page_posts')->getStatuses() as $key => $status)
                                <option value="{{ $key }}" @if($key === $oPost->pivot_status) selected @endif>{{ $status }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary inner-form-submit" data-style="slide-down">{{ trans('admin.submit.edit') }}</button>
                </div>
            </form>
        </div>
    </div>
@endforeach