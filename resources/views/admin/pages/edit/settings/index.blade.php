<div class="row">
    <div class="col-md-12">
        @foreach($oPage->posts as $oPost)
        <div class="row dragdrop">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header drag">
                        {{ $oPost->title }}

                        <div class="card-actions">
                            <div style="display: block;float: left;padding: 0.75rem 0;width: 50px;">
                                <label class="switch switch-sm switch-text switch-info mb-0">
                                    <input type="checkbox" class="switch-input" @if($oPost->status) checked @endif>
                                    <span class="switch-label" data-on="On" data-off="Off"></span>
                                    <span class="switch-handle"></span>
                                </label>
                            </div>
                            <a href="#" class="btn-close"><i class="icon-close"></i></a>
                        </div>
                    </div>
                    <div class="card-block">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex
                        ea commodo consequat.
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-4">
                                <strong>Текущий статус поста:</strong>
                                <span class="{{ $oPost->status_icon['class'] }}" title="{{ $oPost->status_text }}">{{ $oPost->status_icon['title'] }}</span>
                            </div>
                            <div class="col-md-4">
                                <strong>Текущий статус поста на странице:</strong>
                                <span class="{{ $oPost->pivot_status_icon['class'] }}" title="{{ $oPost->pivot_status_text }}">{{ $oPost->pivot_status_icon['title'] }}</span>
                            </div>
                            <div class="col-md-4">
                                <strong>Тип:</strong> {{ $oPost->type->title }}
                                <span class="float-right">
                                    <i class="fa fa-arrow-circle-o-up text-success"></i>
                                    15%
                                </span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>