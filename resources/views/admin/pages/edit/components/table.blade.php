@if(count($oPage->posts))
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th>Заголовок</th>
            <th>Url</th>
            <th>Статус</th>
            <th>Заголовок поста</th>
            <th>Статус поста</th>
            <th>Приоритет поста на этой странице</th>
            <th>Статус поста на этой странице</th>
        </tr>
        </thead>
        <tbody>
        @foreach($oPage->posts as $key => $oPost)
            <tr>
                @if($key === 0)
                    <td rowspan="{{ count($oPage->posts) }}">{{ $oPage->id }}</td>
                    <td rowspan="{{ count($oPage->posts) }}">
                        <a href="/admin/pages/edit/{{ $oPage->id }}" target="_blank">
                            {{ $oPage->title }}
                        </a>
                    </td>
                    <td rowspan="{{ count($oPage->posts) }}">
                        @if($oPage->status === 2)
                            <a href="{{ url('/') }}" target="_blank" title="{{ url('/') }}" data-rel="tooltip">
                                {{ str_limit(url('/'), 30) }}
                            </a>
                            <br>
                        @endif
                        <a href="{{ url('/pages/'.$oPage->url) }}" target="_blank" title="{{ url('/pages/'.$oPage->url) }}" data-rel="tooltip">
                            {{ str_limit(url('/pages/'.$oPage->url), 30) }}
                        </a>
                    </td>
                    <td rowspan="{{ count($oPage->posts) }}">
                        <span class="{{ $oPage->status_icon['class'] }}" title="{{ $oPage->status_text }}">{{ $oPage->status_icon['title'] }}</span>
                    </td>
                @endif
                <td>
                    <a href="/admin/posts/edit/{{ $oPost->id }}" target="_blank">
                        {{ $oPost->title }}
                    </a>
                </td>
                <td>
                    <span class="{{ $oPost->status_icon['class'] }}" title="{{ $oPost->status_text }}">{{ $oPost->status_icon['title'] }}</span>
                </td>
                <td>{{ $oPost->priority }}</td>
                <td>
                    <span class="{{ $oPost->pivot_status_icon['class'] }}" title="{{ $oPost->pivot_status_text }}">{{ $oPost->pivot_status_icon['title'] }}</span>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    На этой странице нет постов
@endif
