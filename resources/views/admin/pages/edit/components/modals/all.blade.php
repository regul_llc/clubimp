<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form class="modal-content ajax-form" action="{{ url('/admin/pages/action/edit-page') }}" method="POST"
              data-list=".page-table-block"
              data-list-action="{{ url('/admin/pages/action/get-page-table') }}"
              data-callback="refreshAfterSubmit, closeModalAfterSubmit"
              data-form-data="#page-form"
              data-ajax-init="tooltip"
        >
            <div class="modal-header">
                <h4 class="modal-title">Изменить страницу</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="page[id]" value="{{ $oPage->id }}">
                <div class="form-group">
                    <label>Заголовок</label>
                    <input type="text" class="form-control" name="page[title]" placeholder="Заголовок" value="{{ $oPage->title }}" required>
                </div>
                <div class="form-group">
                    <label>Статус</label>
                    <select class="form-control" name="page[status]">
                        @foreach(Model::init('page')->getStatuses() as $key => $status)
                            <option value="{{ $key }}" @if($key === $oPage->status) selected @endif>{{ $status }}</option>
                        @endforeach
                    </select>
                </div>
                <hr>
                <div class="form-group">
                    <label>Url</label>
                    <input type="text" class="form-control" name="page[url]" placeholder="Title" value="{{ $oPage->url }}" required>
                </div>
                <div class="form-group">
                    <label>Seo Description</label>
                    <textarea name="page[seo][description]" rows="9" class="form-control" placeholder="Description...">{{ $oPage['seo']['description'] or ''}}</textarea>
                </div>
                <div class="form-group">
                    <label>Seo Keywords</label>
                    <textarea name="page[seo][keywords]" rows="9" class="form-control" placeholder="key, word, something">{{ $oPage['seo']['keywords'] or ''}}</textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('admin.submit.close') }}</button>
                <button type="submit" class="btn btn-primary inner-form-submit">{{ trans('admin.submit.edit') }}</button>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->