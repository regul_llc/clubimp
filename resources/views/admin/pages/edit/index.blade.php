{{--<input id="global-data" type="text" value="{'page_id': '{{$oPage->id}}'}" style="display: none;">--}}

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i>
                {{ $oPage->title }}
                <div class="card-actions">
                    <a href="#" class="btn-close" onclick="event.preventDefault();document.getElementById('page-form').submit();"
                        data-rel="tooltip" title="Удалить"
                    >
                        <i class="icon-close"></i>
                    </a>
                </div>
                <form id="page-form" action="{{ url('/admin/pages/action/delete-page') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $oPage->id }}">
                </form>
            </div>
            <div class="card-block page-table-block">
                @if(isset($oPage))
                    @include('admin.pages.edit.components.table', [
                        'oPage' => $oPage
                    ])
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-12">
                <form class="card ajax-form" action="{{ url('/admin/pages/action/append-page-post') }}" method="POST"
                      data-list=".page-table-block, .page-posts"
                      data-list-action="{{ url('/admin/pages/action/get-page-table') }}, {{ url('/admin/pages/action/get-page-posts') }}"
                      data-callback="refreshAfterSubmit"
                      data-form-data="#page-form"
                      data-ajax-init="tooltip"
                >
                    <input type="hidden" name="post[page_id]" value="{{ $oPage->id }}">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>
                        Добавить новый пост на страницу
                        <span class="badge badge-info float-right">Добавить новый пост</span>
                    </div>
                    <div class="card-block">
                        <div class="form-group">
                            <label>Пост</label>
                            <select class="form-control" name="post[id]">
                                @foreach($oPosts as $key => $oPost)
                                    <option value="{{ $oPost->id }}">{{ $oPost->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Приоритет</label>
                            <input type="text" class="form-control" name="post[priority]" placeholder="Приоритет" value="0" required>
                        </div>
                        <div class="form-group">
                            <label>Статус</label>
                            <select class="form-control" name="post[status]">
                                @foreach(Model::init('page_posts')->getStatuses() as $key => $status)
                                    <option value="{{ $key }}">{{ $status }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary inner-form-submit" data-style="slide-down">{{ trans('admin.submit.add') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-6 page-posts">
        @include('admin.pages.edit.posts', [
            'oPage' => $oPage
        ])
    </div>
</div>