/**
 * -------------------------------------------
 * Lodash
 * -------------------------------------------
 *
 */
require('lodash/lodash.min.js');

/**
 * -------------------------------------------
 * Cleave.js phone mask
 * -------------------------------------------
 *
 */
require('cleave.js/dist/cleave.min.js');


/**
 * -------------------------------------------
 * Append laravel token
 * -------------------------------------------
 *
 */
window.Laravel = $('meta[name="csrf-token"]').attr('content');

$.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': window.Laravel }
});


console.log('app/app.js');

/*
require('../../../clubimp_layout/assets/bootstrap/js/bootstrap.min.js');
require('../../../clubimp_layout/assets/js/jquery.mb.YTPlayer.min.js');
require('../../../clubimp_layout/assets/js/jquery.backstretch.min.js');
require('../../../clubimp_layout/assets/js/owl.carousel.min.js');
require('../../../clubimp_layout/assets/js/jquery.fitvids.js');
require('../../../clubimp_layout/assets/js/wow.min.js');

require('../../../clubimp_layout/assets/js/typed.min.js');
require('../../../clubimp_layout/assets/js/jquery.countdown.min.js');
require('../../../clubimp_layout/assets/js/jqBootstrapValidation.js');
require('../../../clubimp_layout/assets/js/smoothscroll.js');
require('../../../clubimp_layout/assets/js/contact.js');
require('../../../clubimp_layout/assets/js/custom.js');
require('../../../clubimp_layout/js/plugins/jquery.mask.min.js');
require('../../../clubimp_layout/js/project/mask.js');
require('../../../clubimp_layout/assets/js/fss.js');
require('../../../clubimp_layout/assets/js/fss-settings.js');
*/