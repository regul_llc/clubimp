$(document).ready(function($){
    window['tooltip']();
});

window['tooltip'] = function() {
    $('[rel="tooltip"],[data-rel="tooltip"]').tooltip({"placement":"top",delay: { show: 200, hide: 200 }});
};