$(document).ready(function() {
    initFileUploader();
});

/**
 * @see ./blueimp.js
 */
window['uploader'] = function() {
    initFileUploader();
};

var initFileUploader = function() {
    $('#file').fileupload({
        dataType: 'json',
        sequentialUploads: true,
        singleFileUploads: false,
        add: function (e, data) {
            $('#file').closest('form').find('.help').remove();
            console.log(data.formData);
            data.formData = {id: $('#file').data('id')};
            $('#file + label').addClass('is-loading');
            data.submit();
        },
        done: function (e, data) {
            console.log(data.result);
            $('#file + label').removeClass('is-loading');
            var $target = $('#file');
            if (data.result.success) {
                $target.closest('form').find('.is-modal-body').html(data.result.view);
                if (data.result.src) {
                    $('img[data-user-image]').attr('src', data.result.src);
                }
            }

        },
        error: function (data, textStatus, errorThrown) {
            for (var key in data.responseJSON) {
                $('#file').closest('form').prepend('<span class="help has-text-centered is-danger">' + data.responseJSON[key][0] + '</span>');
            }

            $('#file + label').removeClass('is-loading');
            console.log('error');
        }
    });
};