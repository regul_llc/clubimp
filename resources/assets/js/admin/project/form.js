var ajaxForm = {

    options : {
        ajaxLink: true,
        ajaxForm: true,
        ajaxTabs: true,
        submit: '.btn.inner-form-submit'
    },

    form : null,

    bind : function (sElem, sDelegateFrom, sAction) {
        sDelegateFrom = sDelegateFrom || '';
        sAction = sAction || 'submit';
        var fn = function (event) {
            event.preventDefault;
            this.form = $(event.currentTarget);
            this.send();
            return false;
        };
        fn = _.bind(fn, this);
        if(sDelegateFrom){
            $(sDelegateFrom).on(sAction, sElem, fn);
        } else {
            $(sElem).on(sAction, fn);
        }
    },

    send : function () {
        if(this.form.attr('method') === 'get'){

        } else {
            this.post();
        }
    },

    post : function () {
        var data;
        if (this.form.data('search')) {
            if (this.form.val().length !== 0 && this.form.val().length < 3) {
                return;
            } else {
                data = {search : this.form.val()};
            }
        } else {
            data = this.getFormData();
        }
        this.clearValidate();
        if(!this.validate()) {
            return;
        }
        this.before();
        var self = this;
        $.ajax({
            url: this.form.attr('action'),
            type: "POST",
            data: data,
            success: function (result) {
                self.stopLoading();

                if (result.toastr) {
                    self.notification(result.toastr);
                }
                if(result.success) {
                    self.after(result);
                } else if(result.error) {
                    self.showError(result.message);
                }
                if (result.item) {
                    self.setItemError(result.item);
                }
                if (self.form.data('pagination') && result.view) {
                    $(self.form.data('pagination-container')).html(result.view);
                }
                if (self.form.data('search') && result.view) {
                    $(self.form.data('pagination-container')).html(result.view);
                }
                if (self.form.data('tab') && result.view) {
                    $('.ajax-tabs').find('li').removeClass('is-active');
                    self.form.closest('li').addClass('is-active');
                    $(self.form.data('container')).html(result.view);
                }
                if (result.push) {
                    self.push(result.push);
                }
            },
            error: function (data, status, headers, config) {
                self.stopLoading();

                self.validateServer(data);
            }
        });
    },

    notification : function(notification) {
        toastr.options.closeButton = true;
        toastr.options.closeDuration = 10;
        switch(notification.type) {
            case 'warning':
                toastr.warning(notification.text, notification.title, notification.options);
                break;
            case 'success':
                toastr.success(notification.text, notification.title, notification.options);
                break;
            case 'error':
                toastr.error(notification.text, notification.title, notification.options);
                break;
            case 'info':
                toastr.info(notification.text, notification.title, notification.options);
                break;
            default:
                break;
        }
    },

    push : function(data) {
        /*
        var title = data.title ? data.title : 'Новое сообщение';
        var text = data.text ? data.text : '';
        var icon = data.icon ? data.icon : '';
        var timeout = data.timeout ? data.timeout : 10000;

        Push.create(title, {
            body: text,
            icon: icon,
            timeout: timeout,
            vibrate: [200,100],
            onClick: function () {
                window.focus();
                this.close();
            }
        });
        */
    },

    get : function () {

    },

    submit : function () {

    },

    stopLoading : function() {
        this.form.find('input').closest('.control').removeClass('is-loading');
        this.form.find('.with-loading').removeClass('is-loading');
        this.form.removeClass('is-loading');
        this.form.closest('.select').removeClass('is-loading');
        this.form.find('select').closest('.select').removeClass('is-loading');
        this.form.find(this.options.submit).removeClass('is-loading');

        if (this.form.data('preloader')) {
            $(this.form.data('preloader')).addClass('hidden')
        }
    },

    after : function (result) {
        var sCallbacks = this.form.data('callback') || result.callback;
        var bDefaultsCall = true;
        var self = this;
        if(sCallbacks) {
            var aCallbacks = _.split(sCallbacks, ',');
            if(_.first(aCallbacks) == '@'){
                bDefaultsCall = false;
                aCallbacks = _.drop(aCallbacks);
            }
            _.each(aCallbacks, function (val) {
                var sFuncName = _.trim(val);
                if (_.isFunction(window[sFuncName])){
                    window[sFuncName](result, self.form);
                }
            });
        }
        var sInit = this.form.data('ajax-init');
        if (sInit) {
            var aInit = _.split(sInit, ',');
            _.each(aInit, function (val) {
                var sFuncName = _.trim(val);
                if (_.isFunction(window[sFuncName])){
                    window[sFuncName]();
                }
            });
        }

        if(bDefaultsCall) {
            this.afterDefault(result);
        }
        if(result.redirect) {
            window.location.replace(result.redirect);
        }
        if(result.post) {
            if (result.post.form) {
                $('body').append(result.post.form);
                var form = $('#ext_auth_form');
                form.submit();
                form.remove();
            }
        }
        if (result._blank) {
            var linkToDownload = result._blank;
            var downloadLink = document.createElement('a');
            downloadLink.id = "link-to-download";
            downloadLink.href = linkToDownload;
            downloadLink.setAttribute("target", "_blank");
            document.body.appendChild(downloadLink);
            downloadLink.click();
            setTimeout(function() {
                $('#link-to-download').remove();
            }, 1000);
        }

        if (this.form.data('has-active')) {
            var aItems = _.split(this.form.data('items'), ',');
            _.each(aItems, function (val) {
                var sItem = _.trim(val);
                $(sItem).removeClass('is-active');
                if (!self.form.hasClass('is-active')) {
                    self.form.addClass('is-active');
                    if (self.form.get(0).tagName === 'SELECT') {
                        self.form.find('option').removeAttr('selected')
                            .filter('[value='+self.form.val()+']')
                            .attr('selected', true)
                    }
                }
            });
            if (self.form.get(0).tagName !== 'SELECT') {
                console.log('!== SELECT');
                _.each(aItems, function (val) {
                    var sItem = _.trim(val);
                    if ($(sItem).get(0).tagName === 'SELECT') {
                        console.log('=== SELECT');
                        $(sItem).find('option').prop("selected", false);
                    }
                });
            }
        }
    },

    afterDefault : function (result) {
        this.form.find('input, div').removeClass('__error');
        $('.text-error[data-name="error"]').empty();
    },

    before : function () {
        /*
        this.form.find('input').filter(function() {
            return $(this).attr('required') || $(this).val() !== '';
        }).closest('.control').addClass('is-loading');

        this.form.find('select').closest('.select').addClass('is-loading');


        this.form.closest('.select').addClass('is-loading');

        this.form.find('.with-loading').addClass('is-loading');


        if (this.form.data('pagination')) {
            this.form.addClass('is-loading');
        }
        if (this.form.data('loading')) {
            this.form.addClass('is-loading');
        }


        */
        this.form.find(this.options.submit).addClass('is-loading');

        if (this.form.data('preloader')) {
            $(this.form.data('preloader')).removeClass('hidden')
        }
    },

    beforeDefault : function () {

    },

    clearValidate: function () {
        this.form.find('.help.is-danger').remove();
        this.form.find('.form-group.has-danger').removeClass('has-danger');
    },
    validate : function () {
        var $requireds = this.form.find('input[required]');
        var bResult = true;
        $requireds.each(function (i, e) {
            var $elem = $(e);
            var sElemType = $elem.get(0).tagName;
            var bFilled = ($elem.is(':checkbox')) ? $elem.prop('checked') : $elem.val();
            if(!bFilled){
                $elem.addClass('is-danger');
                //$elem.addClass('is-temporary');
                bResult = false;
                switch (sElemType){
                    case 'SELECT':
                        if($elem.hasClass('combobox') || $elem.parent().hasClass('__combobox')){
                            $elem.closest('.form-group').addClass('__error');
                        }
                        if($elem.data('role') == 'chosen-select'){
                            $elem.closest('.form-group').addClass('__error');
                        }
                        break;
                    case 'INPUT':
                        if($elem.is(':checkbox')){
                            $elem.parent().addClass('__error');
                        }else{
                            $elem.addClass('__error');
                        }
                        break;
                    default:
                        $elem.addClass('__error');
                        break;
                }
            }
        });
        return bResult;
    },

    validateServer : function (result) {
        /**
         * Для валидации.
         */
        /*
        var self = this;
        _.each(result.responseJSON, function(field, index) {
            var input = self.form.find('.input[name="' + index + '"]');
            var message = field.responseJSON[index][0];
            input.addClass('is-danger');
            input.parent().append('<span class="help is-danger">' + message + '</span>');
        });
        */

        for (var key in result.responseJSON) {
            var input = this.form.find('.form-control[name="' + key + '"]');
            if (input.length === 0) {
                input = this.form.find('input[name="' + key + '"]');
            }
            input.closest('.form-group').addClass('has-danger');
            var message;
            if (_.isArray(result.responseJSON[key])) {
                message = result.responseJSON[key][0];
            } else {
                message = result.responseJSON[key];
            }
            var $container = input.closest('.form-group');
            if (input.attr('type') === 'radio') {
                $container = $('[data-radio-danger-name="' + key + '"]');
            }
            $container.append('<span class="help is-danger">' + message + '</span>');
            //this.form.find('.text-error[data-name="' + key + '"]').text(result.responseJSON[key][0]);
            if (key === 'error') {
                this.showError(message);
            }
        }
    },

    getFormData : function () {
        var data = {};
        var name;
        if (this.form.get(0).tagName == 'FORM') {
            data = this.form.serialize();
        } else if(this.form.get(0).tagName == 'SELECT') {
            name = this.form.attr('name');
            data['' + name] = this.form.val();
        } else if (this.form.get(0).tagName == 'A' && this.form.attr('value')) {
            name = this.form.attr('name');
            data['' + name] = this.form.attr('value');
        } else {
            data = this.form.data();
        }
        if(!data._token || data._token === undefined) {
            data._token = window.Laravel;
        }
        console.log(data);
        if($('#global-data').length){
            var keyData = '_global-data';
            var out = '';
            var json = $.parseJSON($('#global-data').val());
            _.each(json, function(field, key) {
                out += '&' + keyData + '%5B' + key + '%5D=' + field;
            });
            data = data + out;
        }
        if (this.form.data('merge-data')) {
            data[this.form.data('merge-data-name')] = $(this.form.data('merge-data')).val();
        }
        console.log(data);
        return data;
    },

    showError : function(sMessage) {
        this.form.prepend('<div class="help is-danger text-center" style="margin-bottom: 10px;">' + sMessage + '</div>');
    },

    setItemError : function(item) {
        console.log(item);
        console.log('[data-item-id="' + item + '"]');
        this.form.find('[data-item-id="' + item + '"]').addClass('__error');
    }
};


$(document).ready(function () {
    console.log('ajax-form init');
    ajaxForm.bind('.ajax-form', 'body');
    ajaxForm.bind('.ajax-link', 'body', 'click');
    ajaxForm.bind('.ajax-select', 'body', 'change');
});