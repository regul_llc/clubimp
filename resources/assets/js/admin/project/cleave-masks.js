/*
new Cleave('input[cleave-mask-cart]', {
    delimiters: ['.', '.', '-'],
    blocks: [3, 3, 3, 2],
    uppercase: true
});

new Cleave('input[cleave-mask-date]', {
    date: true,
    datePattern: ['d', 'm', 'Y']
});
*/
var cleaveMasks = {
    phone:      '[data-role="js-mask-phone"]',
    phoneint:   '[data-role="js-mask-phone-int"]',
    int:        '[data-role="js-mask-int"]',
    price:      '[data-role="js-mask-price"]'
};
var cleave;

var initCleave = function($element) {
    cleave = new Cleave($element, {
        prefix: '+7',
        blocks: [2, 0, 3, 0, 3, 2, 2],
        delimiters: [' ', '(', ')', ' ', ' ', '-'],
        numericOnly: true
    });
};
var initCleavePhoneInt = function($element) {
    new Cleave($element, {
        blocks: [11],
        numericOnly: true
    });
};
if ($(cleaveMasks.phone).is(':focus')) {
    initCleave($(cleaveMasks.phone));
}
$('body').on('focus', cleaveMasks.phone, function() {
    initCleave($(this));
});
$('body').on('focusout', cleaveMasks.phone, function() {
    if ($(this).val().length !== 18) {
        $(this).val('');
    }
});
if ($(cleaveMasks.phoneint).is(':focus')) {
    initCleavePhoneInt($(cleaveMasks.phoneint));
}
$('body').on('focus', cleaveMasks.phoneint, function() {
    initCleavePhoneInt($(this));
});
$('body').on('focusout', cleaveMasks.phoneint, function() {
    if ($(this).val().length !== 11) {
        $(this).val('');
    }
});

if ($(cleaveMasks.int).length) {
    new Cleave(cleaveMasks.int, {
        blocks: [2],
        numericOnly: true
    });
}