/**
 * Init callbacks
 */

/*
 * Каллбэк для закрытия модального окна после отправки формы.
 */
window['closeModalAfterSubmit'] = function (result, $target) {
    if(result.success){
        $target.closest('.modal').modal('hide');
    }
};
/*
 * Каллбэк для закрытия модального окна после отправки формы.
 */
window['closeAndTriggerDialogAfterSubmit'] = function (result, $target) {
    if(result.success){
        $target.closest('.modal').modal('hide');
        var action = $target.data('trigger-action');
        var id = $target.data('trigger-action-id');
        $('button[data-action="' + action + '"][data-id="' + id + '"]').trigger('click');
    }
};
/*
 * Каллбэк для закрытия модального окна после отправки формы.
 */
window['closeDialogAfterSubmit'] = function (result, $target) {
    if(result.success){
        var $dialog = $target.closest('.dialog');
        $dialog.find('.dialog__overlay').trigger('click');
    }
};

/*
 * Каллбэк для закрытия модального окна после отправки формы.
 */
window['refreshAfterSubmit'] = function (result, $target) {
    if (result.success && $target.data('list') && $target.data('list-action')) {
        var aLists = _.split($target.data('list'), ',');
        var aListActions  = _.split($target.data('list-action'), ',');
        if ($target.data('preloader')) {
            $($target.data('preloader')).removeClass('hidden')
        }
        _.each(aLists, function (list, key) {
            var $view = $(list);
            var url = aListActions[key];
            var data = null;
            if ($target.data('form-data')) {
                console.log('serializeArray');
                data = $($target.data('form-data')).serializeArray();
            }
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                success: function ($list) {
                    if ($list.view) {
                        $view.html($list.view);
                        if ($target.data('counter')) {
                            var $counter = $($target.data('counter'));
                            $counter.html($list.count);
                        }
                    } else {
                        $view.html($list);
                    }
                    if ($target.data('preloader')) {
                        $($target.data('preloader')).addClass('hidden')
                    }
                },
                error: function (msg) {
                    console.log(msg);
                    if ($target.data('preloader')) {
                        $($target.data('preloader')).addClass('hidden')
                    }
                }
            });
        });
    }
};

window['strongRefreshView'] = function($view, url, $data, $counter) {
    var data = null;
    if ($data) {
        data = $data.serializeArray();
    }
    console.log(url);
    $.ajax({
        url: url,
        type: "POST",
        data: data,
        success: function ($list) {
            if ($list.view) {
                $view.html($list.view);
                if ($counter) {
                    $counter.html($list.count);
                }
            } else {
                $view.html($list);
            }
        },
        error: function (msg) {
            console.log(msg);
        }
    });
};

window['refreshModalAfterSubmit'] = function(result, $target) {
    if (result.success) {
        var $view = $target.closest('.is-modal-body');
        if (result.view) {
            $view.html(result.view);
        } else {
            $view.html(result);
        }
    }
};


window['replaceIconAfterSubmit'] = function(result, $target) {
    if (result.success) {
        var $icon = $target.find('.icon i');
        if ($icon.hasClass($icon.attr('on-class'))) {
            $icon.removeClass();
            $icon.addClass($icon.attr('off-class'));
        } else {
            $icon.removeClass();
            $icon.addClass($icon.attr('on-class'));
        }
    }
};

window['replaceFormAttributesAfterSubmit'] = function(result, $target) {
    if (result.success) {
        var title = $target.attr('title');
        if (title == $target.attr('on-title')) {
            $target.attr('title', $target.attr('off-title'));
        } else {
            $target.attr('title', $target.attr('on-title'));
        }
    }
};
window['showHideAfterSuccess'] = function(result, $target) {
    if (result.success) {
        if (result.view && $target.data('support-view')) {
            $($target.data('support-view')).html(result.view);
        }
        if (result.check || $target.data('without-check')) {
            var hide = $target.data('hide');
            var show = $target.data('show');
            if(hide) {
                var hides = _.split(hide, ',');
                _.each(hides, function (val) {
                    var sSelector = _.trim(val);
                    $(sSelector).addClass('hidden')
                });
            }
            if(show) {
                var shows = _.split(show, ',');
                _.each(shows, function (val) {
                    var sSelector = _.trim(val);
                    $(sSelector).removeClass('hidden')
                });
            }
        } else {
            $('#check-reservation-error').modal('show');
        }
    }
};


