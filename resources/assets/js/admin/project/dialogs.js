$.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': window.Laravel }
});

window['closeDialogCallback'] = function($dialog) {
    $dialog.find('input.is-danger').removeClass('is-danger');
    $dialog.find('.help.is-danger').remove();
    $dialog.find('input').empty();
};

var ajaxDialogs = {

    settings : {
        openDialogClass: 'dialog--open',

        ajax: false,
        confirm: false,
        ajaxClass: 'dialog-ajax',
        disableOverlayClass: 'dialog-disabled_overlay',
        loadingClass: '.dialog__loading',
        loading: '<div class="dialog__loading"><div></div></div>'
    },

    target : null,

    bind : function (sElem, sDelegateFrom, sAction, oSettings) {
        sDelegateFrom = sDelegateFrom || '';
        sAction = sAction || 'submit';
        var fn = function (event) {
            event.preventDefault();
            this.target = $(event.currentTarget);
            this.send();
            return false;
        };
        fn = _.bind(fn, this);
        if(sDelegateFrom){
            $(sDelegateFrom).on(sAction, sElem, fn);
        } else {
            $(sElem).on(sAction, fn);
        }
        var self = this;
        _.each(oSettings, function(field, key) {
            self.settings[key] = field;
        });
        console.log(ajaxDialogs.settings);
    },

    send : function () {
        console.log('send click');
        var $dialog = $(this.target.attr('data-dialog'));
        $dialog.addClass(this.settings.openDialogClass);
        //$dialog.modal('show');

        if (this.settings.ajax) {
            console.log(this.settings.ajax);
            this.addAjax($dialog, this.target);
        }

        if (this.settings.confirm) {
            console.log(this.settings.confirm);
            this.addConfirm($dialog, this.target);
        }

        if (this.target.attr('data-disabled-overlay') !== undefined) {
            $dialog.addClass(this.settings.disableOverlayClass);
        }

    },

    addAjax: function ($dialog, $target) {
        var self = this;
        this.removeAjax($dialog);
        $dialog.addClass(this.settings.ajaxClass);
        $dialog.append(this.settings.loading);
        $dialog.addClass(this.settings.disableOverlayClass);

        setTimeout(function() {
            $dialog.removeClass(self.settings.disableOverlayClass);
        }, 7000);


        $.ajax({
            url: $target.attr('data-action'),
            type: "POST",
            data: self.dataAjax($target),
            success: function (result) {
                $dialog.find(self.settings.loadingClass).remove();
                $dialog.removeClass(self.settings.disableOverlayClass);
                if (result.view) {
                    self.appendAjax($dialog, result.view);
                } else {
                    self.appendAjax($dialog, result);
                }
                self.afterAppend(result);

            },
            error: function(data, status, headers, config) {
                $dialog.removeClass(self.settings.disableOverlayClass);
                self.removeAjax($dialog);
            }
        });
    },
    afterAppend: function(result) {
        if (this.target.attr('data-ajax-init') !== undefined) {
            var aInit = _.split(this.target.attr('data-ajax-init'), ',');
            if(_.first(aInit) == '@'){
                aInit = _.drop(aInit);
            }
            _.each(aInit, function (val) {
                console.log(val);
                var sFuncName = _.trim(val);
                if (_.isFunction(window[sFuncName])){
                    window[sFuncName]();
                }
            });
        }
        if (this.target.attr('data-callback') !== undefined) {
            var aInit = _.split(this.target.attr('data-callback'), ',');
            if(_.first(aInit) == '@'){
                aInit = _.drop(aInit);
            }
            var self = this;
            var $target = self.target;
            _.each(aInit, function (val) {
                var sFuncName = _.trim(val);
                if (_.isFunction(window[sFuncName])){
                    window[sFuncName](result, $target);
                }
            });
        }
    },
    removeAjax: function ($dialog) {
        $dialog.children()
            .filter(function() {
                return (!$(this).hasClass('dialog__overlay'));
            }).remove();
    },
    appendAjax: function($dialog, view) {
        this.removeAjax($dialog);
        $dialog.append(view);
    },
    dataAjax: function($target) {
        return $target.data();
    },
    addConfirm: function($dialog, $target) {
        if ($target.data('text') !== undefined) {
            $dialog.find('.subtitle').text($target.data('text'));
        }
        if ($target.data('action') !== undefined) {
            $dialog.find('.ajax-form').attr('action', $target.data('action'));
        }
        if ($target.data('list-action') !== undefined) {
            $dialog.find('.ajax-form').attr('data-list-action', $target.data('list-action'));
        }
        if ($target.data('id') !== undefined) {
            $dialog.find('.ajax-form input[name="id"]').val($target.data('id'));
        }
    }
};



/*
 |----------------------------------------
 | Dialogs
 |
 | Template:
 |
 | Button
 | <a class="trigger" data-dialog="#register">Регистрация</a>
 | <a class="trigger" data-dialog="#register" data-ajax data-action="url" data-ajax-init="callback, callback">Регистрация</a>
 | <a class="trigger" data-dialog="#register" data-disabled-overlay>Регистрация</a>
 |----------------------------------------
 */
$(document).ready(function() {
    window['bulkInit']();
});

window['bulkInit'] = function() {


    $('.dialog').on('click', '.dialog__close', function() {
        var $dialog = $(this).closest('.dialog');

        if (!$dialog.hasClass(ajaxDialogs.settings.disableOverlayClass)) {
            $dialog.removeClass(ajaxDialogs.settings.openDialogClass);
            if (_.isFunction(window['closeDialogCallback'])){
                window['closeDialogCallback']($dialog);
            }

            if ($dialog.hasClass(ajaxDialogs.settings.ajaxClass)) {
                $dialog.removeClass(ajaxDialogs.settings.ajaxClass);
                ajaxDialogs.removeAjax($dialog);
            }
        }
    });
    $('.dialog').on('click', '.dialog__overlay', function() {
        var $dialog = $(this).closest('.dialog');

        if (!$dialog.hasClass(ajaxDialogs.settings.disableOverlayClass)) {
            $dialog.removeClass(ajaxDialogs.settings.openDialogClass);
            if (_.isFunction(window['closeDialogCallback'])){
                window['closeDialogCallback']($dialog);
            }

            if ($dialog.hasClass(ajaxDialogs.settings.ajaxClass)) {
                $dialog.removeClass(ajaxDialogs.settings.ajaxClass);
                ajaxDialogs.removeAjax($dialog);
            }
        }
    });


    var simpleDialog = $.extend(true, {}, ajaxDialogs);
    var ajaxDialog = $.extend(true, {}, ajaxDialogs);
    var confirmDialog = $.extend(true, {}, ajaxDialogs);

    simpleDialog.bind('.trigger', 'body', 'click', {
        openDialogClass: 'dialog--open'
    });
    ajaxDialog.bind('.trigger[data-ajax]', 'body', 'click', {
        openDialogClass: 'dialog--open',
        ajax: true
    });
    confirmDialog.bind('.trigger[data-confirm]', 'body', 'click', {
        openDialogClass: 'dialog--open',
        confirm: true
    });
};