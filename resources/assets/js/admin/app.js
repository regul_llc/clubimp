/*
window.$ = window.jQuery = require('jquery');

window.select2 = require('select2');
window.Tether = require('tether');
window.Bootstrap = require('bootstrap');
window.autosize = require('autosize');
window.Sortable = require('sortablejs');
window.Moment = require('moment');
*/
//import toastr from 'toastr';
//const toastr = require('toastr/build/toastr.min.js');

require('jquery-ui/ui/widgets/sortable.js');
require('jquery-ui/ui/widgets/draggable.js');

/**
 * -------------------------------------------
 * Lodash
 * -------------------------------------------
 *
 */
require('lodash/lodash.min.js');

/**
 * -------------------------------------------
 * Summernote
 * -------------------------------------------
 *
 */
require('codemirror/lib/codemirror.js');
require('summernote-webpack-fix/dist/summernote.min.js');
require('summernote-webpack-fix/dist/lang/summernote-ru-RU.min.js');

/**
 * -------------------------------------------
 * Jquery Blueimp File Uploader
 * -------------------------------------------
 *
 */
require('blueimp-file-upload/js/vendor/jquery.ui.widget.js');
require('blueimp-file-upload/js/jquery.iframe-transport.js');
require('blueimp-file-upload/js/jquery.fileupload.js');

/**
 * -------------------------------------------
 * Cleave.js phone mask
 * -------------------------------------------
 *
 */
require('cleave.js/dist/cleave.min.js');


/**
 * -------------------------------------------
 * Append laravel token
 * -------------------------------------------
 *
 */
window.Laravel = $('meta[name="csrf-token"]').attr('content');

$.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': window.Laravel }
});

$(function() {
    $('#summernote').summernote({
        lang: 'ru-RU',
        height: 250,                 // set editor height

        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor

        focus: true ,                // set focus to editable area after initializing summernote
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['insert', ['link', 'hr']],
            //['color', ['color']],
            //['height', ['height']],
            ['view', [
                //'fullscreen',
                'codeview'
            ]],
            ['para', ['ul', 'ol', 'paragraph']]
        ]
    });
    $('.dialog').on('click', '.clone-tr', function() {
        var tbody = $(this).closest('tbody');
        var tr = $(this).data('tr');
        var clone = $(tr).clone().css('display', 'table-row').removeClass('temp-ref-value');
        clone.find('input[disabled]').removeAttr('disabled');
        clone.find('select[disabled]').removeAttr('disabled');
        tbody.children('.clone-tr-container').before(clone);
    });
    $('.dialog').on('click', '.delete-clone-tr', function() {
        $(this).closest('tr').remove();
    });
});

