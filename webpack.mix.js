const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 | Env settings 'admin', 'app', 'all', 'first'
 |
 */

var env = 'app';

var manageAssets = {
    app : function(mix) {
        mix.js([
                'resources/assets/js/app/app.js',
                'resources/assets/js/app/main.js',
                'resources/assets/js/admin/project/cleave-masks.js',
                'resources/assets/js/admin/project/form.js',
                'resources/assets/js/admin/project/dialogs.js',
                'resources/assets/js/admin/project/callbacks.js',

            ], 'public/js/app/app.js')
            .copy('resources/clubimp_layout/images', 'public/images', false)
            //.copy('resources/clubimp_layout/', 'public/web', false)
            .sass('resources/assets/sass/app/app.scss', 'public/css/app/app.css')
            .version();
    },
    admin : function(mix) {
        mix.autoload({
            jquery: ['$', 'window.jQuery', 'jQuery', 'jquery'],
            tether: ['window.Tether', 'Tether'],
            'tether-shepherd': ['Shepherd', 'tether-shepherd'],
            toastr: ['window.Toastr', 'toastr'],
            moment: ['window.Moment', 'moment'],
            'chart.js': ['window.Chart', 'Chart']
        })
            .js([
                'resources/assets/js/admin/app.js',
                'resources/assets/js/admin/project/cleave-masks.js',
                'resources/assets/js/admin/project/form.js',
                'resources/assets/js/admin/project/dialogs.js',
                'resources/assets/js/admin/project/callbacks.js',
                'resources/assets/js/admin/template/libs/pace.min.js',
                'resources/assets/js/admin/template/views/shared.js',
                'resources/assets/js/admin/template/app.js',
                'resources/assets/js/admin/template/libs/gauge.min.js',
                'resources/assets/js/admin/template/libs/daterangepicker.js',
                'resources/assets/js/admin/template/views/main.js',
                'resources/assets/js/admin/template/views/draggable-cards.js',

                'resources/assets/js/admin/project/tooltip.js'
            ], 'public/js/admin/app.js')
            .sass('resources/assets/sass/admin/app.scss', 'public/css/admin/app.css')
            .extract([
                'jquery',
                'toastr',
                'bootstrap',
                'moment',
                'tether',
                'tether-shepherd',
                'chart.js',
                'spin.js'
            ]);
            /*
             .combine([
             'public/js/admin/manifest.js',
             'public/js/admin/vendor.js',
             'public/js/admin/app.js'
             ], 'public/js/admin.app.js');
            */

    },
    first: function() {
        mix.copy('resources/clubimp_layout/', 'public/web', false);
    }
};

switch(env) {
    case 'app':
        manageAssets.app(mix);
        break;
    case 'admin':
        manageAssets.admin(mix);
        break;
    case 'all':
        manageAssets.app(mix);
        manageAssets.admin(mix);
        break;
    case 'first':
        manageAssets.first(mix);
        break;
    default: break;
}
