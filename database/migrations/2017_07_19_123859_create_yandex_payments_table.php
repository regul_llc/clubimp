<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYandexPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        Schema::create('yandex_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cps');
            $table->text('parameters')->nullable()->default(null);
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
            $table->unique(['cps'], 'cps_unique');
        });
        */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yandex_payments');
    }
}
