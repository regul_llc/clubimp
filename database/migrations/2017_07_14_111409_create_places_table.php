<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('plan_id')->unsigned()->nullable()->default(null);
            $table->integer('section_id')->unsigned()->nullable()->default(null);
            $table->integer('row');
            $table->integer('num');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
        Schema::table('places', function($table) {
            $table->foreign('plan_id')->references('id')->on('plans')->onDelete('cascade');
        });
        Schema::table('places', function($table) {
            $table->foreign('section_id')->references('id')->on('sections')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('places', function($table) {
            $table->dropForeign('places_plan_id_foreign');
        });
        Schema::table('places', function($table) {
            $table->dropForeign('places_section_id_foreign');
        });
        Schema::dropIfExists('places');
    }
}
