<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->float('price')->default(300);
            $table->integer('plan_id')->unsigned()->nullable()->default(null);
            $table->timestamp('beginning_at')->nullable()->default(null);
            $table->timestamp('ticket_sale_to_at')->nullable()->default(null);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
        Schema::table('events', function($table) {
            $table->foreign('plan_id')->references('id')->on('plans')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function($table) {
            $table->dropForeign('events_plan_id_foreign');
        });
        Schema::dropIfExists('events');
    }
}
