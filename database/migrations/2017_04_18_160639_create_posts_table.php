<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_type_id')->unsigned();
            $table->string('title')->nullable()->default(null);
            $table->text('options')->nullable()->default(null);
            $table->string('template')->nullable()->default(null);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
        Schema::table('posts', function($table) {
            $table->foreign('post_type_id')->references('id')->on('post_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function($table) {
            $table->dropForeign('posts_post_type_id_foreign');
        });
        Schema::dropIfExists('posts');
    }
}
