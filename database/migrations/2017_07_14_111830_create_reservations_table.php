<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->integer('place_id')->unsigned();
            $table->string('code')->nullable()->default(null)->comment('places.section.name-places.row-places.num');
            $table->integer('payment_id')->unsigned()->nullable()->default(null);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
        Schema::table('reservations', function($table) {
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
        });
        Schema::table('reservations', function($table) {
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
        });
        Schema::table('reservations', function($table) {
            $table->foreign('place_id')->references('id')->on('places')->onDelete('cascade');
        });
        Schema::table('reservations', function($table) {
            $table->foreign('payment_id')->references('id')->on('payments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservations', function($table) {
            $table->dropForeign('reservations_event_id_foreign');
        });
        Schema::table('reservations', function($table) {
            $table->dropForeign('reservations_client_id_foreign');
        });
        Schema::table('reservations', function($table) {
            $table->dropForeign('reservations_place_id_foreign');
        });
        Schema::table('reservations', function($table) {
            $table->dropForeign('reservations_payment_id_foreign');
        });
        Schema::dropIfExists('reservations');
    }
}
