<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('page_id')->unsigned();
            $table->integer('post_id')->unsigned();
            $table->integer('priority')->default(0);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
        Schema::table('page_posts', function($table) {
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
        });
        Schema::table('page_posts', function($table) {
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('page_posts', function($table) {
            $table->dropForeign('page_posts_page_id_foreign');
        });
        Schema::table('page_posts', function($table) {
            $table->dropForeign('page_posts_post_id_foreign');
        });
        Schema::dropIfExists('page_posts');
    }
}
