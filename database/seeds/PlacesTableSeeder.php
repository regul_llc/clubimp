<?php

use Illuminate\Database\Seeder;
use App\Models\Place;
use App\Models\Section;
use App\Models\Plan;
use App\Database\Seeds\CommonDatabaseSeeder;

class PlacesTableSeeder extends Seeder
{
    use CommonDatabaseSeeder;

    private $defaults = [
        [
            'name' => 'p',
            'title' => 'партер',
            'count' => 20,
            'rows' => 12,
        ], [
            'name' => 't1',
            'title' => 'трибуна 1',
            'count' => [6, 6, 8],
            'rows' => 3
        ], [
            'name' => 't2',
            'title' => 'трибуна 2',
            'count' => [6, 6, 8],
            'rows' => 3
        ], [
            'name' => 't3',
            'title' => 'трибуна 3',
            'count' => [6, 6, 8],
            'rows' => 3
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncate(Place::class);
        $oPlan = Plan::first();
        foreach($this->defaults as $default) {
            $oSection = Section::where('name', $default['name'])->first();
            if (is_null($oSection)) {
                $oSection = Section::create([
                    'plan_id' => $oPlan->id,
                    'name' => $default['name'],
                    'title' => $default['title'],
                ]);
            }
            $aPlaces = [];
            if (is_array($default['count'])) {
                foreach($default['count'] as $key => $count) {
                    $aPlaces = $this->setPlaces($key + 1, $count, $oSection, $oPlan, $aPlaces, true);
                }
            } else {
                $aPlaces = $this->setPlaces($default['rows'], $default['count'], $oSection, $oPlan, $aPlaces);
            }
            Place::insert($aPlaces);
        }
    }


    public function setPlaces($row, $count, $oSection, $oPlan, $aPlaces, $single = false)
    {
        if ($single) {
            $start = $row - 1;
        } else {
            $start = 0;
        }
        for($i=$start;$i<$row;$i++) {
            for($j=0;$j<$count;$j++) {
                $aPlaces[] = [
                    'section_id' => $oSection->id,
                    'plan_id' => $oPlan->id,
                    'row' => $i + 1,
                    'num' => $j + 1,
                ];
            }
        }
        return $aPlaces;
    }
}
