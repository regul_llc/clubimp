<?php

namespace App\Database\Seeds;


trait CommonDatabaseSeeder
{
    public function truncate($class)
    {
        $oModels = $class::all();
        if (!empty($oModels) && !empty($oModels[0])) {
            foreach($oModels as $oModel) {
                $oModel->delete();
            }
        }
    }
}