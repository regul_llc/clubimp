<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;

class PostTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('post_types')->delete();

        $types = Config::get('project.post_types');
        $aTypes = [];
        $id = 1;
        foreach($types as $key => $type) {
            if (isset($type['slug']) && !empty($type['slug']) && isset($type['title']) && !empty($type['title'])) {
                $aTypes[$key]['id']             = $id;
                $aTypes[$key]['slug']           = $type['slug'];
                $aTypes[$key]['title']          = $type['title'];
                $aTypes[$key]['rules']          = json_encode($type['rules']);
                $aTypes[$key]['created_at']     = Carbon::now()->format('Y-m-d H:i:s');
                $aTypes[$key]['updated_at']     = Carbon::now()->format('Y-m-d H:i:s');
                $id++;
            }
        }
        if (!empty($aTypes)) {
            \DB::table('post_types')->insert($aTypes);
        }
    }
}
