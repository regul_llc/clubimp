<?php

use Illuminate\Database\Seeder;
use App\Models\Event;
use App\Models\Plan;
use Carbon\Carbon;
use App\Database\Seeds\CommonDatabaseSeeder;

class EventsTableSeeder extends Seeder
{
    use CommonDatabaseSeeder;

    private $defaults = [
        [
            'title' => 'Мероприятие 1',
            'beginning_at' => '25.07.2017 20:00:00',
            'price' => 300
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncate(Event::class);

        foreach($this->defaults as $default) {
            Event::create([
                'plan_id' => Plan::first()->id,
                'price' => $default['price'],
                'title' => $default['title'],
                'beginning_at' => Carbon::parse($default['beginning_at']),
                'ticket_sale_to_at' => Carbon::now(),
            ]);
        }
    }
}
