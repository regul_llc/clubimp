<?php

use Illuminate\Database\Seeder;
use App\Models\Section;
use App\Models\Plan;
use App\Database\Seeds\CommonDatabaseSeeder;

class SectionsTableSeeder extends Seeder
{
    use CommonDatabaseSeeder;

    private $defaults = [
        [
            'name' => 'section_1',
            'title' => 'Секция 1',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncate(Section::class);
        /*
        foreach($this->defaults as $default) {
            Section::create([
                'plan_id' => Plan::first()->id,
                'name' => $default['name'],
                'title' => $default['title'],
            ]);
        }
        */
    }
}
