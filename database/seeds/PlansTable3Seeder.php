<?php

use Illuminate\Database\Seeder;
use App\Models\Place;
use App\Models\Section;
use App\Models\Plan;
use App\Models\Event;
use Carbon\Carbon;
use App\Database\Seeds\CommonDatabaseSeeder;

class PlansTable3Seeder extends Seeder
{
    use CommonDatabaseSeeder;

    private $fastReservation = true;

    private $defaults = [
        [
            'name' => 'plan_3',
            'title' => 'План 3',
            'tpl' => 'plans.3',
            'address' => 'г.Ростов-на-Дону, Большая Садовая 51, бывший к/т Победа',
            'address_title' => '',
            'coordinates' => [
                'lon' => 47.221784,
                'lat' => 39.714185
            ]
        ],
    ];

    private $places = [
        [
            'name' => 'p',
            'title' => 'партер',
            'matrix' => [
                14 => [1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1],
                13 => [1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1],
                12 => [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0],
                11 => [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                10 => [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                9  => [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                8  => [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                7  => [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                6  => [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                5  => [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                4  => [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                3  => [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                2  => [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                1  => [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0], // [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0]
                //     1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21
            ],
        ],
        [
            'name' => 'b',
            'title' => 'балкон',
            'matrix' => [
                3  => [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                2  => [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                1  => [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                //     1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16
            ],
        ],
    ];

    private $event = [
        'title' => 'Мероприятие 3',
        'beginning_at' => '25.10.2017 20:00:00',
        'price' => 350
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->defaults as $default) {
            Plan::create($default);
        }

        $oPlan = Plan::where('name', 'plan_3')->first();
        foreach($this->places as $place) {
            $oSection = Section::where('name', $place['name'])->where('plan_id', $oPlan->id)->first();
            if (is_null($oSection)) {
                $oSection = Section::create([
                    'plan_id' => $oPlan->id,
                    'name' => $place['name'],
                    'title' => $place['title'],
                ]);
            }
            $aPlaces = [];
            foreach($place['matrix'] as $key => $array) {
                $aPlaces = $this->setPlaces($key, $array, $oSection, $oPlan, $aPlaces);
            }
            Place::insert($aPlaces);
        }

        Event::create([
            'plan_id' => $oPlan->id,
            'price' => $this->event['price'],
            'title' => $this->event['title'],
            'beginning_at' => Carbon::parse($this->event['beginning_at']),
            'ticket_sale_to_at' => Carbon::now(),
        ]);

        if ($this->fastReservation) {
            $this->setFastReservation();
        }
    }


    /**
     * Сгенерирование массива для inset'a
     *
     * @param $key
     * @param $array
     * @param $oSection
     * @param $oPlan
     * @param $aPlaces
     * @return array
     */
    public function setPlaces($key, $array, $oSection, $oPlan, $aPlaces)
    {
        foreach($array as $subKey => $value) {
            if (intval($value) !== 0) {
                $aPlaces[] = [
                    'section_id' => $oSection->id,
                    'plan_id' => $oPlan->id,
                    'row' => $key,
                    'num' => $subKey + 1,
                ];
            }
        }
        return $aPlaces;
    }


    /**
     * Просили заблокировать места при создании нового плана
     *
     *
     */
    public function setFastReservation()
    {
        $oPlan = Plan::where('name', 'plan_3')->first();
        $oPlaces = Place::where('plan_id', $oPlan->id)->where('row', 6)->whereIn('num', [6, 7, 8, 9, 10, 11, 12])->get();
        foreach ($oPlaces as $oPlace) {
            $oPlace->update([
                'status' => 0
            ]);
        }
        $oPlaces = Place::where('plan_id', $oPlan->id)->where('row', 5)->whereIn('num', [8, 9, 10, 11, 12, 13])->get();
        foreach ($oPlaces as $oPlace) {
            $oPlace->update([
                'status' => 0
            ]);
        }

    }
}
