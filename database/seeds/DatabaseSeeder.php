<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(PostTypesTableSeeder::class);
        $this->call(PagesFakeSeeder::class);
        $this->call(SettingsSeeder::class);
        $this->call(UsersTableSeeder::class);

        $this->call(PlansTableSeeder::class);
        $this->call(SectionsTableSeeder::class);
        $this->call(PlacesTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        $this->call(ClientsTableSeeder::class);
    }
}
