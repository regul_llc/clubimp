<?php

use Illuminate\Database\Seeder;
use App\Database\Seeds\CommonDatabaseSeeder;
use App\Models\Client;

class ClientsTableSeeder extends Seeder
{
    use CommonDatabaseSeeder;

    private $defaults = [
        [
            'name' => 'Клиент',
            'phone' => 78005553535,
            'email' => 'temp@temp.com',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncate(Client::class);

        foreach($this->defaults as $default) {
            /*
            Client::create([
                'name' => $default['name'],
                'phone' => $default['phone'],
                'email' => $default['email'],
            ]);
            */
        }
    }
}
