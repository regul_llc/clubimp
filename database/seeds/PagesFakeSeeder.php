<?php

use Illuminate\Database\Seeder;
use App\Models\Page;
use App\Models\Post;
use App\Models\PostType;
use App\Models\PagePosts;

class PagesFakeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $oFirstPage = Page::create([
            'title' => 'Test first page',
            'url' => str_slug('Test first page'),
            'seo' => [
                'description' => 'description',
                'keywords' => 'key, words'
            ]
        ]);
        $oSecondPage = Page::create([
            'title' => 'Test second page',
            'url' => str_slug('Test second page'),
        ]);
        $oPostTypes = PostType::all();

        $oFirstPost = Post::create([
            'post_type_id' => $oPostTypes->where('slug', 'header')->first()->id,
            'title' => 'Главная',
        ]);
        $oSecondPost = Post::create([
            'post_type_id' => $oPostTypes->where('slug', 'blocks')->first()->id,
            'title' => 'Блоки',
        ]);
        $oThirdPost = Post::create([
            'post_type_id' => $oPostTypes->where('slug', 'slider.simple')->first()->id,
            'title' => 'Слайдер',
        ]);
        $oFourPost = Post::create([
            'post_type_id' => $oPostTypes->where('slug', 'contacts')->first()->id,
            'title' => 'Контакты',
        ]);
        $oFivePost = Post::create([
            'post_type_id' => $oPostTypes->where('slug', 'static')->first()->id,
            'title' => 'Работы',
        ]);
        $oFirstPage->posts()->attach($oFirstPost->id,   ['priority' => 10]);
        $oFirstPage->posts()->attach($oSecondPost->id,  ['priority' => 6]);
        $oFirstPage->posts()->attach($oFivePost->id,    ['priority' => 5]);
        $oFirstPage->posts()->attach($oThirdPost->id,   ['priority' => 2]);
        $oFirstPage->posts()->attach($oFourPost->id,    ['priority' => 1]);

        $oSecondPage->posts()->attach($oFirstPost->id,  ['priority' => 1]);
        $oSecondPage->posts()->attach($oThirdPost->id,  ['priority' => 5]);
    }
}
