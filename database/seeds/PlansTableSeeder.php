<?php

use Illuminate\Database\Seeder;
use App\Models\Plan;
use App\Database\Seeds\CommonDatabaseSeeder;

class PlansTableSeeder extends Seeder
{
    use CommonDatabaseSeeder;

    private $defaults = [
        [
            'name' => 'plan_1',
            'title' => 'План 1',
            'tpl' => 'plans.1',
            'address' => 'г.Ростов-на-Дону, просп. Михаила Нагибина, 3Г',
            'address_title' => 'Медиапарк «Южный Регион-ДГТУ»',
            'coordinates' => [
                'lon' => 47.238410,
                'lat' => 39.712066
            ]
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncate(Plan::class);

        foreach($this->defaults as $default) {
            Plan::create($default);
        }
    }
}
