<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'key' => 'site.title',
            'value' => config('app.name')
        ]);
        Setting::create([
            'key' => 'site.description',
            'value' => 'Default description'
        ]);
        Setting::create([
            'key' => 'site.keywords',
            'value' => 'key, words, something'
        ]);
        Setting::create([
            'key' => 'site.favicon',
            'value' => asset('img/favicon.png')
        ]);
        Setting::create([
            'key' => 'site.copyright',
            'value' => '2011-2017 - Название компании'
        ]);
    }
}
