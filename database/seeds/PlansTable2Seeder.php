<?php

use Illuminate\Database\Seeder;
use App\Models\Place;
use App\Models\Section;
use App\Models\Plan;
use App\Models\Event;
use Carbon\Carbon;
use App\Database\Seeds\CommonDatabaseSeeder;

class PlansTable2Seeder extends Seeder
{
    use CommonDatabaseSeeder;

    private $defaults = [
        [
            'name' => 'plan_2',
            'title' => 'План 2',
            'tpl' => 'plans.2',
            'address' => 'г.Ростов-на-Дону, Коммунаров 52',
            'address_title' => '"Волшебный пендель"',
            'coordinates' => [
                'lon' => 47.246264574261524,
                'lat' => 39.77640350000001
            ]
        ],
    ];

    private $places = [
        [
            'name' => 'p',
            'title' => 'партер',
            'matrix' => [
                6 => [0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0],
                5 => [1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0],
                4 => [1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1],
                3 => [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                2 => [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0],
                1 => [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0],
            ],
        ],
    ];

    private $event = [
        'title' => 'Мероприятие 2',
        'beginning_at' => '15.08.2017 20:00:00',
        'price' => 250
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->defaults as $default) {
            Plan::create($default);
        }

        $oPlan = Plan::where('name', 'plan_2')->first();
        foreach($this->places as $place) {
            $oSection = Section::where('name', $place['name'])->where('plan_id', $oPlan->id)->first();
            if (is_null($oSection)) {
                $oSection = Section::create([
                    'plan_id' => $oPlan->id,
                    'name' => $place['name'],
                    'title' => $place['title'],
                ]);
            }
            $aPlaces = [];
            foreach($place['matrix'] as $key => $array) {
                $aPlaces = $this->setPlaces($key, $array, $oSection, $oPlan, $aPlaces);
            }
            Place::insert($aPlaces);
        }

        Event::create([
            'plan_id' => $oPlan->id,
            'price' => $this->event['price'],
            'title' => $this->event['title'],
            'beginning_at' => Carbon::parse($this->event['beginning_at']),
            'ticket_sale_to_at' => Carbon::now(),
        ]);
    }


    public function setPlaces($key, $array, $oSection, $oPlan, $aPlaces)
    {
        foreach($array as $subKey => $value) {
            if (intval($value) !== 0) {
                $aPlaces[] = [
                    'section_id' => $oSection->id,
                    'plan_id' => $oPlan->id,
                    'row' => $key,
                    'num' => $subKey + 1,
                ];
            }
        }
        return $aPlaces;
    }
}
