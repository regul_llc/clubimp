ymaps.ready(init);
var myMap,
    myPlacemark;
var $eventContainer = $('#event-map');

var sBalloonTpl = [
    '<div class="branch-balloon">',
    '<div class="balloon__content">',
    '<div class="companies-item__header clearfix">',
    '<div class="baloon-head">',
    '<div class="baloon-close">х</div>',
    '<div class="title-desc">',
    '<div class="baloon-title __color-2">',
    '<span class="name">' + $eventContainer.data('event-title') + '</span>',
    '</div>',
    '<div class="baloon-street">' + $eventContainer.data('plan-address') + '</div>',
    '</div>',
    '</div>',
    '</div>',
    '<div class="blue-down arrow-ballon"></div>',
    '</div>',

].join('');

function init(){

    myMap = new ymaps.Map("map-yandex", {
        center: [$eventContainer.data('coordinates-lon'), $eventContainer.data('coordinates-lat')],
        zoom: 14
    });

    var customBalloon = ymaps.templateLayoutFactory.createClass(sBalloonTpl);


    //47.238410, 39.712066
    //'Медиапарк «Южный Регион-ДГТУ»',

    myPlacemark = new ymaps.Placemark([
        $eventContainer.data('coordinates-lon'),
        $eventContainer.data('coordinates-lat'),
    ], {
        hintContent: $eventContainer.data('hint-content'),
        balloonContent: $eventContainer.data('balloon-content'),

    },{
        balloonAutoPan: false,
        balloonLayout : customBalloon,
        iconLayout: 'default#image',
        iconImageHref: "images/mark.png",
        iconImageSize: [44, 64],
        iconImageOffset: [-24, -64]
    });

    myMap.geoObjects.add(myPlacemark);
    myMap.behaviors.disable('scrollZoom');

    $('.module-map').on('click','.baloon-close',function(){
        myPlacemark.balloon.close();
    })
}